# Plotting Manhattan and Receiver Operating Curve Plots

ROC <- function(SCORES, LABELS, ID=NULL){
	#INPUT:
	#	SCORES = list or vector of -log(p-values)
	#	LABELS = incidence list of which elements are causal (lalelled 1) and non-causal (labelled 0)
	#	ID = optinal identifier for the resulting ROC plot
	#OUTPUT:
	#	ROC plot with the aurea under the curve (AUC) value

	LABELS = LABELS[order(SCORES, decreasing=TRUE)] #We are asking if the highest scores the ones labeled 1?
	TPR = cumsum(LABELS)/sum(LABELS)
	FPR = cumsum(!LABELS)/sum(!LABELS)
	dx = FPR[2:length(FPR)] - FPR[1:(length(FPR)-1)]
	fx = TPR[2:length(TPR)]
	AUC = sum(dx*fx)
	plot(x=FPR, y=TPR, type="l", col="red", main=paste0("ROC Plot - ", ID), xlab="False Positive Rate", ylab="True Positive Rate")
	legend("bottomright", legend=c(paste0("AUC = ", round(AUC, 2))))
}

Manhattan <- function(LOD, alpha, ID=NULL, QTL=NULL){
	l = length(LOD)
	bonferroni = -log(alpha/l, base=10)
	plot(x=1:l, y=LOD, type="p", pch=20, col="gray", main=paste0("Manhattan Plot - ", ID), xlab="Sites", ylab=expression("-log"[10]*"(p-value)"))
	lines(x=1:l, y=rep(bonferroni, times=l), lty=3, col="red")
	if(!is.null(QTL)){
		points(x=QTL$loc, y=LOD[QTL$loc], type="p", pch=10, col="red")
		performance = sum(LOD[QTL$loc]>=bonferroni)*100/nrow(QTL)
		legend("topright", legend=c(paste0("Bonferroni Threshold = ", round(bonferroni)), paste0("Correctly Detected = ", round(performance), "%")))
	} else {
		legend("topright", legend=c(paste0("Bonferroni Threshold = ", round(bonferroni))))
	}
}
