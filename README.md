# Modelling Genome-Transcriptome-Proteome-Phenome Networks
Modelling Genotype-Transcript to Phenotype Translation and using artificial neural network for connecting genomes to transcriptomes to proteomes to phenomes

## Building addtive, and multiplicative phenotype models
1. Additive
	- genetic (SNP) data alone
	- transcript (abundance) data alone
	- genetic + transcript additive (independent)
1. Additive and Multiplicative
	- addive + dominance
	- additive + dominance + epistasis
	- genetic + transcript + interaction
1. Completely Multiplicative
	- Y = XKABj + e

## Testing the feasibility of using neural networks for transcriptome/proteome/phenome prediction given the genome as input
I have very vague ideas on how to do these..
1. training
1. prediction
1. artificial neural networks
1. deep neural networks
1. which activation function to use? Sigmoid function??
1. how many nodes in the hidden layers???
