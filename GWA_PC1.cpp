#include <RcppGSL.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_cdf.h>
#include <cmath>
// declare a dependency on the RcppGSL package;
// also activates plugin (but not needed when
// ’LinkingTo: RcppGSL’ is used with a package)

//-----from Eddelbuettel & Francois 2018 - RcppGSL: Easier GSL use from R via Rcpp
//----------dependencies:
// ---------------sudo apt install libgsl-dev
// ---------------install.packages("Rcpp")
// ---------------install.packages("RcppGSL")

// The nex two lines are very important! They turn the following function into a callable function in R
//[[Rcpp::depends(RcppGSL)]]
// [[Rcpp::export]]
Rcpp::NumericVector fastLm(const RcppGSL::Matrix & X, const RcppGSL::Vector & y) {
	// row and column dimension
	int n = X.nrow(), k = X.ncol();
	double chisq;
	// to hold the coefficient vector
	RcppGSL::Vector coef(k);
	// and the covariance matrix
	RcppGSL::Matrix cov(k,k);
	// the actual fit requires working memory
	// which we allocate and then free
	gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc (n, k);
	gsl_multifit_linear (X, y, coef, cov, &chisq, work);
	gsl_multifit_linear_free (work);
	// assign diagonal to a vector, then take
	// square roots to get std.error
	Rcpp::NumericVector std_err;
	// need two step decl. and assignment
	std_err = gsl_matrix_diagonal(cov);
	// sqrt() is an Rcpp sugar function
	std_err = Rcpp::sqrt(std_err);
	//OUTPUT vector of just the slope and stderr of the single factor (not including the instercept or mean)
	Rcpp::NumericVector OUT(2);
	OUT(0) = coef[1];
	OUT(1) = std_err[1];
	return(OUT);
}


/////////////////////////////////////////////////////////////////////////



// [[Rcpp::export]]
Rcpp::List GWA_PC1(Rcpp::NumericMatrix X, Rcpp::NumericVector PC1, RcppGSL::Vector y) {
	int n = X.nrow();
	int l = X.ncol();
	RcppGSL::Matrix Xi(n,3);
	Rcpp::NumericVector fast_out(2);
	Rcpp::NumericVector eff(l);
	Rcpp::NumericVector lod(l);
	//create the X matrix with the first column as the mean and the second column as the genotype at the ith site
	for(int i=0; i<l; i++){
		for(int j=0; j<n; j++){
			Xi(j, 0) = 1;
			Xi(j, 1) = X(j,i);
			Xi(j, 2) = PC1(j);
		}
		fast_out = fastLm(Xi, y);
		eff(i) = fast_out(0);
		lod(i) = -log10( 1 - gsl_cdf_tdist_P(abs(fast_out(0)/fast_out(1)), n) ); //needed to subtract from 1 since the cdf function starts from the lower tail
	}
	return
	Rcpp::List::create(
	Rcpp::Named("effect") = eff,
	Rcpp::Named("log10_pval") = lod);
}
