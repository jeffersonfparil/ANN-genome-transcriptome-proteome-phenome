#!/usr/bin/env Rscript
# Genome-wide and Transcriptome-wide Association Analyses  (GWAS & TWAS)
# using empirical Arabidospis thaliana (Salk lines) data

#################
###			  ###
### ARGUMENTS ###
###			  ###
#################
args = commandArgs(trailingOnly=FALSE)
#R.DIR = args[1]
#slave = args[2]
#restore = args[3]
src_dir = sub("--file=", "", dirname(args[4])) #directory where the source code of the functions are located
#--args = args[5]
DIR = args[5]

# src_dir="/data/Lolium/Softwares/ANN-genome-transcriptome-proteome-phenome"
# DIR="/data/Lolium/Quantitative_Genetics/At_empirical"

#####################################################
###												  ###
### Load genome, transcriptome and phenotype data ###
###												  ###
#####################################################
GENOMES = readRDS(paste0(DIR, "/GENOMES/At_genomes.rds"))
TRANSCRIPTOMES = readRDS(paste0(DIR, "/TRANSCRIPTOMES/At_transcriptomes.rds"))
PHENOTYPES = read.csv(paste0(src_dir, "/res/At_phenotype_FT10.csv"))

######################################
###								   ###
### Pre-processing the genome data ###
###								   ### so that we have n rows (number of individuals)
###################################### and l columns (number of loci)
X0 = t(GENOMES[,2:ncol(GENOMES)])
colnames(X0) = GENOMES$LOCI
rownames(X0) = colnames(GENOMES)[2:ncol(GENOMES)]
X0 = X0[order(rownames(X0)),]

#############################################
###										  ###
### Pre-processing the transcriptome data ###
###										  ### so that we have n rows (number of individuals)
############################################# and m columns (number of transcripts)
T0 = t(TRANSCRIPTOMES[,2:ncol(TRANSCRIPTOMES)])
colnames(T0) = TRANSCRIPTOMES$TRAN_ID
rownames(T0) = colnames(TRANSCRIPTOMES)[2:ncol(TRANSCRIPTOMES)]
T0 = T0[order(rownames(T0)),]

#########################################
###									  ###
### Pre-processing the phenotype data ###
###									  ### so that we only have the accession names
######################################### and the one phenotype flowering time
PHEN = PHENOTYPES[!duplicated(PHENOTYPES$Accession),]
y0 = data.frame(FT=PHEN$Phenotype)
rownames(y0) = PHEN$Accession

#########################################################
###													  ###
### Merge genomic, transcriptomic and phenotypic data ###
###													  ### to find the individuals with genomic,
######################################################### transcriptomic and phenotypic data
XT = merge(X0, T0, by="row.names")
	rownames(XT) = XT$Row.names
	XT = XT[,2:ncol(XT)]
XTy = merge(XT, y0, by="row.names")
	rownames(XTy) = XTy$Row.names
	XTy = XTy[,2:ncol(XTy)]

######################
###				   ###
### Genotypic data ###
###				   ### filtering out monomorphic sites and sites with minimum allele frequency (MAF) < 0.01 and maximum allele frequency > 0.99
######################
X = as.matrix(XTy[,1:ncol(X0)])
X = X[,colSums(X)!=0 & colSums(X)!=nrow(X)]
MAF = 0.01
X = X[,colMeans(X)>=MAF & colMeans(X)<=(1-MAF)]
saveRDS(X, file="At_134Salk_SNP.rds") #save the consolidated SNP data

###########################
###						###
### Transcriptomic data ###
###						### remove transcripts without any variation
###########################
T = as.matrix(XTy[,(ncol(X0)+1):(ncol(X0)+ncol(T0))])
VT = apply(T, MARGIN=2, FUN=var)
T = T[,VT!=0]
T = T[,order(colnames(T))]
saveRDS(T, file="At_134Salk_Transcripts.rds") #save the consolidated SNP data

#######################
###					###
### Phenotypic data ###
###					###
#######################
y = as.matrix(XTy[,ncol(XTy)])
rownames(y) = rownames(XTy)

#########################################
###									  ###
### List dimensions of the input data ###
###									  ###
#########################################
n = nrow(y) #number of individuals
l = ncol(X) #number of SNP loci
m = ncol(T) #number of transcripts

#######################
###					###
### Really fast GWA ###	install GSL: sudo apt install libgsl-dev
###					### install RcppGSL: install.packages("RcppGSL")
####################### test: src_dir="~/SOFTWARES/ANN-genome-transcriptome-proteome-phenome/"
library(Rcpp)
library(RcppGSL)
sourceCpp(paste0(src_dir, "/GWA.cpp"))
#INPUT: X SNP matrix and y phenotype vector
#OUTPUT: list of SNP effects ($effects) and t-values ($t.val) (tdist pdf in RcppGSL not yet working ;-|)

############################################
###										 ###
### ROC and Manhattan Plotting Functions ###
###										 ###
############################################
source(paste0(src_dir, "/plot_Manhattan_ROC.r"))

############
###		 ###
### GWAS ###
###		 ###
############
GWAS = GWA(X,y)
LOD.GWAS = -log(GWAS$p.value, base=10)
par(mfrow=c(1,2))
Manhattan(LOD=LOD.GWAS, alpha=0.05, ID="GWAS")
ROC(SCORES=LOD.GWAS, LABELS=rbinom(l, 1, p=0.5), ID="GWAS")

############
###		 ###
### TWAS ###
###		 ###
############
TWAS = GWA(T,y)
LOD.TWAS = -log(TWAS$p.value, base=10)
par(mfrow=c(1,2))
Manhattan(LOD=LOD.TWAS, alpha=0.05, ID="TWAS")
ROC(SCORES=LOD.TWAS, LABELS=rbinom(m, 1, p=0.5), ID="TWAS")
