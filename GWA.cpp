#include <Rcpp.h>
#include <RcppGSL.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_linalg.h>
#include <cmath>
#include <malloc.h>
// declare a dependency on the RcppGSL package;
// also activates plugin (but not needed when
// ’LinkingTo: RcppGSL’ is used with a package)

//-----from Eddelbuettel & Francois 2018 - RcppGSL: Easier GSL use from R via Rcpp
//----------dependencies:
// ---------------sudo apt install libgsl-dev
// ---------------install.packages("Rcpp")
// ---------------install.packages("RcppGSL")

//[[Rcpp::depends(RcppGSL)]]
// [[Rcpp::export]]
// Rcpp::NumericVector fastLm(const RcppGSL::Matrix & X, const RcppGSL::Vector & y) {
gsl_vector fastLm(const RcppGSL::Matrix & X, const RcppGSL::Vector & y) {
// gsl_vector fastLm(gsl_matrix *X, gsl_vector *y) { //how do we properly declare input variables here?!?!?!?
	// setup variables
	int n = X.nrow(), k = X.ncol();
	// int n = X->size1, k = X->size2;
	// int n = X.size1, k = X.size2;
	gsl_vector *b = gsl_vector_alloc(k);
	gsl_matrix *XtX = gsl_matrix_alloc(k, k);
	gsl_matrix *XtX_inverse = gsl_matrix_alloc(k, k);
	gsl_vector *Xty = gsl_vector_alloc(k);

	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, X, X, 0.0, XtX);
	gsl_blas_dgemv(CblasTrans, 1.0, X, y, 0.0, Xty);
   	gsl_permutation *p = gsl_permutation_alloc(k); //permutation input of LU decomposition
	int w; //kind of like a workspace me thinks...
    gsl_linalg_LU_decomp(XtX, p, &w); //LU decomposition
    // gsl_linalg_LU_solve(XtX, p, Xty, b); //solve for beta
    gsl_linalg_LU_invert(XtX, p, XtX_inverse); //inverse of X'X
    gsl_blas_dgemv(CblasNoTrans, 1.0, XtX_inverse, Xty, 0.0, b);

    /////////////////////////////////////////
    // PROBLEM IS HERE!
    // How do we properly estimate the residual variance?
    // Is it specific for each beta element such that there is error variance partitioning and we only need the one for the SNP effect?

    gsl_vector *e = gsl_vector_alloc(n);
    double Ve;
    gsl_blas_dgemv(CblasNoTrans, -1.0, X, b, 0.0, e);
    gsl_vector_add(e, y);
    gsl_blas_ddot(e, e, &Ve);
    Ve = (1.0/n) * Ve;
    // se = sqrt(Ve);

    // the correct standard error for multiple linear regression:
    double se = sqrt(Ve * gsl_matrix_get(XtX_inverse, k-1, k-1));
    /////////////////////////////////////////

	//OUTPUT vector of just the slope and stderr of the single factor (not including the instercept or mean)
	// Rcpp::NumericVector OUT(2);
	gsl_vector *OUT = gsl_vector_alloc(3); //intercept, SNP effect & standard error of SNP effect
	gsl_vector_set(OUT, 0, gsl_vector_get(b, 0));
	gsl_vector_set(OUT, 1, gsl_vector_get(b, 1));
	gsl_vector_set(OUT, 2, se);
	// printf("int=%f\n", gsl_vector_get(b,0));
	// printf("SNP=%f\n", gsl_vector_get(b,1));
	// printf("se=%f\n", se);
	gsl_matrix_free(XtX);
	gsl_matrix_free(XtX_inverse);
	gsl_vector_free(Xty);
	gsl_vector_free(b);
	gsl_permutation_free(p);
	gsl_vector_free(e);

	return(*OUT);

	gsl_vector_free(OUT);
}

// //[[Rcpp::depends(RcppGSL)]]
// //[[Rcpp::export]]
// gsl_vector fastLm(const RcppGSL::Matrix & X, const RcppGSL::Vector & y) {
// // gsl_vector fastLm(gsl_matrix *X, gsl_vector *y) { //how do we properly declare input variables here?!?!?!?
// 	// setup variables
// 	int n = X.nrow(), k = X.ncol();
// 	// int n = X->size1, k = X->size2;
// 	// int n = X.size1, k = X.size2;
// 	double *chisq = (double *) malloc(sizeof(double));
// 	// double *chisq;
// 	// to hold the coefficient vector
// 	// RcppGSL::Vector coef(k);
// 	gsl_vector *coef = gsl_vector_alloc(k);
// 	// and the covariance matrix
// 	// RcppGSL::Matrix cov(k,k);
// 	gsl_matrix *cov = gsl_matrix_alloc(k, k);
// 	// the actual fit requires working memory
// 	// which we allocate and then free
// 	gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc (n, k);
// 	// gsl_multifit_linear (X, y, coef, cov, &chisq, work);
// 	gsl_multifit_linear (X, y, coef, cov, chisq, work);
// 	gsl_multifit_linear_free (work);
// 	// assign diagonal to a vector, then take
// 	// square roots to get std.error
// 	// Rcpp::NumericVector std_err;
// 	gsl_vector *std_err = gsl_vector_alloc(k);
// 	// double std_err[k];
// 	// need two step decl. and assignment
// 	// std_err = gsl_matrix_diagonal(cov);
// 	*std_err = gsl_matrix_diagonal(cov).vector;
// 	// sqrt() is an Rcpp sugar function
// 	// std_err = Rcpp::sqrt(std_err);
// 	for (int i=0; i<k; i++){
// 		gsl_vector_set(std_err, i, sqrt(gsl_vector_get(std_err, i)));
// 	}
// 	//OUTPUT vector of just the slope and stderr of the single factor (not including the instercept or mean)
// 	// Rcpp::NumericVector OUT(2);
// 	gsl_vector *OUT = gsl_vector_alloc(3); //intercept, SNP effect & standard error of SNP effect
// 	// OUT(0) = coef[1]; //for t-test of the coefficient against 0 not the intercept!
// 	gsl_vector_set(OUT, 0, gsl_vector_get(coef, 0));
// 	gsl_vector_set(OUT, 1, gsl_vector_get(coef, 1));
// 	// OUT(1) = std_err[1];
// 	// OUT(1) = gsl_vector_get(std_err, 1);
// 	gsl_vector_set(OUT, 2, gsl_vector_get(std_err, 1));
// 	return(*OUT);

// 	gsl_matrix_free(cov);
// 	gsl_vector_free(coef);
// 	gsl_vector_free(std_err);
// 	gsl_vector_free(OUT);
// }


/////////////////////////////////////////////////////////////////////////


//[[Rcpp::depends(RcppGSL)]]
// [[Rcpp::export]]
Rcpp::List GWA(const Rcpp::NumericMatrix X, const RcppGSL::Vector y) {
// Rcpp::List GWA(Rcpp::NumericMatrix & XX, Rcpp::NumericVector & yy) {
// Rcpp::List GWA(const RcppGSL::Matrix & X, const RcppGSL::Vector & y) {
	int n = X.nrow();
	int l = X.ncol();
	// int n = XX.nrow();
	// int l = XX.ncol();
	// gsl_matrix *X = gsl_matrix_alloc(n, l);
	// for (int i=0; i<n; i++){
	// 	for (int j=0; j<l; j++){
	// 		gsl_matrix_set(X, i, j, XX[i,j]);
	// 	}
	// }
	// gsl_vector *y = gsl_vector_alloc(n);
	// for (int i=0; i<n; i++){
	// 	gsl_vector_set(y, i, yy[i]);
	// }
	RcppGSL::Matrix Xi(n,2);
	// gsl_matrix *Xi = gsl_matrix_alloc(n,2);
	Rcpp::NumericVector fast_out(2);
	// gsl_vector *fast_out = gsl_vector_alloc(2);
	Rcpp::NumericVector eff(l);
	Rcpp::NumericVector ste(l);
	// gsl_vector *eff = gsl_vector_alloc(l);
	Rcpp::NumericVector lod(l);
	// gsl_vector *pval = gsl_vector_alloc(l);
	//create the X matrix with the first column as the mean and the second column as the genotype at the ith site
	double intercept;
	double effect;
	double stderr;
	double tcalc;
	for(int i=0; i<l; i++){
		for(int j=0; j<n; j++){
			Xi(j, 0) = 1;
			Xi(j, 1) = X(j,i);
		}
		// for (int j=0; j<n; j++){
		// 	gsl_matrix_set(Xi, j, 0, 1);
		// 	gsl_matrix_set(Xi, j, 1, gsl_matrix_get(X, i, j));
		// }
		// gsl_matrix_free(Xi);
		fast_out = fastLm(Xi, y);
		// gsl_vector fast_out = fastLm(Xi, y); //what-up here?!?!?!
		// *fast_out = fastLm(Xi, y);
		intercept = fast_out(0);
		effect = fast_out(1);
		stderr = fast_out(2);
		// double EFF = gsl_vector_get(&fast_out, 0);
		// double VER = gsl_vector_get(&fast_out, 1);
		// eff(i) = EFF;
		// gsl_vector_set(eff, i, gsl_vector_get(fast_out, 0));
		tcalc = abs(effect)/stderr; //t-statistic computed to test if the SNP effect is significantly different from zero SNP effect / SNP error
		eff(i) = effect;
		ste(i) = stderr;
		lod(i) = -log10( 1 - gsl_cdf_tdist_P(tcalc, n) ); //needed to subtract from 1 since the cdf function starts from the lower tail
		// pval(i) = 1 - gsl_cdf_tdist_P(abs(EFF/VER), n); //needed to subtract from 1 since the cdf function starts from the lower tail
		// double coef = gsl_vector_get(fast_out, 0);
		// double sder = gsl_vector_get(fast_out, 1);
		// gsl_vector_set(pval, i, gsl_cdf_tdist_P(abs(coef/sder), n));
	}
	return
	Rcpp::List::create(
	Rcpp::Named("effect") = eff,
	Rcpp::Named("stderr") = ste,
	Rcpp::Named("log10_pval") = lod);
}
