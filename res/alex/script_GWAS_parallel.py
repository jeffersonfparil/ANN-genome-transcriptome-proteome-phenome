#!/usr/bin/env python

from datetime import *
import sys
import os
import h5py
import numpy as np
from scipy import stats
if os.path.isdir("mixmogam"):
    sys.path.append("mixmogam")
elif os.path.isdir("../mixmogam"):
    sys.path.append("../mixmogam")
else: sys.exit("Can't find a path to mimogam libraries") 
import linear_models as lm

if not os.path.exists("1001genomes_accessions.npy"):
    prefix="../"
else: prefix="" #sys.exit("Can't find the genotype file(s)")

START=datetime.now()

#Missing Value Threshold
Miss_Tol=.3
#Minor Allele Frequency Threshold
MAF_Tol=.05
#Which Models should be computed?

if "GLM" not in sys.argv:
	GLM=False
else: GLM=True

if "EMMAX" in sys.argv:
	EMMAX=True
else: EMMAX=False

if "KW" in sys.argv:
	KW=True
else: KW=False

#get the SNP data
SNP_data=np.load(sys.argv[2])
SNP_names=np.load(sys.argv[2].replace('data','names'))
n_snps_start=SNP_data.shape[0]
Lines_geno=np.load(prefix+"1001genomes_accessions.npy")

#get the phenotype data
Pheno_data = np.genfromtxt(str(sys.argv[1]), delimiter=',')
Pheno_handle=open(sys.argv[1],'r')
Pheno_data = Pheno_handle.readlines()
Pheno_data = np.array([x.replace('\n','').split(',') for x in Pheno_data])
Lines_pheno=Pheno_data[1:,0]
Pheno_data=Pheno_data[1:,1].astype("float")

#remove lines with missing phenotype
Pheno_data=Pheno_data[~np.isnan(Pheno_data)]
Lines_pheno=Lines_pheno[~np.isnan(Pheno_data)]
#sort the lines in Phenotype
Pheno_sort=np.argsort(Lines_pheno)
Pheno_data=Pheno_data[Pheno_sort]
#remove the lines with no genotypes
Lines_in_pheno=np.in1d(Lines_pheno,Lines_geno)
Pheno_data=Pheno_data[Lines_in_pheno]
Lines_pheno=Lines_pheno[Lines_in_pheno]

#remove lines with missing phenotype
Lines_in_geno=np.in1d(Lines_geno,Lines_pheno)
SNP_data=SNP_data[:,Lines_in_geno]
print('=============================================================')
print('FROM PHENOTYPE FILE # '+str(sys.argv[1]))
print('Line/Ecotype ID included:')
print(Lines_pheno)

#sort the lines in SNP
SNP_sort=np.argsort(Lines_geno[Lines_in_geno])
SNP_data=np.transpose(SNP_data[:,SNP_sort])
SNP_data=SNP_data.astype('float')

#include the kinship matrix
with h5py.File(prefix+'kinship_ibs_mac5.hdf5','r') as hf:
	K_data = hf.get('kinship')
	K_data = np.array(K_data)
	Lines_K = hf.get('accessions')
	Lines_K = np.array(Lines_K)

Lines_in_K=np.in1d(Lines_K,Lines_pheno)
K_data=K_data[Lines_in_K,:][:,Lines_in_K]
K_sort=np.argsort(Lines_K[Lines_in_K])
K_data=K_data[K_sort,:][:,K_sort]
K_data=K_data/np.min(K_data)

#estimating the allele frequencies in the data
SNPsum=np.nansum(SNP_data,axis=0)
nInd=np.sum(~np.isnan(SNP_data),axis=0)
freq_hat=np.array(SNPsum,dtype="float")/(2*nInd)
mask=np.ndarray.flatten(np.array(np.all([freq_hat>MAF_Tol,freq_hat<(1-MAF_Tol)],axis=0)).astype("bool"))
SNP_data=SNP_data[:,mask]
SNP_names=SNP_names[mask,:]
MAF=freq_hat[mask]
SNP_in=np.sum(mask.astype("int"))

print('=============================================================')
print('FROM GENOTYPE FILE # '+str(sys.argv[2]))
if SNP_in==0:
	sys.exit('ERROR: No SNP satisfied Missing Value and/or Minor Allele Frequency Threshold, please edit')
else:
	print(str(n_snps_start-SNP_in)+'/'+str(n_snps_start)+' SNPs did not meet the MAF threshold')
	print(str(SNP_data.shape[1])+'/'+str(SNP_in)+' SNPs included in this GWAS')
	END_load=datetime.now()
	print('Files were loaded in '+str(END_load-START))

if GLM:
	START=datetime.now()
	test=[]
	print('_____________________________________________________________')
	print('Starting the computation of GLM')
	for i in range(SNP_data.shape[1]):
		test_data=np.transpose((SNP_data[:,i],Pheno_data))
		test_data=test_data[~np.isnan(test_data).any(axis=1)]
		try:
			slope, intercept, r_value, p_value, std_err = stats.linregress(test_data[:,0],test_data[:,1])
		except ValueError:
			slope, p_value = float('nan'), float('nan')
		test.append([SNP_names[i,0],SNP_names[i,1],slope,p_value,MAF[i]])
		sys.stdout.write('\r')
		sys.stdout.write("[%-20s] %d%%" % ('='*(100*i/SNP_in), 100*i/SNP_in))
		sys.stdout.flush()

	test=np.array(test)
	header='Chromosome,Position,beta,p-val,MAF'
	filename="GWAS_GLM_for_"+str(sys.argv[1]).split(".")[0]+"_"+str(sys.argv[2]).split("_")[2].replace(".npy","")+".csv"
	np.savetxt(filename, test, delimiter=",",header=header,fmt="%s")
	END_GLM=datetime.now()
	print('\rGLM completed in '+str(END_GLM-START))

if KW:
	START=datetime.now()
	test_H=[]
	test_p=[]
	print('_____________________________________________________________')
	print('Starting the computation of KW')
	for i in range(SNP_data.shape[1]):
		H, p_value = stats.kruskal(Pheno_data[SNP_data[:,i]==0],Pheno_data[SNP_data[:,i]==1])
		test_H.append(H)
		test_p.append(p_value)
		sys.stdout.write('\r')
		sys.stdout.write("[%-20s] %d%%" % ('='*(100*i/SNP_in), 100*i/SNP_in))
		sys.stdout.flush()

	test=np.vstack((SNP_names[:,0].astype("int"),SNP_names[:,1].astype("int"),np.array(test_H),np.array(test_p),MAF))
	test=np.transpose(test)
	header='Chromosome,Position,H,p-val,MAF'
	filename="GWAS_KW_for_"+str(sys.argv[1]).split(".")[0]+"_"+str(sys.argv[2]).split("_")[2].replace(".npy","")+".csv"
	np.savetxt(filename, test, delimiter=",",header=header,fmt="%s")
	END_KW=datetime.now()
	print('\rKW completed in '+str(END_KW-START))


if EMMAX:
	START=datetime.now()
	print('_____________________________________________________________')
	print('Starting the computation of EMMAX using mixmogam')
	mm_results = lm.emmax(np.transpose(SNP_data), Pheno_data, K_data,with_betas=True)
	betas=mm_results['betas']
	betas=[[0,0] if i is None else i for i in betas]
	betas=[[0,0] if len(i)!=2 else i for i in betas]
	betas=np.hstack(betas).reshape(len(betas),2)
	test=np.vstack((SNP_names[:,0].astype("int"),SNP_names[:,1].astype("int"),betas[:,1],mm_results['ps'],MAF))
	test=np.transpose(test)
	header='Chromosome,Position,beta,p-val,MAF'
	filename="GWAS_EMMAX_for_"+str(sys.argv[1]).split(".")[0]+"_"+str(sys.argv[2]).split("_")[2].replace(".npy","")+".csv"
	np.savetxt(filename, test, delimiter=",",header=header,fmt=["%i","%i","%s","%s","%s"])
	END_EMMAX=datetime.now()
	print('\rEMMAX completed in '+str(END_EMMAX-START))

