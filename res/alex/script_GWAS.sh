#!/bin/bash

#usage is: bash script_GWAS.sh Pheno.csv Method p(Thresh) window_bp

parallel --gnu -j 12 'python -W ignore script_GWAS_parallel.py {2} {1} {3}' ::: `ls 1001genomes_SNPdata*.npy` ::: $1 ::: $2

printf Chromosome,Position,beta,p-val,MAF\\n > GWAS_$2_${1/.csv/}_out.csv

for FILE in GWAS_$2_*part*; do
	sed '1d' $FILE >> GWAS_$2_${1/.csv/}_out.csv
	rm $FILE
done

OUT=GWAS_$2_${1/.csv/}_out.csv

grep -v ',nan,' $OUT > TMP && mv TMP $OUT

Thresh=$3

if [[ $3 = *erm* ]]; then
    mkdir ${1/.csv/}_Perm
    cd ${1/.csv/}_Perm
    for p in {1..10}; do
        head -n 1 ../${1} > HD
        tail -n +2 ../${1} > TMP
        cut -f1 -d, TMP > TMP1
        cut -f2 -d, TMP | shuf > TMP2
        paste -d, TMP1 TMP2 >> HD
        parallel --gnu -j 12 'python ../script_GWAS_parallel.py {2} {1} {3}' ::: `ls ../1001genomes_SNPdata*.npy` ::: HD ::: $2
        for FILE in GWAS_$2_*part*; do
            cut -d, -f4 $FILE | tail -n +2 | grep -v 'nan'| grep -v 'p-val' | awk '$1!=0' >> PermPvals.txt
            rm $FILE
        done
    done
    WC=$(wc -l PermPvals.txt | cut -d' ' -f1)
    Thresh=$(sort -n PermPvals.txt | head -n $(($WC/1000)) | tail -n 1)
    echo "empirical Threshold with risk 0.1% is $Thresh"
    cd ../
    rm -r ${1/.csv/}_Perm/*
    rmdir ${1/.csv/}_Perm
fi

Rscript script_for_manhattan.r $OUT $Thresh $4

cat ${OUT/_out.csv/_TopSNPs.csv} | cut -f6 -d, | tail -n +2 > ${OUT/.csv/.loci}

printf "Gene\n%.0s" > ${OUT/.csv/.genes}

while read g; do
	if [ -z "$g" ]; then
		printf "\n%.0s" >> ${OUT/.csv/.genes}
	else
		gnames=""
		IFS=';' read -ra loci <<< "$g"
		for i in "${loci[@]}"; do
			gene=$(grep `echo $i` TAIR10_functional_descriptions.bk | cut -d$'\t' -f3 | sort | uniq )
			gnames+="$gene;"
		done;
		echo $gnames >> ${OUT/.csv/.genes}
	fi
done < "${OUT/.csv/.loci}"

paste ${OUT/_out.csv/_TopSNPs.csv} ${OUT/.csv/.genes} -d, > ${OUT/.csv/.tmp}
mv ${OUT/.csv/.tmp} ${OUT/_out.csv/_TopSNPs.csv}
rm ${OUT/.csv/.loci} ${OUT/.csv/.genes}
