#!/usr/bin/Rscript

args=commandArgs(TRUE)

manhattan=function (x, chr = "CHR", bp = "BP", p = "P", snp = "SNP", col = c("gray10", 
    "gray60"), chrlabs = NULL, suggestiveline = -log10(Thresh), 
    genomewideline = -log10(5e-08), highlight = NULL, logp = TRUE, 
    ...) 
{
    CHR = BP = P = index = NULL
    if (!(chr %in% names(x))) 
        stop(paste("Column", chr, "not found!"))
    if (!(bp %in% names(x))) 
        stop(paste("Column", bp, "not found!"))
    if (!(p %in% names(x))) 
        stop(paste("Column", p, "not found!"))
    if (!is.numeric(x[[chr]])) 
        stop(paste(chr, "column should be numeric. Do you have 'X', 'Y', 'MT', etc? If so change to numbers and try again."))
    if (!is.numeric(x[[bp]])) 
        stop(paste(bp, "column should be numeric."))
    if (!is.numeric(x[[p]])) 
        stop(paste(p, "column should be numeric."))
    d = data.frame(CHR = x[[chr]], BP = x[[bp]], P = x[[p]])
    if (!is.null(x[[snp]])) 
        d = transform(d, SNP = x[[snp]])
    d <- subset(d, (is.numeric(CHR) & is.numeric(BP) & is.numeric(P)))
    d <- d[order(d$CHR, d$BP), ]
    if (logp) {
        d$logp <- -log10(d$P)
    }
    else {
        d$logp <- d$P
    }
    d$pos = NA
    d$index = NA
    ind = 0
    for (i in unique(d$CHR)) {
        ind = ind + 1
        d[d$CHR == i, ]$index = ind
    }
    nchr = length(unique(d$CHR))
    if (nchr == 1) {
        options(scipen = 999)
        d$pos = d$BP/1e+06
        ticks = floor(length(d$pos))/2 + 1
        xlabel = paste("Chromosome", unique(d$CHR), "position(Mb)")
        labs = ticks
    }
    else {
        lastbase = 0
        ticks = NULL
        for (i in unique(d$index)) {
            if (i == 1) {
                d[d$index == i, ]$pos = d[d$index == i, ]$BP
            }
            else {
                lastbase = lastbase + tail(subset(d, index == 
                  i - 1)$BP, 1)
                d[d$index == i, ]$pos = d[d$index == i, ]$BP + 
                  lastbase
            }
            ticks = c(ticks, (min(d[d$CHR == i, ]$pos) + max(d[d$CHR == 
                i, ]$pos))/2 + 1)
        }
        xlabel = "Chromosome"
        labs <- unique(d$CHR)
    }
    xmax = ceiling(max(d$pos) * 1.03)
    xmin = floor(max(d$pos) * -0.03)
    def_args <- list(xaxt = "n", bty = "n", xaxs = "i", yaxs = "i", 
        las = 1, pch = 20, xlim = c(xmin, xmax), ylim = c(0, 
            ceiling(max(d$logp))), xlab = xlabel, ylab = expression(-log[10](italic(p))))
    dotargs <- list(...)
    do.call("plot", c(NA, dotargs, def_args[!names(def_args) %in% 
        names(dotargs)]))
    if (!is.null(chrlabs)) {
        if (is.character(chrlabs)) {
            if (length(chrlabs) == length(labs)) {
                labs <- chrlabs
            }
            else {
                warning("You're trying to specify chromosome labels but the number of labels != number of chromosomes.")
            }
        }
        else {
            warning("If you're trying to specify chromosome labels, chrlabs must be a character vector")
        }
    }
    if (nchr == 1) {
        axis(1, ...)
    }
    else {
        axis(1, at = ticks, labels = labs, ...)
    }
    col = rep(col, max(d$CHR))
    if (nchr == 1) {
        with(d, points(pos, logp, pch = 20, col = col[1], ...))
    }
    else {
        icol = 1
        for (i in unique(d$index)) {
            with(d[d$index == unique(d$index)[i], ], points(pos, 
                logp, col = col[icol], pch = 20, ...))
            icol = icol + 1
        }
    }
    if (suggestiveline) 
        abline(h = suggestiveline, col = "blue")
    if (genomewideline) 
        abline(h = genomewideline, col = "red")
    if (!is.null(highlight)) {
        if (any(!(highlight %in% d$SNP))) 
            warning("You're trying to highlight SNPs that don't exist in your results.")
        d.highlight = d[which(d$SNP %in% highlight), ]
        with(d.highlight, points(pos, logp, col = "green3", pch = 20, 
            ...))
    }
}

qq=function (pvector, ...) {
    if (!is.numeric(pvector)) 
        stop("Input must be numeric.")
    pvector <- pvector[!is.na(pvector) & !is.nan(pvector) & !is.null(pvector) & 
        is.finite(pvector) & pvector < 1 & pvector > 0]
    o = -log10(sort(pvector, decreasing = FALSE))
    e = -log10(ppoints(length(pvector)))
    def_args <- list(pch = 20, xlim = c(0, max(e)), ylim = c(0, 
        max(o)), xlab = expression(Expected ~ ~-log[10](italic(p))), 
        ylab = expression(Observed ~ ~-log[10](italic(p))))
    dotargs <- list(...)
    tryCatch(do.call("plot", c(list(x = e, y = o), def_args[!names(def_args) %in% 
        names(dotargs)], dotargs)), warn = stop)
    abline(0, 1, col = "red")
}



OUT=read.csv(args[1],header=T)
Window=as.numeric(args[3])

chr.lab=c("Chr1","Chr2","Chr3","Chr4","Chr5")
names(OUT)=c("CHR","BP","Beta","P","MAF")
OUT$CHR=as.numeric(as.factor(OUT$CHR))
OUT$BP=as.numeric(as.character(OUT$BP))
OUT$P=as.numeric(as.character(OUT$P))
if (!is.na(as.numeric(args[2]))) {
    Thresh=as.numeric(args[2])
} else {
Padj=p.adjust(OUT$P,method="fdr")
q=as.numeric(gsub("%","",gsub("FDR=","",args[2])))/100
if (sum(Padj<q)==0) Thresh=1e-16
else Thresh=max(OUT$P[Padj<q])
}
png(file=gsub("_out.csv",".png",args[1]),width=1200,height=400)
manhattan(OUT, main=gsub("GWAS_","",gsub("_out.csv","",args[1])),
          col = c("#104E8B","#ADD8E6"),
          chrlabs = chr.lab,
          suggestiveline=F,
          genomewideline = -log10(Thresh),
          highlight = NULL,
          logp = TRUE
          ) 
dev.off()

png(file=paste("qq_",gsub("_out.csv",".png",args[1]),sep=""),width=400,height=400)
qq(OUT$P,main=gsub("GWAS_","",gsub("_out.csv","",args[1])))
dev.off()

OUT.top=OUT[OUT$P<Thresh,]
if (nrow(OUT.top)<1) quit()

GFF=read.table("TAIR10_GFF3_genes.gff",header=F)
GFF=GFF[GFF[,3]=="gene",]

candidate.list=c()
for (i in 1:nrow(OUT.top)) {
    candidate=unlist(strsplit(as.character(GFF[GFF[,1]==paste("Chr",OUT.top[i,1],sep="")
                            & GFF[,4]<=(OUT.top[i,2]-Window/2)
                            & GFF[,5]>=(OUT.top[i,2]-Window/2),9]),";"))
    candidate=candidate[grep("ID=",candidate)]
    candidate=paste(gsub("ID=","",candidate),collapse=";")
    if (length(candidate)==0) candidate="no gene"
    candidate.list=c(candidate.list,candidate)
}

OUT.top$Locus=candidate.list
write.csv(OUT.top,file=gsub("_out.csv","_TopSNPs.csv",args[1]),row.names=F,quote=F)

