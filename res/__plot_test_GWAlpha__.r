#!/usr/bin/env Rscript
#for testing:
	model=0
	n = 500
	l = 10000
	m = l
	q = 10
	t = q
	k = 1
	trait = 0
	fileNamesSuffix = "Full_Model"
	iterations = 20

source("simulate_genotypes_transcripts_phenotypes.r")
source("GWAS_TWAS.r")
source("GWAlpha.r")
source("TWAlpha.r")
source("calculate_ROC_AUC.r")

AUC_GWAS = c()
AUC_TWAS = c()
AUC_GWAlpha_1 = c()
AUC_TWAlpha_1 = c()
AUC_GWAlpha_2 = c()
AUC_TWAlpha_2 = c()

for (iter in 1:iterations)
{
	#################################
	###							  ###
	### S I M U L A T E   D A T A ###
	###							  ### simulate genomes, transcriptomes and phenotypes
	################################# LD simulated
	SIMULATED = simulate_genomes_transcriptomes_phenotypes(model, n, l, m, q, t, k)
	X = SIMULATED$X
	T = SIMULATED$T
	Y = SIMULATED$Y
	qtl.loc = SIMULATED$qtl.loc
	qtl.additive = SIMULATED$qtl.additive
	tra.loc = SIMULATED$tra.loc
	tra.additive = SIMULATED$tra.additive
	
	qtl.labels = matrix(0, nrow=l, ncol=1)
	qtl.labels[qtl.loc,1] = 1
	tra.labels = matrix(0, nrow=m, ncol=1)
	tra.labels[tra.loc,1] = 1

	# plot(density(Y))	# ~normally distributed
	# plot(density(X))	# 3 peaks: 0, 1, 2
	# plot(density(T))	# ~chi-square distributed - folded normal dist'n

	#################################
	###							  ###
	###		G W A l p h a _ 1	  ###
	###							  ### pooled genotypes
	################################# individual phenotypes
	# (1) PHENOTYPING
	#arrange by phenotypic values
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	ALL = ALL[order(ALL$Y), ]
	PHEN = matrix(ALL[, 2])
	GENO = ALL[, (2+1):(2+m)]
	TRAN = ALL[, (2+m+1):(2+m+m)]
	#pooling: make sure mod(n, npool) == 0
	npool = round(n/100) #set the number of individuals per pool to 100 and make sure the number of pools is a natural number > 1
	for (i in 1:npool) {
		index1 = ((n/npool)*(i-1)) + 1
		index2 = (n/npool)*i
		assign(sprintf("PHEN_pool.%02d", i), PHEN[index1:index2,])
		assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
		assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])
	}
	perc = seq(0, 1, by=1/npool); perc = perc[2:(length(perc)-1)]

	# (2) GENOTYPING
	# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
	SYNC = c()
	for (p in ls()[grepl("GENO_pool.", ls())]) {
		p = eval(parse(text=p))
		sync.A = colSums(p)
		sync.T = (2*nrow(p)) - sync.A
		sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
		sync = apply(sync, 1, paste, collapse=":")
		SYNC = cbind(SYNC, sync)
	}
	SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)

	# (3) GWAlpha
	SD = sqrt(var(PHEN[,1]))
	MIN = min(PHEN[,1])
	MAX = max(PHEN[,1])
	PERC = perc
	ALPHA_GWAlpha_1 = GWAlpha(SYNC, SD, MIN, MAX, PERC)
	ROC_GWAlpha_1 = simple_roc.bobHorton(qtl.labels, ALPHA_GWAlpha_1)
	AUC_GWAlpha_1 = c(AUC_GWAlpha_1, simple_auc.jef(ROC_GWAlpha_1$TPR, ROC_GWAlpha_1$FPR))

	jpeg(paste("manhattan-", iter, ".jpeg", sep=""), quality=100, width=1400, height=700)
	par(mar=c(5,5,4,2))
	plot(1:length(ALPHA_GWAlpha_1), ALPHA_GWAlpha_1, type="p", pch=20, col="gray", ylab="alpha", xlab="SNP ID", cex=2, cex.axis=2, cex.lab=2)
	points(qtl.loc, ALPHA_GWAlpha_1[qtl.loc], type="p", pch=19, col="red", cex=3)
	dev.off()

	jpeg(paste("ROC-", iter, ".jpeg", sep=""), quality=100, width=700, height=700)
	par(mar=c(5,5,4,2))
	plot(ROC_GWAlpha_1$FPR, ROC_GWAlpha_1$TPR, type="l", lwd=5, col="red", xlab="False Positive Rate", ylab="True Positive Rate", cex=5, cex.axis=2, cex.lab=2)
	legend("bottomright", legend=paste("AUC=", round(AUC_GWAlpha_1[iter], 2), sep=""), cex=3)
	dev.off()

	#clean-up:
	rm(list=ls()[grepl("PHEN_pool.", ls())])
	rm(list=ls()[grepl("GENO_pool.", ls())])
	rm(list=ls()[grepl("TRAN_pool.", ls())])

	print(iter)
}