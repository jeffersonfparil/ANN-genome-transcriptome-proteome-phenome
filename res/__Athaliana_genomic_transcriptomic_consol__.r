#!/usr/bin/env Rscript

library(plyr) #for join() function for fast merging-include-all

DIR="/data/Lolium/Quantitative_Genetics/At_empirical"

###################################
###								###
### CONSOLIDATE TRANSCRIPTOMES  ###
###								###
###################################
system(paste0("cd ", DIR, ";
		mkdir TRANSCRIPTOMES;
		cd TRANSCRIPTOMES;
		wget -O GSE43858_RAW.tar 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE43858&format=file';
		tar xvf GSE43858_RAW.tar;
		rm GSE43858_RAW.tar;
		gunzip *.tsv.gz"))

setwd(paste0(DIR, "/TRANSCRIPTOMES"))
input_files = system("find | grep .tsv", intern=T)
input_files = gsub("\\./", "", input_files)

nrow_tran=0; biggest_tran=""
counter = 0; pb = txtProgressBar(min=0, max=length(input_files), initial=counter, style=3)
for (fname in input_files){
	counter = counter+1; setTxtProgressBar(pb, value=counter)
	id = gsub("_expression.tsv", "", substr(fname, 12, nchar(fname)))
	dat = read.table(fname, header=TRUE)
	ID = rep(id, times=nrow(dat))
	TRAN = dat$tracking_id
	FPKM = dat$fpkm
	eval(parse(text=paste0("DATA_", id, " = data.frame(ID, TRAN, FPKM)")))
	if(nrow(dat)>nrow_tran){biggest_tran=paste0("DATA_", id); nrow_tran=nrow(dat)}
}
close(pb)

TRANSCRIPTOMES = eval(parse(text=paste0(biggest_tran, "[,2:3]"))) #TRAN, FPKM columns
colnames(TRANSCRIPTOMES) = c("TRAN_ID", gsub("DATA_", "", biggest_tran))
rownames(TRANSCRIPTOMES) = TRANSCRIPTOMES$TRAN_ID

tran_list = ls()[grep("DATA_", ls())]
tran_list = tran_list[tran_list!=biggest_tran]
counter = 0; pb = txtProgressBar(min=0, max=length(tran_list), initial=counter, style=3)
for (i in tran_list) {
	counter = counter+1; setTxtProgressBar(pb, value=counter)
	DATA = eval(parse(text=i))
	VECTOR_TRAN = data.frame(DATA$FPKM)
	rownames(VECTOR_TRAN) = DATA$TRAN
	TRANSCRIPTOMES = merge(TRANSCRIPTOMES, VECTOR_TRAN, by="row.names")
	TRANSCRIPTOMES = TRANSCRIPTOMES[,2:ncol(TRANSCRIPTOMES)]
	colnames(TRANSCRIPTOMES) = c(colnames(TRANSCRIPTOMES)[1:(ncol(TRANSCRIPTOMES)-1)], gsub("DATA_", "", i))
	rownames(TRANSCRIPTOMES) = TRANSCRIPTOMES$TRAN_ID
}
close(pb)
saveRDS(TRANSCRIPTOMES, file="At_transcriptomes.rds")

#clean-up
rm(list=ls()[ls()!="DIR"])
system(paste0("cd ", DIR, "/TRANSCRIPTOMES/;
		mkdir TSV;
		mv *.tsv TSV/"))

###########################
###						###
### CONSOLIDATE GENOMES ###
###						###
###########################

#download files and use only the filtered genotype data
system(paste0("cd ", DIR, ";
		mkdir GENOMES;
		cd GENOMES;
		wget --recursive --no-parent http://1001genomes.org/data/Salk/releases/current/TAIR10/strains/;
		mv 1001genomes.org/data/Salk/releases/current/TAIR10/strains/*/quality_variant_filtered_*.txt.gz .;
		rm -R 1001genomes.org;
		gunzip *.gz;
		mv quality_variant_filtered_Li_2:1.txt quality_variant_filtered_Li_2.txt;
		rm quality_variant_filtered_Alst_1.txt"))

#list all genotype files
setwd(paste0(DIR, "/GENOMES"))
input_files = system("find | grep quality_variant_filtered_", intern=T)
input_files = gsub("\\./", "", input_files)

#load the genotype files
nrow_geno=0; biggest_geno=""
counter = 0; pb = txtProgressBar(min=0, max=length(input_files), initial=counter, style=3)
for (fname in input_files){
	counter = counter+1; setTxtProgressBar(pb, value=counter)
	id = gsub("quality_variant_filtered_", "", gsub(".txt", "", fname))
	dat = read.table(fname, header=FALSE)
	colnames(dat) = c("ID", "CHROM", "POS", "REF", "ALT", "QUAL", "SUP", "CON", "ALI")
	ID = rep(id, times=nrow(dat))
	LOCI = paste(dat$CHROM, dat$POS, sep="_")
	REF = dat$REF
	ALT = dat$ALT
	eval(parse(text=paste0("DATA_", id, " = data.frame(ID, LOCI, REF, ALT)")))
	if(nrow(dat)>nrow_geno){biggest_geno=paste0("DATA_", levels(dat$ID)); nrow_geno=nrow(dat)}
}
close(pb)

#set-up the first genotype with the largest number of loci
GENOMES = eval(parse(text=paste0(biggest_geno, "[,2:4]"))) #LOCI, REF, ALT columns
GENOMES$REF = as.character(GENOMES$REF); GENOMES$ALT = as.character(GENOMES$ALT)
rownames(GENOMES) = GENOMES$LOCI
colnames(GENOMES) = c("LOCI", "REF", gsub("DATA_", "", biggest_geno))

#merge genotype files by loci
geno_list = ls()[grep("DATA_", ls())]
geno_list = geno_list[geno_list!=biggest_geno]
counter = 0; pb = txtProgressBar(min=0, max=length(geno_list), initial=counter, style=3)
for (i in geno_list) {
	counter = counter+1; setTxtProgressBar(pb, value=counter)
	DATA = eval(parse(text=i))
	VECTOR_ALT = data.frame(DATA$ALT)
	VECTOR_ALT$DATA.ALT = as.character(VECTOR_ALT$DATA.ALT)
	VECTOR_ALT$LOCI = DATA$LOCI
	GENOMES = join(GENOMES, VECTOR_ALT, by="LOCI", match='all')
	colnames(GENOMES) = c(colnames(GENOMES)[1:(ncol(GENOMES)-1)], gsub("DATA_", "", i))
}
close(pb)

#set non-matching loci as REF
GENOMES[is.na(GENOMES)] = "-"

# #testing
# G = GENOMES
# GENOMES = G; GENOMES = GENOMES[1:20000,]
# saveRDS(GENOMES, file="At_genomes_immature.rds")

# #################################### not working
# #TRYING TO IMPROVE SPEED WITH Rcpp
# TEST = GENOMES
# TEST = TEST[,3:ncol(TEST)]
# TEST[TEST=="-"] = 0
# TEST[TEST=="A"] = 1
# TEST[TEST=="T"] = 2
# TEST[TEST=="C"] = 3
# TEST[TEST=="G"] = 4
# TEST = sapply(TEST, as.integer)
# library(Rcpp)
# sourceCpp("../SRC/__Athaliana_genomic_transcriptomic_consol__.cpp")
# ####################################

#filtering biallelic sites #will take a while ....
biallelic = c()
nloci = nrow(GENOMES)
nind = ncol(GENOMES)
pb = txtProgressBar(min=0, max=nloci, initial=0, style=3)
for (i in 1:nloci){
	setTxtProgressBar(pb, value=i)
	if(length(unique(unlist(GENOMES[i,3:nind])))==2){
		biallelic = c(biallelic, i)
	}
}
close(pb)

GENOMES = GENOMES[biallelic,]
NREF = rep(0, times=nrow(GENOMES))
REST = (GENOMES[,3]!=GENOMES[,4:ncol(GENOMES)])*1
GENOMES = data.frame(GENOMES$LOCI, NREF, REST)
colnames(GENOMES) = c("LOCI", gsub("DATA_", "", biggest_geno), colnames(REST))

saveRDS(GENOMES, file="At_genomes.rds")

#clean-up
rm(list=ls()[ls()!="DIR"])
system(paste0("cd ", DIR, "/GENOMES/;
		mkdir TXT;
		mv *.txt TXT/"))


#######################
###					###
###  MISCELLANEOUS	###
###					###
#######################

#looking at LD
setwd(paste0(DIR, "/GENOMES"))
GENOMES = readRDS("At_genomes.rds")
nloci=nrow(GENOMES)
distance=c()
LD = c()
X.sub = t(GENOMES[1:round(nloci/2500), 2:ncol(GENOMES)])
l.sub = ncol(X.sub)
x.mean = colMeans(X.sub)
progressBar = txtProgressBar(min = 0, max = ( ((l.sub^2)-l.sub) / 2 ), initial = 0, style=3, width=50); counter = 0
for (i in 1:(l.sub-1)) {
	for (j in (i+1):l.sub) {
		distance = c(distance, abs(i-j))
		p1 = x.mean[i]
		p2 = 1-p1
		q1 = x.mean[j]
		q2 = 1-q1
		f11 = mean(X.sub[,i]==0 & X.sub[,j]==0)
		f22 = mean(X.sub[,i]==1 & X.sub[,j]==1)
		f12 = mean(X.sub[,i]==0 & X.sub[,j]==1)
		f21 = mean(X.sub[,i]==1 & X.sub[,j]==0)
		D = (f11*f22) - (f12*f21)
		r = D/sqrt(p1*p2*q1*q2)
		r2 = r^2
		LD = c(LD, r2)
		# LD = c(LD, cor(X.sub[,i], X.sub[,j]))
		setTxtProgressBar(progressBar, counter); counter = counter + 1
	}
}
close(progressBar)

#LD plot
jpeg("LD_plot_Athaliana.jpg", quality=100, width=700, height=500)
	plot(distance, LD, xlab="Distance Proxy", ylab=expression(r^2), main="Empirical LD in Arabidopsis")
dev.off()

#allele frequencies
jpeg("Allele_freq_Athaliana.jpg", quality=100, width=700, height=500)
	hist(rowMeans(GENOMES[,2:ncol(GENOMES)]), xlab="Allele Frequency", ylab="Frequency", main="Empirical Allele Frequencies in Arabidopsis")
dev.off()

#transctipt abundance
setwd(paste0(DIR, "/TRANSCRIPTOMES/"))
dat = readRDS("At_transcriptomes.rds")
jpeg("Transcript_Abundance_Athaliana.jpg", quality=100, width=1400, height=500)
	par(mfrow=c(2,3))
	for (i in 1:6){
		hist(unlist(dat[i,2:ncol(dat)]), breaks=10, ylab="Frequency", xlab="Transcript Abundance",main=dat$TRAN_ID[i])
	}
dev.off()
