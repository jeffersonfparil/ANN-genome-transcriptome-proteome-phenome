#!/bin/bash

if [ $1 == -h ] || [ $1 == --help ]; then
	echo ""
	echo "------------------------------------------------------------------------------"
	echo ""
	echo "SIMULATE GENOTYPES, TRANSCRIPT ABUNDANCE, AND PHENOTYPES"
	echo "For Testing Causal SNP (QTL) and Transcript (QTT) Inference through:"
	echo ""
	echo "GWAS 		- individual genotypes 		+ individual phenotypes (LS)"
	echo "TWAS 		- individual transcripts 	+ individual phenotypes (LS)"
	echo "GWAlpha_1	- pooled genotypes 		+ individual phenotypes (ML)"
	echo "TWAlpha_1	- pooled transcripts 		+ individual phenotypes (ML)"
	echo "GWAlpha_2	- pooled genotypes 		+ pooled phenotypes (ML)"
	echo "TWAlpha_2	- pooled transcripts 		+ pooled phenotypes (ML)"
	echo ""
	echo "------------------------------------------------------------------------------"
	echo ""
	echo "Argument 1 = how many parallel processes you want to run"
	echo "Argument 2 = how many iterations per process you want"
	echo ""
	echo "Example for 10 parallel processes and 50 iterations per process:"
	echo "nohup ./parallelize_main.sh 10 50 &"
	echo ""
else
	parallel ./main.r 1 500 1000 1000 10 10 1 1 {} $2 ::: $(seq 1 1 $1)
	#arguments:
	#(1) 1=fully multiplicative; 2=additive-multiplicative
	#(2) number of individuals
	#(3) number of loci
	#(4) number of transcripts = number of loci
	#(5) number of causal loci or QTL (can sampled from some distribution)
	#(6) number of causal transcripts (can sampled from some distribution)
	#(7) Verror / Vmodel
	#(8) trait state (only for GWAlpha_2-pooled phenotyping): 0=quantitative mean & 1=binary survival rate
	#(9) output filenames suffix
	#(10) number of simulation iterations

	#combine them all
	cat OUT* | sed '/"AUC_GWAS","AUC_TWAS","AUC_GWAlpha_1","AUC_TWAlpha_1","AUC_GWAlpha_2","AUC_TWAlpha_2"*$/d' > OUT.temp 
	cat <(head -n1 OUT*-1.csv) OUT.temp > ALL_OUT.csv
	rm OUT*
fi
