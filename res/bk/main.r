#!/usr/bin/env Rscript
# production code to simulate: (1) individual geno & pheno (GWAS), (2) pooled geno & individual pheno (GWAplha), and (3) pooled geno & pheno (GWAlpha)
# parallelize me via: "parallelize_main.sh"
#model1: Y1 = XKABj + e; model2: Y2 = XC1 + XKABj + e;

############
### INPUTS
############
args = commandArgs(trailingOnly=FALSE)
#R.DIR = args[1]
#slave = args[2]
#restore = args[3]
src_dir = sub("--file=", "", dirname(args[4])) #directory where the source code of the functions are located
#--args = args[5]
model = as.integer(args[6])			#0=gene effects alone; 1=fully multiplicative; 2=additive-multiplicative
n = as.integer(args[7])				#number of individuals
l = as.integer(args[8]) 			#number of loci
m = as.integer(args[9]) 			#number of transcripts (== number of loci)
q = as.integer(args[10])			#number of causal loci or QTL (can sampled from some distribution)
t = as.integer(args[11]) 			#number of causal transcripts (can sampled from some distribution)
k = as.numeric(args[12])			#Verror / Vmodel
fileNamesSuffix = args[13]			#output filenames suffix
iterations = as.integer(args[14])	#number of simulation iterations
empirical = as.integer(args[15])	#use Arabidopsis thaliana empirical genomic and transcriptomic data?: 0=No(simulate); 1=yes(use the empirical data)

#############
### OUTPUTS
#############
# An output folder will be created in the current working directory called OUT_<fileNamesSuffix>

#############
### EXAMPLE
#############
#Example run: ./main.r 0 500 1000 1000 10 10 1 Test 2 0

#############
### TESTING
#############
	model=0
	# n = 500
	# l = 1000
	# m = l
	# q = 10
	# t = q
	# h2 = 0.5
	# fileNamesSuffix = "Full_Model"
	# iterations = 2
	src_dir = getwd()
	iter =1
	src_dir = "~/SOFTWARES/ANN-genome-transcriptome-proteome-phenome"

#############################
######## DEFINE FUNCTIONS
#############################
source(paste0(src_dir, "/GWAS_TWAS.r"))
source(paste0(src_dir, "/GWAlpha.r"))
source(paste0(src_dir, "/TWAlpha.r"))
source(paste0(src_dir, "/calculate_ROC_AUC.r"))
source(paste0(src_dir, "/plot_manhattan_ROC.r"))
source(paste0(src_dir, "/plot_LD.r"))
#END OF FUNCTION DEFINITIONS#

#############################
####### SET OUTPUT DIRECTORY
#############################
system(paste0("mkdir OUT_", fileNamesSuffix, "/"))
out_dir = system(paste0("echo OUT_", fileNamesSuffix, "/"), intern=TRUE)
setwd(out_dir)

#############################
####### EXECUTION SCRIPT
#############################
AUC_GWAS = c(); PER_GWAS = c()
AUC_TWAS = c(); PER_TWAS = c()
AUC_GWAlpha_1 = c(); PER_GWAlpha_1 = c()
AUC_TWAbeta = c(); PER_TWAbeta = c()
# AUC_TWAlpha_1 = c(); PER_TWAlpha_1 = c()
#AUC_GWAlpha_2 = c(); PER_GWAlpha_2 = c() #original GWAlpha: GWAlpha.py

for (iter in 1:iterations)
{
	print("#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#")
	print(paste0("Iteration ", iter, " of ", iterations))
	print("#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#")

	####################################
	###						  	  	 ###
	### E M P I R I C A L    D A T A ###
	###							  	 ### 
	#################################### load empirical Arabidopsis data X(138x636,658) & T(138x33,554)
	GENOMES = readRDS("/volume1/Salk_Athaliana_genomes_transcriptomes/GENOMES/At_genomes.rds")
	TRANSCRIPTOMES = readRDS("/volume1/Salk_Athaliana_genomes_transcriptomes/TRANSCRIPTOMES/At_transcriptomes.rds")
	X.temp = t(GENOMES[,2:ncol(GENOMES)]); X.temp = X.temp[order(rownames(X.temp)),]
	T.temp = t(TRANSCRIPTOMES[,2:ncol(TRANSCRIPTOMES)]); T.temp = T.temp[order(rownames(T.temp)),]
	XT.mer = merge(X.temp, T.temp,by="row.names")
	#Genotypic data processing:
		X = as.matrix(XT.mer[,2:(ncol(X.temp)+1)]) #exclude the Row.names column
		#X too big, random sampling 2000 loci here:
		X = X[,sample(1:ncol(X), size=2000, replace=FALSE)]
		#remove monomorphic sites
		X = X[,colSums(X)!=0 & colSums(X)!=ncol(X)]
		#filter loci at >= minimum allele frequency MAF
		MAF = 0.01
		X = X[,colMeans(X)>=MAF]
		#remove highly correlated loci
		COR = cor(X)
		COR[upper.tri(COR)] = 0
		diag(COR) = 0
		X = X[, !apply(COR, 2, function(x) any(x>0.99))]
		# #plot LD
		# plot_LD(X)
	#Transcriptomic data processing:
		T = as.matrix(XT.mer[,(ncol(X.temp)+2):(ncol(X.temp)+1+ncol(T.temp))])
		#T too big, random sampling 2000 transcripts here:
		T = T[,sample(1:ncol(T), size=2000, replace=FALSE)]
		#remove transcripts with no variation
		T = T[, apply(T, 2, function(x) var(x)!=0)]
		#remove highly correlated transcripts
		COR = cor(T)
		COR[upper.tri(COR)] = 0
		diag(COR) = 0
		T = T[, !apply(COR, 2, function(x) any(x>0.99 | x<(-0.99)))]
	# #AraPheno FT10 flowering time phenotype
	#  	PHENO = read.csv("AraPheno_FT10.csv")
	#  	pheno = data.frame(Row.names=PHENO$Accession, FT=PHENO$Phenotype)
	#  	PXT = merge(pheno, XT.mer, by="Row.names")
	#  	pheno = data.frame(ACC=PXT[,1], Y=PXT[,2])
	#  	X = as.matrix(PXT[,3:(ncol(X.temp)+1)])
	#  	MAF = 0.01; X = X[,colMeans(X)>=MAF]
	# 	GWAS_LOD_EFX <- function(Y, X) {
	# 		l = ncol(X)
	# 		p_values = c()
	# 		effects = c()
	# 		progressBar = txtProgressBar(min=0, max=l, initial=0, width=50, style=3)
	# 		for (i in 1:l) {
	# 			mod = lm(Y[,1] ~ X[,i])
	# 			p_values = c(p_values, summary(mod)$coefficients[2,4])
	# 			effects = c(effects, summary(mod)$coefficients[2,1])
	# 			setTxtProgressBar(progressBar, i)
	# 		}
	# 		close(progressBar)
	# 		LOD = -log(p_values, base=10)
	# 		OUT = data.frame(LOD=LOD, EFX=effects)
	# 		return(OUT)
	# 	}
	# 	GWAS_OUT = GWAS_LOD_EFX(matrix(pheno$Y, nrow=136), X)
	# 	LOD_GWAS = GWAS_OUT$LOD
	# 	EFX_GWAS = GWAS_OUT$EFX
	# 	write.csv(GWAS_OUT, file="FLT10_empirical.csv")
	# 	jpeg("FLT10_empirical.jpg", quality=100, width=1000, height=1500)
	# 	par(mfrow=c(3,2))
	# 	plot(x=1:ncol(X), y=LOD_GWAS, xlab="Site", ylab=expression("-log"[10]*"(p-value)"))
	# 	lines(x=0:ncol(X), y=rep(-log(0.05/ncol(X), base=10), each=ncol(X)+1), lty=3)
	# 	plot(x=1:ncol(X), y=10^LOD_GWAS)
	# 	hist(EFX_GWAS)
	# 	plot(x=10^LOD_GWAS, y=EFX_GWAS)
	# 	hist(EFX_GWAS[10^LOD_GWAS>1e6])
	# 	hist(EFX_GWAS[10^LOD_GWAS>1e3])
	# 	dev.off()

	##############################################################
	###														   ###
	### S I M U L A T E    Q T L   A N D   P H E N O T Y P E S ###
	###					   Q T T							   ###
	##############################################################
	#parameters:
	n = nrow(X) #no. of individuals
	l = ncol(X) #no. of loci
	m = ncol(T) #no. of transcripts
	q = 10 #no. of QTL
	t = 10 #no. of causal transcripts
	h2 = 0.5
	if (model == 0){
		#############################
		### SNP model: y = Xb + e ###
		#############################
		qtl.loc = sample(1:l, size=q, replace=FALSE)
		qtl.labels = matrix(0, nrow=l, ncol=1)
		qtl.labels[qtl.loc,1] = 1 #SNP labels: 1 for QTL and 0 for non-QTL
		qtl.eff = rgamma(n=q, shape=10) #sample QTL effects from a gamma distribution
		b = matrix(0, nrow=l, ncol=1)
		b[qtl.loc, 1] = qtl.eff
		QTL = data.frame(SITE=1:l, QTL=qtl.labels, EFFECTS=b)
		Vmodel = var(X%*%b)
		Verror = Vmodel*(1-h2)/h2
		e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
		Y = X%*%b + e
		Y = (Y-min(Y))/(max(Y)-min(Y)) #bound between 0,1
		# #testing alternative variance partitioning:
		# Vg = 1/4 * sum( cor(X[,qtl.loc])%*%(qtl.eff%*%t(qtl.eff)) )
		# Ve = Vg*(1-h2)/h2 #INFLATES ERROR VARIANCE MAGNITUDES HIGHER!!!
		# e = matrix(rnorm(n=n, mean=0, sd=sqrt(Ve)), nrow=n, ncol=1)
		# Y = X%*%b + e
		# assess simulated data:
		par(mfrow=c(2,2))
		plot(x=1:l, y=QTL$QTL, type="l", main="QTL locations", xlab="Sites", ylab="")
		hist(QTL$EFFECTS[QTL$QTL==1], main="QTL effects", xlab="QTL effect", ylab="Frequency")
		hist(Y, main="Phenotypes", xlab="Phenotypic value", ylab="Frequency")
		hist(e, main="Error effects", xlab="Error effects", ylab="Frequency")
		} else if (model == 1) {
		###################################
		### Trancript model: y = Td + e ### I MAY NEED TO FILTER THESE TRANSCRIPTS FURTHER TO REMOVE CERTAIN DISTRIBUTIONS?!
		################################### TO MAXIMIZE TWAS POWER!!!
		qtt.loc = sample(1:m, size=t, replace=FALSE)
		qtt.labels = matrix(0, nrow=m, ncol=1)
		qtt.labels[qtt.loc,1] = 1 #Causal transcript labels: 1 for QTT and 0 for non-QTT
		qtt.eff = rgamma(n=t, shape=10) #sample QTT effects from a gamma distribution
		d = matrix(0, nrow=m, ncol=1)
		d[qtt.loc, 1] = qtt.eff
		# #tame T
		# T_t = apply(T, MARGIN=2, FUN=scale)
		# Vmodel = var(T_t%*%d)
		Vmodel = var(T%*%d)
		Verror = k*Vmodel
		e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
		# Y = T_t%*%d + e
		# #testing alternative variance partitioning:
		# Vg = 1/4 * sum( cor(T[,qtt.loc])%*%(qtt.eff%*%t(qtt.eff)) )
		# Ve = Vg*(1-h2)/h2 #INFLATES ERROR VARIANCE MAGNITUDES HIGHER!!!
		# e = matrix(rnorm(n=n, mean=0, sd=sqrt(Ve)), nrow=n, ncol=1)
		Y = T%*%d + e
		Y = (Y-min(Y))/(max(Y)-min(Y)) #bound between 0,1
		#assess simulated data:
		par(mfrow=c(2,2))
		plot(x=1:m, y=qtt.labels, type="l", main="QTT locations", xlab="Sites", ylab="")
		hist(qtt.eff, main="QTT effects", xlab="QTT effect", ylab="Frequency")
		hist(Y, main="Phenotypes", xlab="Phenotypic value", ylab="Frequency")
		hist(e, main="Error effects", xlab="Error effects", ylab="Frequency")
		} else if (model == 2) {
		#######################################################
		### SNP + Trancript Interaction model: y = XBLj + e ###
		####################################################### NOT YET WORKING!!!
		qtl.loc = sample(1:l, size=q, replace=FALSE)
		qtl.labels = matrix(0, nrow=l, ncol=1)
		qtl.labels[qtl.loc,1] = 1 #SNP labels: 1 for QTL and 0 for non-QTL
		qtl.eff = rgamma(n=q, shape=10) #sample QTL effects from a gamma distribution
		b = matrix(0, nrow=l, ncol=1)
		b[qtl.loc, 1] = qtl.eff
		B = matrix(0, nrow=l, ncol=l)
		diag(B) = b #additive genetic effects only (no inter-gene effects)
		L = solve(t(X)%*%X)%*%t(X)%*%T
		j = matrix(1, nrow=m)
		Vmodel = var(X%*%B%*%L%*%j)
		Verror = k*Vmodel
		e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
		Y = X%*%B%*%L%*%j + e
		} else {
			print("Invalid model specified.")
			print("Please choose from:")
			print("     0 = additive genetic model")
			print("     1 = additive transcript model")
			print("     2 = genetic + transcript interaction model")
		}

	#################################
	###							  ###
	###         G  W  A  S        ###
	###							  ### individual genoptypes
	################################# individual phenotypes
	print("GWAS:")
	# LOD_GWAS = GWAS_TWAS(Y, X)
	# ROC_GWAS = simple_roc.bobHorton(QTL$QTL, LOD_GWAS)
	LOD_GWAS = GWAS_TWAS(y, X)
	ALL_QTL = rep(0, times=l)
	ALL_QTL[QTL$loc] = 1
	ROC_GWAS = simple_roc.bobHorton(ALL_QTL, LOD_GWAS)
	AUC_GWAS = c(AUC_GWAS, simple_auc.jef(ROC_GWAS$TPR, ROC_GWAS$FPR))
	bonferroni.cutoff = -log(0.05/l, base=10)
	# PER_GWAS = plot_manhattan_ROC(LABELS=QTL$QTL, LOD=LOD_GWAS, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_GWAS, N=q, METHOD="GWAS", ITER=iter, CAUSAL="SNP", LOC=QTL$SITE[QTL$QTL==1], M=l, ROC=ROC_GWAS, AUC=AUC_GWAS, PLOT=TRUE)
	PER_GWAS = plot_manhattan_ROC(LABELS=ALL_QTL, LOD=LOD_GWAS, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_GWAS, N=q, METHOD="GWAS", ITER=iter, CAUSAL="SNP", LOC=QTL$loc, M=l, ROC=ROC_GWAS, AUC=AUC_GWAS, PLOT=TRUE)


	#################################
	###							  ###
	###		G W A l p h a _ 1	  ###
	###							  ### pooled genotypes
	################################# individual phenotypes
	print("GWAlpha:")
	# (1) PHENOTYPING
	#arrange by phenotypic values
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	ALL = ALL[order(ALL$Y), ]
	PHEN = matrix(ALL[, 2])
	GENO = ALL[, (2+1):(2+l)]
	TRAN = ALL[, (2+l+1):(2+l+m)]
	NPOOLS=5
	POOLS = (1:5)*floor(n/NPOOLS); POOLS[3] = POOLS[3]+(n%%NPOOLS); POOLS = c(0, POOLS)
	for (i in 1:(length(POOLS)-1)) {
		index1 = POOLS[i]+1
		index2 = POOLS[i+1]
		assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
	}
	PERC = POOLS/n; PERC = PERC[2:(length(PERC)-1)]

	# (2) GENOTYPING
	# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
	SYNC = c()
	for (p in ls()[grepl("GENO_pool.", ls())]) {
		p = eval(parse(text=p))
		sync.A = colSums(p)
		# sync.T = (2*nrow(p)) - sync.A #for heterozygote genotype coding: 0,1,2
		sync.T = (1*nrow(p)) - sync.A #for homozygote genotype coding: 0,1
		sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
		sync = apply(sync, 1, paste, collapse=":")
		SYNC = cbind(SYNC, sync)
	}
	SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)

	# (3) GWAlpha
	SD = sqrt(var(PHEN[,1]))
	MIN = min(PHEN[,1])
	MAX = max(PHEN[,1])
	PERC = PERC
	ALPHA_GWAlpha_1 = GWAlpha(SYNC, SD, MIN, MAX, PERC)
	LL = function(data, par) {sum(-log(dnorm(data, mean=par[1], sd=par[2])))}
	OPTIM = optim(par=c(0,1), fn=LL, data=ALPHA_GWAlpha_1, method="Nelder-Mead", control=list(fnscale=1, reltol=10e-8))
	mean_alpha = OPTIM$par[1]
	sd_alpha = OPTIM$par[2]
	PVAL_GWAlpha_1 = pnorm(abs(ALPHA_GWAlpha_1), mean=mean_alpha, sd=sd_alpha, lower.tail=FALSE)
	# PVAL_GWAlpha_1 = p.adjust(PVAL_GWAlpha_1, method="hochberg") #or simply use the bonferroni-corrected threshold
	LOD_GWAlpha_1 = -log(PVAL_GWAlpha_1, base=10) #used absolute value since we're integrating upto the upper tail 
	ROC_GWAlpha_1 = simple_roc.bobHorton(QTL$QTL, LOD_GWAlpha_1)
	AUC_GWAlpha_1 = c(AUC_GWAlpha_1, simple_auc.jef(ROC_GWAlpha_1$TPR, ROC_GWAlpha_1$FPR))
	PER_GWAlpha_1 = plot_manhattan_ROC(LABELS=QTL$QTL, LOD=LOD_GWAlpha_1, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_GWAlpha_1, N=q, METHOD="GWAlpha_1", ITER=iter, CAUSAL="SNP", LOC=QTL$SITE[QTL$QTL==1], M=l, ROC=ROC_GWAlpha_1, AUC=AUC_GWAlpha_1, PLOT=TRUE)

	#clean-up:
	rm(list=ls()[grepl("GENO_pool.", ls())])


	# #################################
	# ###							  ###
	# ###		G W A l p h a _ 2	  ### misc: GWAlpha.py test using actual GWAlpha.py script
	# ###							  ### 
	# #################################
	# write.table(SYNC, file="GWAlpha_test.sync", row.names=FALSE, col.names=FALSE, sep="\t", quote=FALSE)
	# Pheno_name="GWAlpha_test"
	# sig=SD
	# MIN=MIN
	# MAX=MAX
	# perc=paste0("[", paste(PERC, collapse=","), "]")
	# Q=paste0("[", paste(PHEN[POOLS[2:length(POOLS)-1]], collapse=","), "]")
	# phenpy=rbind(Pheno_name, sig, MIN, MAX, perc, Q)
	# PHENPY = paste0(c("Pheno_name=", "sig=", "MIN=", "MAX=", "perc=", "q="), phenpy, rep(";", times=nrow(phenpy)))
	# write.table(PHENPY, file="GWAlpha_test_pheno.py", row.names=FALSE, col.names=FALSE, sep="\t", quote=FALSE)
	# system("sed -i 's/GWAlpha_test/\"GWAlpha_test\"/g' GWAlpha_test_pheno.py")
	# system("/volume1/SIMULATED/SRC/GWAlpha/GWAlpha.py GWAlpha_test.sync ML")
	# gwalpha = read.csv("GWAlpha_GWAlpha_test_out.csv")
	# #filling up dropped sites with 0s
	# ALPHA_GWAlpha_2 = rep(0, times=l)
	# ALPHA_GWAlpha_2[gwalpha$Position] = gwalpha$Alpha
	# OPTIM = optim(par=c(0,1), fn=LL, data=ALPHA_GWAlpha_2)
	# mean_alpha = OPTIM$par[1]
	# sd_alpha = OPTIM$par[2]
	# LOD_GWAlpha_2 = -log(pnorm(abs(ALPHA_GWAlpha_2), mean=mean_alpha, sd=sd_alpha, lower.tail=FALSE), base=10)
	# ROC_GWAlpha_2 = simple_roc.bobHorton(QTL$QTL, LOD_GWAlpha_2)
	# AUC_GWAlpha_2 = c(AUC_GWAlpha_2, simple_auc.jef(ROC_GWAlpha_2$TPR, ROC_GWAlpha_2$FPR))
	# PER_GWAlpha_2 = plot_manhattan_ROC(LABELS=QTL$QTL, LOD=LOD_GWAlpha_2, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_GWAlpha_2, N=q, METHOD="GWAlpha_2", ITER=iter, CAUSAL="SNP", LOC=QTL$SITE[QTL$QTL==1], M=l, ROC=ROC_GWAlpha_2, AUC=AUC_GWAlpha_2, PLOT=TRUE)
	
	# #clean-up:
	# rm(list="gwalpha")
	# system("rm GWAlpha_test*")


	#################################
	###							  ###
	###         T  W  A  S        ###
	###							  ### individual RNAseq
	################################# individual phenotypes
	print("TWAS:")
	LOD_TWAS = GWAS_TWAS(Y, T)
	# LOD_TWAS = GWAS_TWAS(Y, log(T+1e-6))
	# T_t = apply(T, MARGIN=2, FUN=scale)
	# LOD_TWAS = GWAS_TWAS(Y, T_t)
	ROC_TWAS = simple_roc.bobHorton(qtt.labels, LOD_TWAS)
	AUC_TWAS = c(AUC_TWAS, simple_auc.jef(ROC_TWAS$TPR, ROC_TWAS$FPR))
	bonferroni.cutoff = -log(0.05/m, base=10)
	PER_TWAS = plot_manhattan_ROC(LABELS=qtt.labels, LOD=LOD_TWAS, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_TWAS, N=t, METHOD="TWAS", ITER=iter, CAUSAL="Transcript", LOC=qtt.loc, M=m, ROC=ROC_TWAS, AUC=AUC_TWAS, PLOT=TRUE)
	# ######################
	# ### Improving TWAS ###
	# ######################
	# #(1) How are the high LOD transcripts distributed?
	# idx_tran_interest = which(LOD_TWAS>5)
	# par(mfrow=c(1,length(idx_tran_interest)))
	# for (i in idx_tran_interest){
	# 	if(sum(qtt.loc==i)==1){name=paste0("QTT-", i)
	# 	} else {name=i}
	# 	hist(T[,i], main=name)
	# }
	# #(2) How are the QTT with low LOD distributed?
	# par(mfrow=c(1,length(qtt.loc)))
	# for(i in qtt.loc){
	# 	if(sum(idx_tran_interest==i)==1){name=paste0("DETECTED-",i)
	# 	} else {name=i}
	# 	hist(T[,i], main=name)
	# 	legend("topright", legend=c(paste0("MEAN=", round(mean(T[,i]))), paste0("SD=", round(sd(T[,i]))), paste0("SHAPIRO=", round(shapiro.test(T[,i])$p.value, 7))))
	# }


	#################################
	###							  ###
	###		T W A l p h a _ 1	  ###
	###							  ### pooled RNAseq
	################################# individual phenotypes
	print("TWAlpha:")
		# (1) PHENOTYPING
	#arrange by phenotypic values
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	ALL = ALL[order(ALL$Y, decreasing=FALSE), ]
	PHEN = matrix(ALL[, 2])
	GENO = ALL[, (2+1):(2+l)]
	TRAN = ALL[, (2+l+1):(2+l+m)]
	NPOOLS=5
	POOLS = (1:5)*floor(n/NPOOLS); POOLS[3] = POOLS[3]+(n%%NPOOLS); POOLS = c(0, POOLS)
	for (i in 1:(length(POOLS)-1)) {
		index1 = POOLS[i]+1
		index2 = POOLS[i+1]
		assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])	#(n individuals x m transcripts)
	}
	PERC = seq(0, 1, by=1/NPOOLS); PERC = PERC[2:(length(PERC)-1)]

	# (2) RNAseq
	# pooled RNAseq data (transform pooled RNAseq data ~(0,1)per transcript across pools)
	RNA = c()
	for (p in ls()[grepl("TRAN_pool.", ls())]) {
		p = eval(parse(text=p))
		RNA = cbind(RNA, colSums(p)) #(m transcripts x p pools)
	}
	# divide transcript abundance by the number of individuals per pool
	tranc = c()
	for (i in 1:NPOOLS) {
		p = eval(parse(text=ls()[grepl("TRAN_pool.", ls())][i]))
		tranc = cbind(tranc, RNA[,i]/nrow(p))
	}
	# TRANC = (TRANC-apply(TRANC, MARGIN=1, min)) + 1 # added 1 just so that we don't have 0's which results in optimization failure in TWAlpha.r +++ this improves the power!!!!20180112
	# TRANC = TRANC/rowSums(TRANC)
	TRANC = tranc/rowSums(tranc)
	
	# (3) TWAlpha: beta distribution mean comparison between transcript and pseudo-alternative distribution as rev(transcript)
	SD = sqrt(var(PHEN[,1]))
	MIN = min(PHEN[,1])
	MAX = max(PHEN[,1])
	PERC = PERC
	ALPHA_TWAlpha_1 = TWAlpha(TRANC, SD, MIN, MAX, PERC)
	LL = function(data, par) {sum(-log(dnorm(data, mean=par[1], sd=par[2])))}
	OPTIM = optim(par=c(0,1), fn=LL, data=ALPHA_TWAlpha_1)
	mean_alpha = OPTIM$par[1]
	sd_alpha = OPTIM$par[2]
	LOD_TWAlpha_1 = -log(pnorm(abs(ALPHA_TWAlpha_1), mean=mean_alpha, sd=sd_alpha, lower.tail=FALSE)) #folded the alpha's for simple pnorm upper tail only
	ROC_TWAlpha_1 = simple_roc.bobHorton(qtt.labels, LOD_TWAlpha_1)
	AUC_TWAlpha_1 = c(AUC_TWAlpha_1, simple_auc.jef(ROC_TWAlpha_1$TPR, ROC_TWAlpha_1$FPR))
	PER_TWAlpha_1 = plot_manhattan_ROC(LABELS=qtt.labels, LOD=LOD_TWAlpha_1, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_TWAlpha_1, N=t, METHOD="TWAlpha_1", ITER=iter, CAUSAL="Transcript", LOC=qtt.loc, M=m, ROC=ROC_TWAlpha_1, AUC=AUC_TWAlpha_1, PLOT=TRUE)

	#TWAbeta: linear regression per transcript
	PVAL_TWAbeta = TWAbeta(RNA)
	PVAL_TWAbeta[is.na(PVAL_TWAbeta)] = 1 #remove NAs
	LOD_TWAbeta = -log(PVAL_TWAbeta, base=10)
	ROC_TWAbeta = simple_roc.bobHorton(qtt.labels, LOD_TWAbeta)
	AUC_TWAbeta = c(AUC_TWAlpha_1, simple_auc.jef(ROC_TWAbeta$TPR, ROC_TWAbeta$FPR))
	PER_TWAbeta = plot_manhattan_ROC(LABELS=qtt.labels, LOD=LOD_TWAbeta, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_TWAbeta, N=t, METHOD="TWAlpha_1", ITER=iter, CAUSAL="Transcript", LOC=qtt.loc, M=m, ROC=ROC_TWAbeta, AUC=AUC_TWAbeta, PLOT=TRUE)

	#clean-up:
	rm(list=ls()[grepl("TRAN_pool.", ls())])
}

#########################
########################
######################
####################
#  ULTIMATE OUTPUT
####################
######################
########################
#########################

methods = ls()[grep("AUC_", ls())]

MODEL 	= rep(rep(model, times=length(AUC_GWAS)), times=length(methods))
N 		= rep(rep(n, times=length(AUC_GWAS)), times=length(methods))
L 		= rep(rep(l, times=length(AUC_GWAS)), times=length(methods))
M 		= rep(rep(m, times=length(AUC_GWAS)), times=length(methods))
Q 		= rep(rep(q, times=length(AUC_GWAS)), times=length(methods))
T 		= rep(rep(t, times=length(AUC_GWAS)), times=length(methods))
K 		= rep(rep(k, times=length(AUC_GWAS)), times=length(methods))
ITER 	= rep(seq(1, iterations, by=1), times=length(methods))
METHODS = rep(sub("AUC_", "", methods), each=length(AUC_GWAS))

AUC 		= eval(parse(text=paste0("c(", paste(ls()[grep("AUC_", ls())], collapse=","), ")")))
PERFORMANCE	= eval(parse(text=paste0("c(", paste(ls()[grep("PER_", ls())], collapse=","), ")")))

OUT = data.frame(MODEL, N, L, M, Q, T, K, ITER, METHODS, AUC, PERFORMANCE)
write.csv(OUT, file=paste("PERFORMANCE-", fileNamesSuffix, ".csv", sep=""), row.names=F)
