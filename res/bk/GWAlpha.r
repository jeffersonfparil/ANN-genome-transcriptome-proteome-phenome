#Rscript source function: synthethic pooled GWAS or GWAlpha using maximum likelihood estimation

# INPUTS:
#1	SYNC -->	matrix of allele freqencies across pools (sync file format: nloci x npools x 6 allele states: A,T,C,G,N,DEL)
#2	SD -->		standard deviation of phenotype data across all pools
#3	MIN -->		minimum phenotypic values across all pools
#4	MAX -->		maximum phenotypic values across all pools
#5	PERC -->	list of percentile values, e.g. c(0.2, 0.4, 0.6, 0.8) for 5 pools (1 x (npools-1))

# OUTPUT:
#1	ALPHA -->	list of test statistic alpha across all loci or transcripts (1xm)

# compute alpha for each loci:
compute_SNP_Alpha <- function(FREQ_MATRIX, SD, MIN, MAX, PERC) {
	#determine the size of each pool based on their percentile values
	BINS = c(PERC, 1) - c(0, PERC)
	NPOOLS = length(BINS)
	#find the major allele - that is the allele with the highest frequency across pools
	allele_means = colMeans(FREQ_MATRIX)
	allele_max_index = which.max(colSums(FREQ_MATRIX)); if(allele_max_index==1){allele="A"} else if(allele_max_index==2){allele="T"} else if(allele_max_index==3){allele="C"} else if(allele_max_index==4){allele="G"} else if(allele_max_index==5){allele="N"} else {allele="DEL"} #determine major allele identity
	FREQ_A = FREQ_MATRIX[,allele_max_index]	#potential problem here when more than 1 column or allele are equal to the maximum value!!!
	#transform the allele frequency of the major allele such that the sum of allele frequencies across pools is equal to 1
	BIN_A = FREQ_A*BINS/sum(FREQ_A*BINS); BIN_A = BIN_A/sum(BIN_A) #is dividing them again with the sum necessary???
	#transform the aggregate of the alternative alleles in the same way as in the major allele
	BIN_B = (1 - FREQ_A)*BINS/(1-sum(FREQ_A*BINS));	BIN_B = BIN_B/sum(BIN_B)
	#convert back the transformed allele frequencies into percentiles
	# PERC_A = c(); PERC_B = c()
	# for(i in 1:length(BIN_A)){
	# 	PERC_A = c(PERC_A, sum(BIN_A[1:i]))
	# 	PERC_B = c(PERC_B, sum(BIN_B[1:i]))
	# }
	PERC_A = cumsum(BIN_A); PERC_B = cumsum(BIN_B)
	#define the likelihood functions #FIX US!!!!
	LL_FUN=0
	if (LL_FUN==0){ ###@@@@@@@@@@@@@ CDF difference likelihood @@@@@@@@@@@@@###
		LOG.LIKE <- function(perc, par) {
			perc_0 = c(0, perc[1:(length(perc)-1)])
			return( -sum(log(pbeta(perc, shape1=par[1], shape2=par[2])-pbeta(perc_0, shape1=par[1], shape2=par[2]))) )
		}
		#maximize the likelihood or in this case minimize the -log likelihood
		OPTIM_PAR_A = tryCatch(optim(par=c(1,1), fn=LOG.LIKE, method="Nelder-Mead", control=list(fnscale=1, reltol=10e-8), perc=PERC_A)$par, error=function(e){return(c(1,1))}) #shape1 = shape2 = 1 --> beta == uniform
		OPTIM_PAR_B = tryCatch(optim(par=c(1,1), fn=LOG.LIKE, method="Nelder-Mead", control=list(fnscale=1, reltol=10e-8), perc=PERC_B)$par, error=function(e){return(c(1,1))})
		#compute for the mean of the beta desitribution based on the computed parameters for both major and alternative alleles
		MU_A = MIN + ((MAX-MIN)*OPTIM_PAR_A[1]/(OPTIM_PAR_A[1]+OPTIM_PAR_A[2]))
		MU_B = MIN + ((MAX-MIN)*OPTIM_PAR_B[1]/(OPTIM_PAR_B[1]+OPTIM_PAR_B[2]))
	} else if(LL_FUN==1){ ###@@@@@@@@@@@@@ PDF likelihood @@@@@@@@@@@@@###
		LOG.LIKE <- function(perc, par) {
			perc = perc[1:(length(perc)-1)] #removing 1 in the percentile list since the beta distribution is defined between 0 and 1
			return( -sum(log(dbeta(perc, shape1=par[1], shape2=par[2]))) )
		}
		OPTIM_PAR_A = tryCatch(optim(par=c(1,1), fn=LOG.LIKE, method="Nelder-Mead", control=list(fnscale=1, reltol=10e-8), perc=PERC_A)$par, error=function(e){return(c(1,1))})
		OPTIM_PAR_B = tryCatch(optim(par=c(1,1), fn=LOG.LIKE, method="Nelder-Mead", control=list(fnscale=1, reltol=10e-8), perc=PERC_B)$par, error=function(e){return(c(1,1))})
		MU_A = MIN + ((MAX-MIN)*OPTIM_PAR_A[1]/(OPTIM_PAR_A[1]+OPTIM_PAR_A[2]))
		MU_B = MIN + ((MAX-MIN)*OPTIM_PAR_B[1]/(OPTIM_PAR_B[1]+OPTIM_PAR_B[2]))
	} else if(LL_FUN==2){ ###@@@@@@@@@@@@@ CDF sum of the differences likelihood @@@@@@@@@@@@@###
		LOG.LIKE <- function(perc, par) {
			perc_A = perc[1,]
			perc_B = perc[2,]
			perc_A0 = c(0, perc_A[1:(length(perc_A)-1)])
			perc_B0 = c(0, perc_B[1:(length(perc_B)-1)])
			return( -sum(log(pbeta(perc_A, shape1=par[1], shape2=par[2])-pbeta(perc_A0, shape1=par[1], shape2=par[2]))) - sum(log(pbeta(perc_B, shape1=par[3], shape2=par[4])-pbeta(perc_B0, shape1=par[3], shape2=par[4]))) )
		}
		PERC_AB = rbind(PERC_A, PERC_B)
		OPTIM_PAR = tryCatch(optim(par=c(1,1,1,1), fn=LOG.LIKE, method="Nelder-Mead", control=list(fnscale=1, reltol=10e-8), perc=PERC_AB)$par, error=function(e){return(c(1,1,1,1))})
		MU_A = MIN + ((MAX-MIN)*OPTIM_PAR[1]/(OPTIM_PAR[1]+OPTIM_PAR[2]))
		MU_B = MIN + ((MAX-MIN)*OPTIM_PAR[3]/(OPTIM_PAR[1]+OPTIM_PAR[4]))
	}
	#compute the test statistic alpha
	# ALPHA = (MU_A - MU_B)/SD #no penalization W = 1
	pA = sum(FREQ_A*BINS)
	W = 2*sqrt(pA*(1-pA))
	ALPHA = W*(MU_A - MU_B)/SD #with penalization W = 2*sqrt(pA*(1-pA))
	OUT = data.frame(ALPHA, allele)
	return(OUT)
}


#compute alpha across all loci by calling the previous function: "compute_SNP_Alpha"
GWAlpha <- function(SYNC, SD, MIN, MAX, PERC) {
	allele = c()
	alpha = c()
	progressBar = txtProgressBar(min = 0, max = nrow(SYNC), initial = 0, style=3, width=50)
	for (loci in 1:nrow(SYNC)) {
		FREQ_MATRIX = matrix(as.numeric(unlist(strsplit(SYNC[loci,4:ncol(SYNC)], split=":"))), byrow=TRUE, nrow=NPOOLS, ncol=6)
		FREQ_MATRIX = FREQ_MATRIX / rowSums(FREQ_MATRIX)
		out = compute_SNP_Alpha(FREQ_MATRIX, SD, MIN, MAX, PERC)
		allele = c(allele, as.character(out$allele))
		alpha = c(alpha, out$ALPHA)
		setTxtProgressBar(progressBar, loci)
	}
	close(progressBar)
	# ALPHA = abs(alpha)
	ALPHA = alpha
	return(ALPHA)
}
