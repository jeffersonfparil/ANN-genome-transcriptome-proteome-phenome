#Rscript source function: synthethic pooled GWAS or GWAlpha using maximum likelihood estimation

# INPUTS:
#1	SYNC -->	matrix of allele freqencies across pools (sync file format: nloci x npools x 6 allele states: A,T,C,G,N,DEL)
#2	SD -->		standard deviation of phenotype data across all pools
#3	MIN -->		minimum phenotypic values across all pools
#4	MAX -->		maximum phenotypic values across all pools
#5	PERC -->	list of percentile values, e.g. c(0.2, 0.4, 0.6, 0.8) for 5 pools (1 x (npools-1))

# OUTPUT:
#1	ALPHA -->	list of test statistic alpha across all loci or transcripts (1xm)

simulate_causal_effects <- function()