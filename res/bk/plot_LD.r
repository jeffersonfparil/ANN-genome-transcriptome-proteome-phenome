#Rscript source function: linkage disequilibrium plot

plot_LD <- function(X) {
	nloci=nrow(X)
	distance=c()
	LD = c()
	X.sub = t(X[1:round(nloci/2500), 2:ncol(X)])
	l.sub = ncol(X.sub)
	x.mean = colMeans(X.sub)
	progressBar = txtProgressBar(min = 0, max = ( ((l.sub^2)-l.sub) / 2 ), initial = 0, style=3, width=50); counter = 0
	for (i in 1:(l.sub-1)) {
		for (j in (i+1):l.sub) {
			distance = c(distance, abs(i-j))
			p1 = x.mean[i]
			p2 = 1-p1
			q1 = x.mean[j]
			q2 = 1-q1
			f11 = mean(X.sub[,i]==0 & X.sub[,j]==0)
			f22 = mean(X.sub[,i]==1 & X.sub[,j]==1)
			f12 = mean(X.sub[,i]==0 & X.sub[,j]==1)
			f21 = mean(X.sub[,i]==1 & X.sub[,j]==0)
			D = (f11*f22) - (f12*f21)
			r = D/sqrt(p1*p2*q1*q2)
			r2 = r^2
			LD = c(LD, r2)
			# LD = c(LD, cor(X.sub[,i], X.sub[,j]))
			setTxtProgressBar(progressBar, counter); counter = counter + 1
		}
	}
	close(progressBar)

	plot(distance, LD, xlab="Distance Proxy", ylab=expression(r^2), main="Linkage Disequilibrium Plot")
}