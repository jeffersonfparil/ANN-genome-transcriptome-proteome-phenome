plot_manhattan_ROC <- function(LABELS, LOD, CUT.OFF, PERFORMANCE, N, METHOD, ITER, CAUSAL, LOC, M, ROC, AUC, PLOT=FALSE){
	qtl.qtt = (LABELS * LOD)
	found = qtl.qtt[qtl.qtt>=CUT.OFF]
	PERFORMANCE = c(PERFORMANCE, length(found)/N)

	if (PLOT==FALSE) {
	jpeg(paste0("Manhattan_plot_", METHOD, "_", ITER, ".jpeg"), width=1500, height=800)
		par(mfrow=c(1,2), cex=1.5)
		plot(1:length(LOD), LOD, type="p", pch=20, col="gray", ylab=expression("-log"[10]*"(p-value)"), xlab=paste0(CAUSAL," ID"), main=METHOD)
		points(LOC, LOD[LOC], type="p", pch=10, col="red")
		text(LOC, LOD[LOC], labels=LOC, pos=1)
		lines(x=0:M, y=rep(CUT.OFF, each=M+1), lty=3)
		plot(ROC$FPR, ROC$TPR, type="l", col="red", xlab="False Positive Rate", ylab="True Positive Rate", main=METHOD)
		legend("bottomright", legend=paste("AUC=", round(AUC[iter], 2), sep=""))
		legend("bottomleft", legend=paste("DETECTED=", round(PERFORMANCE[ITER]*100, 0), "%", sep=""))
	dev.off()
	} else {
		par(mfrow=c(1,2), cex=1.5)
		plot(1:length(LOD), LOD, type="p", pch=20, col="gray", ylab=expression("-log"[10]*"(p-value)"), xlab=paste0(CAUSAL," ID"), main=METHOD)
		points(LOC, LOD[LOC], type="p", pch=10, col="red")
		text(LOC, LOD[LOC], labels=LOC, pos=1)
		lines(x=0:M, y=rep(CUT.OFF, each=M+1), lty=3)
		plot(ROC$FPR, ROC$TPR, type="l", col="red", xlab="False Positive Rate", ylab="True Positive Rate", main=METHOD)
		legend("bottomright", legend=paste("AUC=", round(AUC[iter], 2), sep=""))
		legend("bottomleft", legend=paste("DETECTED=", round(PERFORMANCE[ITER]*100, 0), "%", sep=""))
	}

	return(PERFORMANCE)
}
