#Rscript source function: synthethic pooled TWAS or TWAlpha using maximum likelihood estimation

########### IN NEED OF GANGANTUAN EDITING!!! BUT FIRST FIX TRANSCRIPTOME SIMULATION +++>>> MAKE 'EM REALISTIC

# INPUTS:
#1	TRANC -->	matrix of transformed transcript abundance across pools (for each transcript, transformed abundance data across pools sums to 1)
#2	SD -->		standard deviation of phenotype data across all pools
#3	MIN -->		minimum phenotypic values across all pools
#4	MAX -->		maximum phenotypic values across all pools
#5	PERC -->	list of percentile values, e.g. c(0.2, 0.4, 0.6, 0.8) for 5 pools (1 x (npools-1))

# OUTPUT:
#1	ALPHA -->	list of test statistic alpha across all loci or transcripts (1xm)

#compute alpha for each loci:
compute_transcript_Alpha <- function(FREQ_MATRIX, SD, MIN, MAX, PERC) {
	#determine the size of each pool based on their percentile values
	# BINS = c(PERC, 1) - c(0, PERC)
	# #determine the size of each pool based on their percentile values
	# FREQ_A = FREQ_MATRIX
	# FREQ_B = 1-FREQ_A; FREQ_B = FREQ_B/sum(FREQ_B)
	# #transform the allele frequency of the major allele such that the sum of allele frequencies across pools is equal to 1
	# BIN_A = FREQ_A*BINS/sum(FREQ_A*BINS); BIN_A = BIN_A/sum(BIN_A) #is dividing them again with the sum necessary???
	# #transform the aggregate of the alternative alleles in the same way as in the major allele
	# BIN_B = (1 - FREQ_A)*BINS/(1-sum(FREQ_A*BINS));	BIN_B = BIN_B/sum(BIN_B)
	BIN_A = FREQ_MATRIX
	BIN_B = rev(BIN_A) # reverse order of BIN_A to simulate the pseudo-alternative transcript state ---IS THIS FINE???!!!---
	#convert back the transformed allele frequencies into percentiles
	PERC_A = c(); PERC_B = c()
	for(i in 1:length(BIN_A)){
		PERC_A = c(PERC_A, sum(BIN_A[1:i]))
		PERC_B = c(PERC_B, sum(BIN_B[1:i]))
	}
	#define the likelihood function as the cdf(yi, b1,b2) - cdf(yi-1, b1,b2) [--> for the major allele] PLUS cdf(yi, b3,b4) - cdf(yi-1, b3,b4) [--> for the altenative alleles aggregate]
	optimize_me <- function(perc, par) {
		perc_0 = c(0, perc[1:(length(perc)-1)])
		# return(-sum(log(pbeta(PERC_A, shape1=par[1], shape2=par[2])-pbeta(PERC_A0, shape1=par[1], shape2=par[2]))) - sum(log(pbeta(PERC_B, shape1=par[3], shape2=par[4])-pbeta(PERC_B0, shape1=par[3], shape2=par[4]))) )
		return(-sum(log(pbeta(perc, shape1=par[1], shape2=par[2])-pbeta(perc_0, shape1=par[1], shape2=par[2]))) )
	}
	#maximize the likelihood or in the case minimize the -log likelihood
	OPTIM_PAR_A = tryCatch(optim(par=c(1,1), fn=optimize_me, control=list(reltol=10e-8), perc=PERC_A)$par, error=function(e){return(c(1,1))})
	OPTIM_PAR_B = tryCatch(optim(par=c(1,1), fn=optimize_me, control=list(reltol=10e-8), perc=PERC_B)$par, error=function(e){return(c(1,1))})
	#compute for the mean of the beta desitribution based on the computed parameters for both major and alternative alleles
	MU_A = MIN + ((MAX-MIN)*OPTIM_PAR_A[1]/(OPTIM_PAR_A[1]+OPTIM_PAR_A[2]))
	MU_B = MIN + ((MAX-MIN)*OPTIM_PAR_B[1]/(OPTIM_PAR_B[1]+OPTIM_PAR_B[2]))
	#compute the test statistic alpha
	ALPHA = (MU_A - MU_B)/SD #no penalization W = 2*sqrt(pA*(1-pA))
	return(ALPHA)
}

#compute alpha across all loci by calling the previous function: "compute_transcript_Alpha"
TWAlpha <- function(TRANC, SD, MIN, MAX, PERC) {
	alpha = c()
	progressBar = txtProgressBar(min = 0, max = nrow(TRANC), initial = 0, style=3, width=50)
	for (loci in 1:nrow(TRANC)) {
		FREQ_MATRIX = TRANC[loci,]
		out = compute_transcript_Alpha(FREQ_MATRIX, SD, MIN, MAX, PERC)
		alpha = c(alpha, out)
		setTxtProgressBar(progressBar, loci)
	}
	close(progressBar)
	ALPHA = alpha
	return(ALPHA)
}


##################################################################################
##################################################################################
##################################################################################



#regress per transcript
TWAbeta <- function(RNA){
	beta = c()
	progressBar = txtProgressBar(min = 0, max = nrow(RNA), initial = 0, style=3, width=50)
	for (i in 1:nrow(RNA)){
		TRANSCRIPT_VECTOR = RNA[i,]
		y_t = TRANSCRIPT_VECTOR
		x_p = 1:length(y_t)
		mod = lm(y_t ~ x_p)
		pval = summary(mod)$coefficients[2,4]
		eff = summary(mod)$coefficients[2,1]
		out = data.frame(pval=pval, effects=eff)
		beta = c(beta, out$pval)
		setTxtProgressBar(progressBar, i)
	}
	close(progressBar)
	return(beta)
}

# #testing:
# # R_t = t(apply(RNA, MARGIN=1, FUN=scale))
# # R_t = R_t[complete.cases(R_t),] #remove NAs from scaling 0's
# # BETA = TWAbeta(R_t)
# BETA = TWAbeta(RNA)
# BETA = TWAlpha(TRANC, SD, MIN, MAX, PERC)
# LOD = -log(BETA, base=10)
# plot(LOD, type="p", pch=20, col="gray")
# points(qtt.loc, LOD[qtt.loc], type="p", pch=10, col="red")

# #what does our QTT look like
# QTT = data.frame(LOC=qtt.loc, EFX=qtt.eff)
# QTT = QTT[order(QTT$EFX, decreasing=TRUE),]
# par(mfrow=c(2,5))
# for (i in 1:nrow(QTT)){
# 	id = QTT$LOC[i]
# 	TRANSCRIPT_VECTOR = RNA[id,]
# 	y_t = TRANSCRIPT_VECTOR
# 	x_p = 1:length(y_t)
# 	mod = lm(y_t ~ x_p)
# 	pval = summary(mod)$coefficients[2,4]
# 	eff = summary(mod)$coefficients[2,1]
# 	r2 = summary(mod)$r.squared
# 	plot(TRANSCRIPT_VECTOR, main=id)
# 	abline(mod)
# 	legend("bottomright", legend=c( paste0("PVAL = ", round(pval, 2)),
# 									paste0("EFF = ", round(QTT$EFX[i], 2)),
# 									paste0("R2 = ", round(r2, 2))
# 			))
# }

# #what does the top transcripts look instead
# top10.loc =  order(LOD, decreasing=TRUE)[1:10]
# plot(LOD, type="p", pch=20, col="gray")
# points(qtt.loc, LOD[qtt.loc], type="p", pch=10, col="red")
# points(top10.loc, LOD[top10.loc], pch=1, col="blue")
# par(mfrow=c(2,5))
# for (i in top10.loc){
# 	id = i
# 	TRANSCRIPT_VECTOR = RNA[id,]
# 	y_t = TRANSCRIPT_VECTOR
# 	x_p = 1:length(y_t)
# 	mod = lm(y_t ~ x_p)
# 	pval = summary(mod)$coefficients[2,4]
# 	eff = summary(mod)$coefficients[2,1]
# 	r2 = summary(mod)$r.squared
# 	plot(TRANSCRIPT_VECTOR, main=id)
# 	abline(mod)
# 	legend("bottomright", legend=c( paste0("PVAL = ", round(pval, 2)),
# 									paste0("EFF = ", round(QTT$EFX[i])),
# 									paste0("R2 = ", round(r2, 2))
# 			))
# }
