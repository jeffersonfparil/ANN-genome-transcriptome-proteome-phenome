#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]

//NOT WORKING....20180825

int __NUNIQUE__(NumericVector x) {
	int n = x.size();
	int R = 0;
	int A = 0;
	int T = 0;
	int C = 0;
	int G = 0;
	for (int i=0; i<n; i++){
		if (x(i)==0) {
			R = 1;
		}
		else if (x(i)==1) {
			A = 1;
		}
		else if (x(i)==2) {
			T = 1;
		}
		else if (x(i)==3) {
			C = 1;
		}
		else if (x(i)==4) {
			G = 1;
		}
	}
	int out = R + A + T + C + G;
	return(out);
}

NumericVector __FILTER_BIALLELIC__(NumericMatrix GENO) {
	int nIND = GENO.ncol();
	int nLOC = GENO.nrow();
	int nUNQ;
	NumericVector OUT(nLOC);
	for (int i=0; i<nLOC; i++){
		NumericVector TESTVECT = GENO(i, Range(0,nIND));
		nUNQ = __NUNIQUE__(TESTVECT);
		if(nUNQ == 2){
			OUT(i) = 1;
		}
		else {
			OUT(i) = 0;
		}
	}
	return(OUT);
}
