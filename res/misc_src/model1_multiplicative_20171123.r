#!/usr/bin/env Rscript
#model1: Y1 = XKABj + e; model2: Y2 = XC1 + XKABj + e;
args = commandArgs(trailingOnly=TRUE)
model = args[1]	#1=fully multiplicative; 2=additive-multiplicative
n = args[2]		#number of individuals
l = args[3] 	#number of loci
m = args[4] 	#number of transcripts = number of loci
q = args[5]		#number of causal loci or QTL (can sampled from some distribution)
t = args[6] 	#number of causal transcripts (can sampled from some distribution)
k = args[7]		#Verror / Vmodel
fileNamesSuffix = args[8]	#output filenames suffix

##########################
#for testing:
# model=1
# n = 500
# l = 1000
# m = l
# q = 10
# t = q
# k = 1
# fileNamesSuffix = "Full_Model"
##########################



additiveGeneEffectsOnly = FALSE
additiveTranscriptEffectsOnly = FALSE

#######
##	 ##
## X ##
##	 ##
#######
#genotype matrix (n x l)
#X = matrix(rbinom(n=(n*l), size=2, prob=0.5), nrow=n, byrow=T) #without LD
freqREF = rbeta(n=l, shape1=2, shape2=2)
freqALT = 1 - freqREF
link = rbinom(n=l, size=1, prob=freqREF)
nCHROM = 7
X1=matrix(NA, nrow=n, ncol=l)
X2=matrix(NA, nrow=n, ncol=l)
progressBar = txtProgressBar(min = 0, max = n, initial = 0, style=3, width=50)
for (ind in 1:n) {
	x1=c()
	x2=c()
	for (i in 1:l) {
		if (i%%(round(n/nCHROM))==1) {
			a1 = sample(c(0, 1), size=1, prob=c(freqREF[i], freqALT[i]))	#No LD between scaffolds
			a2 = sample(c(0, 1), size=1, prob=c(freqREF[i], freqALT[i]))
		} else {
			if (link[i-1]==link[i]) {
				if ((link[i-1]==0 & a1==0) | (link[i-1]==1 & a1==0)) {
					Pab = min(freqREF[i-1], freqREF[i]) / max(freqREF[i-1], freqREF[i])
					a1 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
					a2 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
				} else {
					Pab = min(freqALT[i-1], freqALT[i]) / max(freqALT[i-1], freqALT[i])
					a1 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
					a2 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab)) }
			} else {
				if ((link[i-1]==0 & a1==0) | (link[i-1]==1 & a1==0)) {
					Pab = min(freqREF[i-1], freqALT[i]) / max(freqREF[i-1], freqALT[i])
					a1 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
					a2 = sample(c(1, 0), size=1, prob=c(Pab, 1-Pab))
				} else {
					Pab = min(freqALT[i-1], freqREF[i]) / max(freqALT[i-1], freqREF[i])
					a1 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab))
					a2 = sample(c(0, 1), size=1, prob=c(Pab, 1-Pab)) } 
			}
		}
		x1 = c(x1, a1)
		x2 = c(x2, a2)
	}
	setTxtProgressBar(progressBar, ind)
	X1[ind,] = x1
	X2[ind,] = x2
}
X = X1 + X2

# #plotting ~LD (correlation)
# distance=c()
# LD = c()
# progressBar = txtProgressBar(min = 0, max = ( ((l^2)-l) / 2 ), initial = 0, style=3, width=50); counter = 0
# for (i in 1:(l-1)) {
# 	for (j in (i+1):l) {
# 		distance = c(distance, abs(i-j))
# 		LD = c(LD, cor(X[,i], X[,j]))
# 		setTxtProgressBar(progressBar, counter); counter = counter + 1
# 	}
# }
# jpeg("Pairwise loci correlation - LD measure.jpg", quality=100, width=700, height=500)
# plot(distance, abs(LD))
# dev.off()

# #building G-matrix
# M_matrix = X
# ref.Allele.freq = colSums(X)/(2*nrow(X))
# F_matrix = matrix(rep(ref.Allele.freq, each=n), ncol=l, byrow=F)
# P_matrix = 2*(F_matrix - 0.50)
# Z_matrix = M_matrix - P_matrix
# G_matrix = (Z_matrix %*% t(Z_matrix)) / 2*sum(ref.Allele.freq*(1-ref.Allele.freq))


#######
##	 ##
## K ##
##	 ##
#######
#loci and loci interaction effects (l x l)
	#QTL additive effects sampling parameters:
		# #beta distribution:
		# 	qa1=2
		# 	qa2=2
		#normal distribution:
			qa1=0 #mean additive qlt effect
			qa2=4 #additive qtl variance (maybe add another parameter as the ratio between additive and non-additive variance?!)
	#QTL interaction effects sampling parameters:
		# #beta distribution:
		# 	qi1=1
		# 	qi2=2
		#normal distribution:
			qi1=0 #mean non-additive qtl effect
			qi2=1 #non-additive qtl vatiance
	qtl.loc = sample(1:l, size=q, replace=F)
	#qtl.additive = matrix(rbeta(n=q, shape1=qa1, shape2=qa2), nrow=q, ncol=1)
	#qtl.additive = qtl.additive/sum(qtl.additive)
	qtl.additive = matrix(rnorm(n=q, mean=qa1, sd=sqrt(qa2)), nrow=q, ncol=1)
	k_vect = matrix(0, nrow=l, ncol=1)
	k_vect[qtl.loc,1] = qtl.additive
K = diag(k_vect[,1]) #----> no interactions - completely additive gene effects! - off-diagonals are all zeros!
	if (additiveGeneEffectsOnly == FALSE) {
			#qtl.interactions = matrix(rbeta(n=q*q, shape1=qi1, shape2=qi2), nrow=q, ncol=q, byrow=TRUE)
			#qtl.interactions = qtl.interactions/sum(qtl.interactions)
			qtl.interactions = matrix(rnorm(n=q*q, mean=qi1, sd=sqrt(qi2)), nrow=q, ncol=q, byrow=TRUE)
			qtl.AxI = qtl.interactions
			diag(qtl.AxI) = qtl.additive
			k_matr = matrix(0, nrow=l, ncol=l)
			k_matr[qtl.loc, qtl.loc] = qtl.AxI
		K = k_matr # ---> additive (diagonals) and interaction (off-diagonals) effects
	}
# #assesing qtl effects distributions:
# plot(density(K[K!=0])) #whole matrix
# plot(density(diag(K)[diag(K)!=0])) #addtive effects only
# d = row(K) - col(K)
# plot(density(K[d!=0][K[d!=0]!=0])) #all non-additive effects

#######
##	 ##
## A ##
##	 ##
#######
#genotype to transcript connection (l x m)
	tra.loc = sample(1:m, size=t, replace=F)
	a = matrix(0, nrow=q, ncol=t, byrow=T)
		for (i in seq(1, t)){a[,i]=sample(c(1, rep(0, each=q-1)), size=q, replace=F)}
	A = matrix(0, nrow=l, ncol=m)
A[qtl.loc, tra.loc] = a

#######
##	 ##
## B ##
##	 ##
#######
#transcript (and transcript genotype?!) interactions effects (m x m)
	#transcript additive effects sampling parameters:
		# #beta distribution:
		# 	ta1=2
		# 	ta2=2
		#normal distribution:
			ta1=0 #mean additive transcript effect
			ta2=4 #additive transcript variance
	#transcript interaction effects sampling parameters:
		# #beta distribution:
		# 	ti1=0.25
		# 	ti2=1
		#normal distribution:
			ti1=0 #mean additive transcript effect
			ti2=1 #additive transcript variance
	# tra.additive = matrix(rbeta(n=t, shape1=ta1, shape2=ta2), nrow=t, ncol=1)
	# tra.additive = tra.additive/sum(tra.additive)
	tra.additive = matrix(rnorm(n=t, mean=ta1, sd=sqrt(ta2)), nrow=t, ncol=1)
	b_vect = matrix(0, nrow=m, ncol=1)
	b_vect[tra.loc,1] = tra.additive
B = diag(b_vect[,1]) #----> no interactions - completely additive transcript effects! - off-diagonals are all zeros!
	if (additiveTranscriptEffectsOnly == FALSE) {
			# tra.interactions = matrix(rbeta(n=q*q, shape1=ti1, shape2=ti2), nrow=q, ncol=q, byrow=TRUE)
			# tra.interactions = tra.interactions/sum(tra.interactions)
			tra.interactions = matrix(rnorm(n=q*q, mean=ti1, sd=sqrt(ti2)), nrow=q, ncol=q, byrow=TRUE)
			tra.BxI = tra.interactions
			diag(tra.BxI) = tra.additive
			b_matr = matrix(0, nrow=m, ncol=m)
			b_matr[tra.loc, tra.loc] = tra.BxI
		B = b_matr # ---> additive (diagonals) and interaction (off-diagonals) effects
			#Assesing tra effects:
				#tra additive effects distribution
					#plot(density(tra.additive))
				#tra interaction effects distribution
					#plot(density(tra.interactions))
	}
# #assesing transcript effects distributions:
# plot(density(B[B!=0])) #whole matrix
# plot(density(diag(B)[diag(B)!=0])) #addtive effects only
# d = row(B) - col(B)
# plot(density(B[d!=0][B[d!=0]!=0])) #all non-additive effects

#######
##	 ##
## J ##
##	 ##
#######
#vector of ones for summing up all effects per individual
j = matrix(1, nrow=m, ncol=1)

#######
##	 ##
## e ##
##	 ##
#######
#residual effects ~ N(0, sd=??? --> such that h2=50%)
if (model == 0) {
	Vmodel = var(X %*% K %*% j)
	Verror = k*Vmodel
	e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
} else if (model == 1) {
	Vmodel = var(X %*% K %*% A %*% B %*% j)
	Verror = k*Vmodel
	e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
} else if (model == 2) {
	Vmodel = var((X %*% K %*% j) + (X %*% K %*% A %*% B %*% j))
	Verror = k*Vmodel
	e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)
}

#######
##	 ##
## Y ##
##	 ##
#######
#phenotypes
if (model == 0) {
	#snp model
	Y = (X %*% K %*% j) + e
} else if (model == 1) {
	#fully multiplicative
	Y = (X %*% K %*% A %*% B %*% j) + e
} else if (model ==2){
	#additive-multiplicative?
	Y = (X %*% K %*% j) + (X %*% K %*% A %*% B %*% j) + e
}
# jpeg("Density Plot of Simulated Phenotypes.jpeg")
# plot(density(Y))
# dev.off()

#######
##	 ##
## T ##
##	 ##
#######
#transcript abundance data - all transcripts
t_causal = X %*% K %*% A #scaled abundance matrix of causal transcripts
T = matrix(rbeta(n=n*m, shape1=0.5, shape2=2), nrow=n, ncol=m, byrow=T)
T[,tra.loc] = t_causal[,tra.loc]



################################################################################################################################################################
################################################################################################################################################################
################################################################################################################################################################



###################################################################################################
###																								###
###		Inferring parameters (or effects) given phenotypes from the different models built		###
###																								###
###################################################################################################

#METHODS:
	#(1) Least squares approach - minimize error variance
	#(2) Maximum likelihood approach - maximize likelihood of parameters given data
	#(3) Bayesian approach - convergence of posterior = likelihood x prior / integral(likelihood x prior)

#####################
#
#(1) LEAST SQUARES: fit each gene, transcript and/or gene-transcript combinations into some specific model or a combination of models
#						then minimize the residual variance
#####################
	simple_roc.bobHorton <- function(labels, scores){labels <- labels[order(scores, decreasing=TRUE)]; data.frame(TPR=cumsum(labels)/sum(labels), FPR=cumsum(!labels)/sum(!labels), labels)}
	simple_auc.erik <- function(TPR, FPR){mean(sample(TPR,l,replace=TRUE) > sample(FPR,l,replace=TRUE))}
	############## 1.1/3
	# SNPs ALONE #
	##############
	p_values = c()
	progressBar = txtProgressBar(min=0, max=l, initial=0, width=50, style=3)
	for (i in 1:l) {
		p_values = c(p_values, summary(lm(Y[,1] ~ X[,i]))$coefficients[2,4])
		setTxtProgressBar(progressBar, i)
	}
	close(progressBar)
	LOD = -log(p_values, base=10)
	LABELS = matrix(0, nrow=l, ncol=1)
	LABELS[qtl.loc,1] = 1
	ROC = simple_roc.bobHorton(LABELS, LOD)
	AUC = simple_auc.erik(ROC$TPR, ROC$FPR)
	#manhattan plot
	jpeg(paste("ManhattanPlot-GWAS-" , fileNamesSuffix, ".jpeg", sep=""), quality=100, width=700, height=500)
	plot(0,pch='',xlim=c(0,l),xlab="SNP ID",xaxt='n',ylab=expression(-log[10](italic(p))),ylim=c(0,max(LOD,na.rm=T)),bty="n",las=2,main="Manhattan Plot - GWAS (GLM)")
	points(LOD)
	dev.off()
	#roc plot and auc
	jpeg(paste("ROCPlot-GWAS-", fileNamesSuffix, ".jpeg", sep=""), quality=100, width=500, height=500)
	plot(x=ROC$FPR, y=ROC$TPR, xlab="False Positive Rate", ylab="True Positive Rate", main="ROC Plot - GWAS (GLM)")
	lines(x=ROC$FPR, y=ROC$TPR)
	abline(0,1, lty=2)
	legend("bottomright",legend=paste("AUC = ", AUC, sep=""), bty ="n", pch=NA)
	dev.off()

	#correlation between true QTL and LOD
	jpeg(paste("CORPlot-GWAS-", fileNamesSuffix, ".jpeg", sep=""), quality=100, width=500, height=500)
	plot(LABELS, LOD)
	abline(lm(LOD~LABELS), col="red")
	dev.off()
	

	#plot(LABELS, LOD)

	# #EXTRA! EXTRA! Using Fst to infer QTL:
	# n.sub.popns = 100
	# ordered.X = X[order(Y[,1]),]
	# Fst = c()
	# progressBar = txtProgressBar(min=0, max=l, initial=0, width=50, style=3)
	# for (i in 1:l) {
	# 	freqs = c()
	# 	for (j in 1:n.sub.popns) {
	# 		freqs = c(freqs, mean(X[(((j-1)*(n/n.sub.popns))+1):(j*(n/n.sub.popns)),i]) / 2)
	# 	}
	# 	Ht = 1 - ( (mean(freqs)^2) + (mean(1-freqs)^2) )
	# 	Hs = mean( 1 - ((freqs)^2 + ((1-freqs)^2)) )
	# 	Fst = c(Fst, (Ht-Hs)/Ht)
	# 	setTxtProgressBar(progressBar, i)
	# }
	# close(progressBar)
	# plot(LOD, Fst)
	# plot(LABELS, Fst)


	#################### 1.2/3
	# TRANSCRIPT ALONE #
	####################
	p_values = c()
	progressBar = txtProgressBar(min=0, max=m, initial=0, width=50, style=3)
	for (i in seq(1, m)) {
		p_values = c(p_values, summary(lm(Y[,1] ~ T[,i]))$coefficients[2,4])
		setTxtProgressBar(progressBar, i)
	}
	close(progressBar)
	LOD = -log(p_values, base=10)
	LABELS = matrix(0, nrow=l, ncol=1)
	LABELS[tra.loc,1] = 1
	ROC = simple_roc.bobHorton(LABELS, LOD)
	AUC = simple_auc.erik(ROC$TPR, ROC$FPR)
	#manhattan plot
	jpeg(paste("ManhattanPlot-TWAS-" , fileNamesSuffix, ".jpeg", sep=""), quality=100, width=700, height=500)
	plot(0,pch='',xlim=c(0,m),xlab="Transcript ID",xaxt='n',ylab=expression(-log[10](italic(p))),ylim=c(0,max(LOD,na.rm=T)),bty="n",las=2,main="Manhattan Plot - TWAS (GLM)")
	points(LOD)
	dev.off()
	#roc plot and auc
	jpeg(paste("ROCPlot-TWAS-", fileNamesSuffix, ".jpeg", sep=""), quality=100, width=500, height=500)
	plot(x=ROC$FPR, y=ROC$TPR, xlab="False Positive Rate", ylab="True Positive Rate", main="ROC Plot - TWAS (GLM)")
	lines(x=ROC$FPR, y=ROC$TPR)
	abline(0,1, lty=2)
	legend("bottomright",legend=paste("AUC = ", AUC, sep=""), bty ="n", pch=NA)
	dev.off()

	################################### 1.3/3
	# SNP & TRANSCRIPT ADDITIVE LASSO #
	###################################
	library(glmnet)
	# glmnet(x, y, family=c("gaussian","binomial","poisson","multinomial","cox","mgaussian"),
	# 	weights, offset=NULL, alpha = 1, nlambda = 100,
	# 	lambda.min.ratio = ifelse(nobs<nvars,0.01,0.0001), lambda=NULL,
	# 	standardize = TRUE, intercept=TRUE, thresh = 1e-07, dfmax = nvars + 1,
	# 	pmax = min(dfmax * 2+20, nvars), exclude, penalty.factor = rep(1, nvars),
	# 	lower.limits=-Inf, upper.limits=Inf, maxit=100000,
	# 	type.gaussian=ifelse(nvars<500,"covariance","naive"),
	# 	type.logistic=c("Newton","modified.Newton"),
	# 	standardize.response=FALSE, type.multinomial=c("ungrouped","grouped"))
	LASSO = cv.glmnet(x=cbind(X,T), y=Y[,1], family='gaussian', alpha=1)
	LASSO_OUT = matrix(coef(LASSO, s='lambda.min')[2:length(coef(LASSO, s='lambda.min')),], nrow=l+m, ncol=1)
	LASSO_snp = LASSO_OUT[1:l, 1]
	LASSO_tra = LASSO_OUT[(l+1):(l+m), 1]

	LABELS_snp = matrix(0, nrow=l, ncol=1)
	LABELS_snp[qtl.loc,1] = 1
	ROC_snp = simple_roc.bobHorton(LABELS_snp, LASSO_snp)
	AUC_snp = simple_auc.erik(ROC_snp$TPR, ROC_snp$FPR)

	LABELS_tra = matrix(0, nrow=m, ncol=1)
	LABELS_tra[tra.loc,1] = 1
	ROC_tra = simple_roc.bobHorton(LABELS_tra, LASSO_tra)
	AUC_tra = simple_auc.erik(ROC_tra$TPR, ROC_tra$FPR)

	#SNPs and Transcrips Effects plots
	jpeg(paste("SNP_effects-LASSO-" , fileNamesSuffix, ".jpeg", sep=""), quality=100, width=700, height=500)
	plot(0,pch='',xlim=c(0,l),xlab="SNP ID",xaxt='n',ylab="LASSO beta",ylim=c(min(LASSO_snp,na.rm=T),max(LASSO_snp,na.rm=T)),bty="n",las=2,main="SNP Effects - LASSO (SNPs + Transcripts)")
	points(LASSO_snp)
	dev.off()

	jpeg(paste("Transcript_effects-LASSO-" , fileNamesSuffix, ".jpeg", sep=""), quality=100, width=700, height=500)
	plot(0,pch='',xlim=c(0,m),xlab="Transcript ID",xaxt='n',ylab="LASSO beta",ylim=c(min(LASSO_tra,na.rm=T),max(LASSO_tra,na.rm=T)),bty="n",las=2,main="Transcript Effects - LASSO (SNPs + Transcripts)")
	points(LASSO_tra)
	dev.off()

	#roc plot and auc
	jpeg(paste("ROCPlot-LASSO-SNPs-", fileNamesSuffix, ".jpeg", sep=""), quality=100, width=500, height=500)
	plot(x=ROC_snp$FPR, y=ROC_snp$TPR, xlab="False Positive Rate", ylab="True Positive Rate", main="ROC Plot - LASSO (X+T): SNPs")
	lines(x=ROC_snp$FPR, y=ROC_snp$TPR)
	abline(0,1, lty=2)
	legend("bottomright",legend=paste("AUC = ", AUC_snp, sep=""), bty ="n", pch=NA)
	dev.off()

	jpeg(paste("ROCPlot-LASSO-Transcripts-", fileNamesSuffix, ".jpeg", sep=""), quality=100, width=500, height=500)
	plot(x=ROC_tra$FPR, y=ROC_tra$TPR, xlab="False Positive Rate", ylab="True Positive Rate", main="ROC Plot - LASSO (X+T): Transcripts")
	lines(x=ROC_tra$FPR, y=ROC_tra$TPR)
	abline(0,1, lty=2)
	legend("bottomright",legend=paste("AUC = ", AUC_tra, sep=""), bty ="n", pch=NA)
	dev.off()



#############################
#
#(2) MAXIMUM LIKELIHOOD: fit each gene, transcript and/or gene-transcript combinations into some specific model or a combination of models
#							then find the parameter/s that maximize/s the log likelihood of the data
#############################

#test code (from alex 20170907):
P_obs=rnorm(1000, mean=10, sd=2)
loglik <- function(par, y) {sum(dnorm(y, mean=par[1], sd=sqrt(par[2]), log = TRUE))}
Par=optim(par=c(mean = 0, var = 1), fn = loglik, y=P_obs, control=list(fnscale = -1, reltol = 1e-16))$par

P_obs=rbinom(100,1,prob=.20)
loglik <- function(prob, y) {sum(dbinom(y, 1,prob=prob, log = TRUE))}
Par=optim(par = c(prob=.5), fn = loglik,y=P_obs, control = list(fnscale = -1, reltol = 1e-5),method="Brent",lower=0,upper=1)$par


#############################
#
#(3) BAYESIAN ESTIMATION: fit each gene, transcript and/or gene-transcript combinations into some specific model or a combination of models
#							then find the parameters whose posterior probability is maximized (converges) after many iterations through the parameter space
#############################





########################################################
########################################################
#########################################################################################
##########################################################################################################################
##########################################################################################################################
###########################################################################################################################################################
##########################################################################################################################
##########################################################################################################################
#########################################################################################
########################################################
########################################################


# FOR TESTING YET !!!!!!!!!!!!!!!!


#################################
###							  ###
### P O O L E D    G T W A S  ###
###							  ###
#################################
# We'll be testing 2 methods:	(1) alex's individual phenotyping followed by pooling; and 
#								(2) my proposed pool then phenotype - requires the knowledge of at least one pool of highly resistant and one pool of highly susceptibles!

# METHOD 1: phenotype then pool

###################
# (1) PHENOTYPING #
###################

#arrange by phenotypic values
ID = 1:n
ALL = data.frame(ID, Y, X, T)
ALL = ALL[order(ALL$Y), ]
PHEN = matrix(ALL[, 2])
GENO = ALL[, (2+1):(2+m)]
TRAN = ALL[, (2+m+1):(2+m+m)]

#pooling
npool = 5
for (i in 1:npool) { 
	index1 = ((n/npool)*(i-1)) + 1
	index2 = (n/npool)*i
	assign(sprintf("PHEN_pool.%02d", i), PHEN[index1:index2,])
	assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
	assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])
}

#generate phenotype data input python file for GWAlpha
perc = seq(0, 1, by=1/npool); perc = perc[2:(length(perc)-1)]
out = rbind(
	c('Pheno_name="TEST_PHEN";'),
	c(paste('sig=', sqrt(var(PHEN[,1])), ';', sep="")), 
	c(paste('MIN=', min(PHEN[,1]), ';', sep="")), 
	c(paste('MAX=', max(PHEN[,1]), ';', sep="")), 
	c(paste('perc=[', apply(matrix(perc), 2, paste, collapse=","), '];', sep="")),
	c(paste('q=[', apply(matrix(quantile(PHEN[,1], perc)), 2, paste, collapse=","), '];', sep=""))
)
write.table(out, file="test_pheno.py", col.names=F, row.names=F, quote=F, sep="\t")


##################
# (2) GENOTYPING #
##################
# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
SYNC = c()
for (p in ls()[grepl("GENO_pool.", ls())]) {
	p = eval(parse(text=p))
	sync.A = colSums(p)
	sync.T = (2*nrow(p)) - sync.A
	sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
	sync = apply(sync, 1, paste, collapse=":")
	SYNC = cbind(SYNC, sync)
}
SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)
write.table(SYNC, file="test.sync", col.names=F, row.names=F, quote=F, sep="\t")


###############
# (3) GWAlpha #
###############
system("~/GWAlpha/GWAlpha.py test.sync ML")
GWA = read.csv("GWAlpha_test_out.csv")
plot(LABELS[GWA$Position], GWA$Alpha)
system("~/GTWAS_POOL_RADseq_SIM/GWAlphaPlot-jefdited.r GWAlpha_test_out.csv pval qqplot")
system("eog GWAlpha_test.png")
QTL.data = data.frame(rep(c("1L"), times=length(qtl.loc)), qtl.loc, qtl.additive); colnames(QTL.data) = c("scaffold", "position", "effect"); write.table(QTL.data, file="QTL.data", row.names=F, quote=F, sep="\t")
system("~/GTWAS_POOL_RADseq_SIM/plotROC.py $(pwd) GWAlpha_test_out.csv QTL.data 1000")
system("eog ROC.png")


################################################################################################################################
################################################################################################################################
##########					######################					###############				############	  ##############
################################################################################################################################
################################################################################################################################


# METHOD 2: pool then phenotype

###################
# (1) PHENOTYPING #
###################
ID = 1:n
ALL = data.frame(ID, Y, X, T)

#pooling ---> let's generate 10 pools
npool = 10 #even number of pools seem to be better than odd numbers probably due to my unequal partiotioning of pool contamination

#STEPS:
#(1) identify 2 pools of 50 individuals each that are highly susceptible and highly resistant then randomize everything in between
	ind.pool = 50
	SUS = ALL[order(ALL$Y),][1:ind.pool, ]
	RES = ALL[order(ALL$Y),][(nrow(ALL)-(ind.pool-1)):nrow(ALL), ]
#(2) randomize in between
	BET = ALL[order(ALL$Y),][(ind.pool+1):(nrow(ALL)-(ind.pool)), ]; BET = BET[sample(1:nrow(BET), size=nrow(BET), replace=F), ]
#(3) generate the pools prior to systematic contamination with SUS or RES pools
	ALL2 = rbind(SUS, BET, RES)
	PHEN = matrix(ALL2[, 2])
	GENO = ALL2[, (2+1):(2+m)]
	TRAN = ALL2[, (2+m+1):(2+m+m)]
	for (i in 1:npool) { 
		index1 = ((n/npool)*(i-1)) + 1
		index2 = (n/npool)*i
		assign(sprintf("PHEN_pool.%02d", i), PHEN[index1:index2,])
		assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
		assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])
	}
#(4) contamination with SUS or RES (KEY STEP IN BULK PHENOTYPING!!!)
	#SUS contamination
		#if npool is even:
		if (npool%%2==0) {
			for (i in 1:((npool-2)/2)) {
				j = i + 1
				k = 1 - (i/((npool-1)/2))
				print(i); print(j); print(k)
				assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_pool.01[sample(1:length(PHEN_pool.01), size=round(k*length(PHEN_pool.01)), replace=F)]))
				assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_pool.01[sample(1:nrow(GENO_pool.01), size=round(k*nrow(GENO_pool.01)), replace=F), ]))
				assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_pool.01[sample(1:nrow(TRAN_pool.01), size=round(k*nrow(TRAN_pool.01)), replace=F), ]))
			}
		} else {
			#if npool is odd:
			for (i in 1:((npool-1)/2)) {
				#include the center pool (npool-1 instead of npool-2)
				j = i + 1
				k = 1 - (i/((npool-1)/2))
				print(i); print(j); print(k)
				assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_pool.01[sample(1:length(PHEN_pool.01), size=round(k*length(PHEN_pool.01)), replace=F)]))
				assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_pool.01[sample(1:nrow(GENO_pool.01), size=round(k*nrow(GENO_pool.01)), replace=F), ]))
				assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_pool.01[sample(1:nrow(TRAN_pool.01), size=round(k*nrow(TRAN_pool.01)), replace=F), ]))
			}
		}
	#RES contamination
		#if npool is even:
		if (npool%%2==0) {
			for (i in 1:((npool-2)/2)) {
				j = (npool/2) + i
				k = i/((npool-1)/2)
				print(i); print(j); print(k)
				assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_pool.10[sample(1:length(PHEN_pool.10), size=round(k*length(PHEN_pool.10)), replace=F)]))
				assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_pool.10[sample(1:nrow(GENO_pool.10), size=round(k*nrow(GENO_pool.10)), replace=F), ]))
				assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_pool.10[sample(1:nrow(TRAN_pool.10), size=round(k*nrow(TRAN_pool.10)), replace=F), ]))
			}
		} else {
			#if npool is odd:
			for (i in 1:((npool-3)/2)) {
				#exclude the center pool (npool-3 instead of npool-2)
				j = ceiling(npool/2) + i
				k = i/((npool-1)/2)
				print(i); print(j); print(k)
				assign(sprintf("PHEN_pool.%02d", j), c(eval(parse(text=sprintf("PHEN_pool.%02d", j))), PHEN_pool.10[sample(1:length(PHEN_pool.10), size=round(k*length(PHEN_pool.10)), replace=F)]))
				assign(sprintf("GENO_pool.%02d", j), rbind(eval(parse(text=sprintf("GENO_pool.%02d", j))), GENO_pool.10[sample(1:nrow(GENO_pool.10), size=round(k*nrow(GENO_pool.10)), replace=F), ]))
				assign(sprintf("TRAN_pool.%02d", j), rbind(eval(parse(text=sprintf("TRAN_pool.%02d", j))), TRAN_pool.10[sample(1:nrow(TRAN_pool.10), size=round(k*nrow(TRAN_pool.10)), replace=F), ]))
			}
		}
#(4) build distrbution of phenotypica values across pools
	poolMEANS = c()
	poolCUMM = c(0)
	for (i in ls()[grepl("PHEN_pool.", ls())]) {
		dat = eval(parse(text=i))
		poolMEANS = c(poolMEANS, mean(dat))
		poolCUMM = c(poolCUMM, poolCUMM[length(poolCUMM)]+length(dat))
		#print(i); print(length(dat)); print(mean(dat))
	}
	#plot(density(poolMEANS))
	perc = poolCUMM/max(poolCUMM); perc = perc[2:(length(perc)-1)]

#generate phenotype data input python file for GWAlpha
out = rbind(
	c('Pheno_name="TEST_PHEN";'),
	c(paste('sig=', sqrt(var(poolMEANS)), ';', sep="")), 
	c(paste('MIN=', min(poolMEANS), ';', sep="")), 
	c(paste('MAX=', max(poolMEANS), ';', sep="")), 
	c(paste('perc=[', apply(matrix(perc), 2, paste, collapse=","), '];')),
	c(paste('q=[', apply(matrix(quantile(poolMEANS, perc)), 2, paste, collapse=","), '];', sep=""))
)
write.table(out, file="test_pheno.py", col.names=F, row.names=F, quote=F, sep="\t")


##################
# (2) GENOTYPING #
##################
# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
SYNC = c()
for (p in ls()[grepl("GENO_pool.", ls())]) {
	p = eval(parse(text=p))
	sync.A = colSums(p)
	sync.T = (2*nrow(p)) - sync.A
	sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
	sync = apply(sync, 1, paste, collapse=":")
	SYNC = cbind(SYNC, sync)
}
SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)
write.table(SYNC, file="test.sync", col.names=F, row.names=F, quote=F, sep="\t")


###############
# (3) GWAlpha #
###############
system("~/GWAlpha/GWAlpha.py test.sync ML")
GWA = read.csv("GWAlpha_test_out.csv")
system("~/GTWAS_POOL_RADseq_SIM/GWAlphaPlot-jefdited.r GWAlpha_test_out.csv pval qqplot")
system("eog GWAlpha_test.png")
QTL.data = data.frame(rep(c("1L"), times=length(qtl.loc)), qtl.loc, qtl.additive); colnames(QTL.data) = c("scaffold", "position", "effect"); write.table(QTL.data, file="QTL.data", row.names=F, quote=F, sep="\t")
system("~/GTWAS_POOL_RADseq_SIM/plotROC.py $(pwd) GWAlpha_test_out.csv QTL.data 1000")
system("eog ROC.png")

####################################################################################
#manual GWAlpha in R (test is I understand what's being done 20180109) (MLE only):
#test:
#FREQ_MATRIX = matrix(as.numeric(unlist(strsplit(SYNC[1,4:ncol(SYNC)], split=":"))), byrow=T, nrow=npool, ncol=6); FREQ_MATRIX = FREQ_MATRIX / rowSums(FREQ_MATRIX)
####################################################################################
compute_Alpha <- function(FREQ_MATRIX, SD, MIN, MAX, PERC, Q) {
	#determine the size of each pool based on their percentile values
	BINS = c(PERC, 1) - c(0, PERC)
	#find the major allele - that is the allele with the highes frequency across pools
	allele_means = colMeans(FREQ_MATRIX)
	allele_max_index = which.max(colSums(FREQ_MATRIX)); if(allele_max_index==1){allele="A"} else if(allele_max_index==2){allele="T"} else if(allele_max_index==3){allele="C"} else if(allele_max_index==4){allele="G"} else if(allele_max_index==5){allele="N"} else {allele="DEL"} #determine major allele identity
	FREQ_A = FREQ_MATRIX[,allele_max_index]	#potential problem here when more than 1 column or allele are equal to the maximum value!!!
	#transform the allele frequency of the major allele such that the sum of allele frequencies across pools is equal to 1
	BIN_A = FREQ_A*BINS/sum(FREQ_A*BINS); BIN_A = BIN_A/sum(BIN_A) #is dividing them again with the sum necessary???
	#transform the aggregate of the alternative alleles in the same way as in the major allele
	BIN_B = (1 - FREQ_A)*BINS/(1-sum(FREQ_A*BINS));	BIN_B = BIN_B/sum(BIN_B)
	#convert back the transformed allele frequencies into percentiles
	PERC_A = c(); PERC_B = c()
	for(i in 1:length(BIN_A)){
		PERC_A = c(PERC_A, sum(BIN_A[1:i]))
		PERC_B = c(PERC_B, sum(BIN_B[1:i]))
	}
	#define the likelihood function as the cdf(yi, b1,b2) - cdf(yi-1, b1,b2) [--> for the major allele] PLUS cdf(yi, b3,b4) - cdf(yi-1, b3,b4) [--> for the altenative alleles aggregate]
	optimize_me <- function(PERC_A, PERC_B, par) {
		PERC_A0 = c(0, PERC_A[1:(length(PERC_A)-1)])
		PERC_B0 = c(0, PERC_B[1:(length(PERC_B)-1)])
		return(-sum(log(pbeta(PERC_A, shape1=par[1], shape2=par[2])-pbeta(PERC_A0, shape1=par[1], shape2=par[2]))) - sum(log(pbeta(PERC_B, shape1=par[3], shape2=par[4])-pbeta(PERC_B0, shape1=par[3], shape2=par[4]))) )
	}
	#maximize the likelihood or in the case minimize the -log likelihood
	OPTIM_PAR = optim(par=c(1,1,1,1), fn=optimize_me, PERC_A=PERC_A, PERC_B=PERC_B)$par
	#compute for the mean of the beta desitribution based on the computed parameters for both major and alternative alleles
	MU_A = MIN + ((MAX-MIN)*OPTIM_PAR[1]/(OPTIM_PAR[1]+OPTIM_PAR[2]))
	MU_B = MIN + ((MAX-MIN)*OPTIM_PAR[3]/(OPTIM_PAR[3]+OPTIM_PAR[4]))
	#compute the test statistic alpha
	ALPHA = (MU_A - MU_B)/SD #no penalization W = 2*sqrt(pA*(1-pA))
	OUT = data.frame(ALPHA, allele)
	return(OUT)
}

SD = sqrt(var(poolMEANS))
MIN = min(poolMEANS)
MAX = max(poolMEANS)
PERC = perc
Q = quantile(poolMEANS, perc)

# SD = sqrt(var(PHEN[,1]))
# MIN = min(PHEN[,1])
# MAX = max(PHEN[,1])
# PERC = perc
# Q = quantile(PHEN[,1], perc)

allele = c()
alpha = c()
progressBar = txtProgressBar(min = 0, max = nrow(SYNC), initial = 0, style=3, width=50)
for (loci in 1:nrow(SYNC)) {
	FREQ_MATRIX = matrix(as.numeric(unlist(strsplit(SYNC[loci,4:ncol(SYNC)], split=":"))), byrow=T, nrow=npool, ncol=6); FREQ_MATRIX = FREQ_MATRIX / rowSums(FREQ_MATRIX)
	out = compute_Alpha(FREQ_MATRIX, SD, MIN, MAX, PERC, Q)
	allele = c(allele, as.character(out$allele))
	alpha = c(alpha, out$ALPHA)
	setTxtProgressBar(progressBar, loci)
}
close(progressBar)

ALPHA_OUT = data.frame(SYNC[,1], SYNC[,2], allele, alpha); colnames(ALPHA_OUT) = c("# Chromosome", "Position", "Mutation", "Alpha")
write.csv(ALPHA_OUT, file="gwalpha_test_out.csv", row.names=F, quote=F)

system("~/GTWAS_POOL_RADseq_SIM/GWAlphaPlot-jefdited.r gwalpha_test_out.csv pval qqplot")
system("eog gwalpha_test.png")
QTL.data = data.frame(rep(c("1L"), times=length(qtl.loc)), qtl.loc, qtl.additive); colnames(QTL.data) = c("scaffold", "position", "effect"); write.table(QTL.data, file="QTL.data", row.names=F, quote=F, sep="\t")
system("~/GTWAS_POOL_RADseq_SIM/plotROC.py $(pwd) gwalpha_test_out.csv QTL.data 1000")
system("eog ROC.png")

##################   I T    W O R K S  !  !  !  !   ####################

#check with python GWAlpha:
cor(ALPHA_OUT$Alpha, GWA$Alpha)
plot(ALPHA_OUT$Alpha, GWA$Alpha)

plot(ALPHA_OUT$Alpha^2, GWA$Alpha^2)
cor(ALPHA_OUT$Alpha^2, GWA$Alpha^2)

######################
# POOLED GTWAS TESTS #
######################
