#!/bin/bash
#$1 = how many parallel process you want to run
#$2 = how many iterations per process you want
parallel ./inference_genotypeDataAlone_GWASvsGWAlpha1vsGWAlpha2.r 1 500 1000 1000 10 10 1 1 {} $2 ::: $(seq 1 1 $1)
#arguments:
#(1) 1=fully multiplicative; 2=additive-multiplicative
#(2) number of individuals
#(3) number of loci
#(4) number of transcripts = number of loci
#(5) number of causal loci or QTL (can sampled from some distribution)
#(6) number of causal transcripts (can sampled from some distribution)
#(7) Verror / Vmodel
#(8) trait state (only for GWAlpha_2-pooled phenotyping): 0=quantitative mean & 1=binary survival rate
#(9) output filenames suffix
#(10) number of simulation iterations

#combine them all
cat OUT* | sed '/"AUC_GWAS","AUC_GWAlpha_1","AUC_GWAlpha_2"*$/d' > OUT.temp 
cat <(head -n1 OUT*-1.csv) OUT.temp > ALL_OUT.csv
rm OUT*