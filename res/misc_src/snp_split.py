#!/usr/bin/env python

#I'm RAM hungry. I need more than your measly 16Gb! Feh! ;-P

from datetime import *
import sys
import numpy as np
import h5py
from scipy import stats
sys.path.append("mixmogam")
# import linear_models as lm

START=datetime.now()

#Missing Value Threshold
Miss_Tol=.3
#Minor Allele Frequency Threshold
MAF_Tol=.05
#Which Models should be computed?

if "GLM" not in sys.argv:
	GLM=False
else: GLM=True

if "EMMAX" in sys.argv:
	EMMAX=True
else: EMMAX=False

#get the SNP data
with h5py.File('imputed_snps_binary.hdf5','r') as hf:
	SNP_data = hf.get('snps')
	SNP_data = np.array(SNP_data)
	Lines_geno = hf.get('accessions')
	Lines_geno = np.array(Lines_geno)
	positions = hf.get('positions')
	chr_regions = positions.attrs['chr_regions']
	Chr=np.array([])
	k=1
	for i in chr_regions[:,1]-chr_regions[:,0]:
		Chr=np.concatenate((Chr,np.repeat(k,i)))
		k+=1
	SNP_names = np.transpose(np.vstack((Chr,positions))).astype("int")

n_snps_start=SNP_data.shape[0]
hf=[]

SNP_data_all=SNP_data
SNP_names_all=SNP_names

k=n_snps_start/13
z=0
for i in range(12):
	SNP_data=SNP_data_all[(k*i):(k*(i+1))]
	SNP_names=SNP_names_all[(k*i):(k*(i+1))]
	np.save("1001genomes_SNPdata_part"+str(i+1).zfill(2)+".npy",SNP_data)
	np.save("1001genomes_SNPnames_part"+str(i+1).zfill(2)+".npy",SNP_names)
	z+=SNP_names.shape[0]
	print(i)

i+=1
SNP_data=SNP_data_all[(k*i):n_snps_start]
SNP_names=SNP_names_all[(k*i):n_snps_start]
np.save("1001genomes_SNPdata_part"+str(i+1).zfill(2)+".npy",SNP_data)
np.save("1001genomes_SNPnames_part"+str(i+1).zfill(2)+".npy",SNP_names)
z+=SNP_names.shape[0]

print(str(z)+" out of "+str(n_snps_start))









