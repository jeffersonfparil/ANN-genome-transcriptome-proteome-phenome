#!/usr/bin/env Rscript
# production code to simulate: (1) individual geno & pheno (GWAS), (2) pooled geno & individual pheno (GWAplha), and (3) pooled geno & pheno (GWAlpha)
# parallelize me via: "parallelize_main.sh"
#model1: Y1 = XKABj + e; model2: Y2 = XC1 + XKABj + e;

############
### INPUTS
############
args = commandArgs(trailingOnly=FALSE)
#R.DIR = args[1]
#slave = args[2]
#restore = args[3]
src_dir = sub("--file=", "", dirname(args[4])) #directory where the source code of the functions are located
#--args = args[5]
model = as.integer(args[6])			#0=gene effects alone; 1=fully multiplicative; 2=additive-multiplicative
n = as.integer(args[7])				#number of individuals
l = as.integer(args[8]) 			#number of loci
m = as.integer(args[9]) 			#number of transcripts (== number of loci)
q = as.integer(args[10])			#number of causal loci or QTL (can sampled from some distribution)
t = as.integer(args[11]) 			#number of causal transcripts (can sampled from some distribution)
k = as.numeric(args[12])			#Verror / Vmodel
fileNamesSuffix = args[13]			#output filenames suffix
iterations = as.integer(args[14])	#number of simulation iterations
empirical = as.integer(args[15])	#use Arabidopsis thaliana empirical genomic and transcriptomic data?: 0=No(simulate); 1=yes(use the empirical data)

#############
### OUTPUTS
#############
# An output folder will be created in the current working directory called OUT_<fileNamesSuffix>

#############
### EXAMPLE
#############
#Example run: ./main.r 0 500 1000 1000 10 10 1 Test 2 0

#############
### TESTING
#############
	# model=0
	# n = 500
	# l = 1000
	# m = l
	# q = 10
	# t = q
	# k = 1
	# fileNamesSuffix = "Full_Model"
	# iterations = 2
	# src_dir = getwd()
	# iter =1
	# empirical = 1
	# src_dir = "~/SOFTWARES/ANN-genome-transcriptome-proteome-phenome/"

#############################
######## DEFINE FUNCTIONS
#############################
source(paste0(src_dir, "/simulate_genotypes_transcripts_phenotypes.r"))
source(paste0(src_dir, "/GWAS_TWAS.r"))
source(paste0(src_dir, "/GWAlpha.r"))
source(paste0(src_dir, "/TWAlpha.r"))
source(paste0(src_dir, "/calculate_ROC_AUC.r"))
source(paste0(src_dir, "/plot_manhattan_ROC.r"))
#END OF FUNCTION DEFINITIONS#

#############################
####### SET OUTPUT DIRECTORY
#############################
system(paste0("mkdir OUT_", fileNamesSuffix, "/"))
out_dir = system(paste0("echo OUT_", fileNamesSuffix, "/"), intern=TRUE)
setwd(out_dir)

#############################
####### EXECUTION SCRIPT
#############################
AUC_GWAS = c(); PER_GWAS = c()
AUC_TWAS = c(); PER_TWAS = c()
AUC_GWAlpha_1 = c(); PER_GWAlpha_1 = c()
AUC_TWAlpha_1 = c(); PER_TWAlpha_1 = c()

AUC_GWAlpha_2 = c(); PER_GWAlpha_2 = c() #original GWAlpha: GWAlpha.py

for (iter in 1:iterations)
{
	print("#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#")
	print(paste0("Iteration ", iter, " of ", iterations))
	print("#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#")

	#################################
	###							  ###
	### S I M U L A T E   D A T A ###
	###							  ### simulate genomes, transcriptomes and phenotypes
	################################# LD simulated
	print("Simulate genomic, transcriptomic and phenotypic data:")
	SIMULATED = simulate_genomes_transcriptomes_phenotypes(model, n, l, m, q, t, k)
	X = SIMULATED$X
	T = SIMULATED$T
	Y = SIMULATED$Y
	qtl.loc = SIMULATED$qtl.loc
	qtl.additive = SIMULATED$qtl.additive
	tra.loc = SIMULATED$tra.loc
	tra.additive = SIMULATED$tra.additive
	
	qtl.labels = matrix(0, nrow=l, ncol=1)
	qtl.labels[qtl.loc,1] = 1
	tra.labels = matrix(0, nrow=m, ncol=1)
	tra.labels[tra.loc,1] = 1

	# plot(density(Y))	# ~normally distributed
	# plot(density(X))	# 3 peaks: 0, 1, 2
	# plot(density(T))	# ~chi-square distributed - folded normal dist'n

	#################################
	###							  ###
	###         G  W  A  S        ###
	###							  ### individual genoptypes
	################################# individual phenotypes
	print("GWAS:")
	LOD_GWAS = GWAS_TWAS(Y, X)
	ROC_GWAS = simple_roc.bobHorton(qtl.labels, LOD_GWAS)
	AUC_GWAS = c(AUC_GWAS, simple_auc.jef(ROC_GWAS$TPR, ROC_GWAS$FPR))
	bonferroni.cutoff = -log(0.05/n)
	PER_GWAS = plot_manhattan_ROC(LABELS=qtl.labels, LOD=LOD_GWAS, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_GWAS, N=q, METHOD="GWAS", ITER=iter, CAUSAL="SNP", LOC=qtl.loc, M=l, ROC=ROC_GWAS, AUC=AUC_GWAS)

	#################################
	###							  ###
	###         T  W  A  S        ###
	###							  ### individual RNAseq
	################################# individual phenotypes
	print("TWAS:")
	LOD_TWAS = GWAS_TWAS(Y, T)
	ROC_TWAS = simple_roc.bobHorton(tra.labels, LOD_TWAS)
	AUC_TWAS = c(AUC_TWAS, simple_auc.jef(ROC_TWAS$TPR, ROC_TWAS$FPR))
	PER_TWAS = plot_manhattan_ROC(LABELS=tra.labels, LOD=LOD_TWAS, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_TWAS, N=t, METHOD="TWAS", ITER=iter, CAUSAL="Transcript", LOC=tra.loc, M=m, ROC=ROC_TWAS, AUC=AUC_TWAS)


	#################################
	###							  ###
	###		G W A l p h a _ 1	  ###
	###							  ### pooled genotypes
	################################# individual phenotypes
	print("GWAlpha:")
	# (1) PHENOTYPING
	#arrange by phenotypic values
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	ALL = ALL[order(ALL$Y), ]
	PHEN = matrix(ALL[, 2])
	GENO = ALL[, (2+1):(2+l)]
	TRAN = ALL[, (2+l+1):(2+l+m)]
	NPOOLS=5
	POOLS = (1:5)*floor(n/NPOOLS); POOLS[3] = POOLS[3]+(n%%NPOOLS); POOLS = c(0, POOLS)
	for (i in 1:(length(POOLS)-1)) {
		index1 = POOLS[i]+1
		index2 = POOLS[i+1]
		assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
	}
	PERC = POOLS/n; PERC = PERC[2:(length(PERC)-1)]

	# (2) GENOTYPING
	# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
	SYNC = c()
	for (p in ls()[grepl("GENO_pool.", ls())]) {
		p = eval(parse(text=p))
		sync.A = colSums(p)
		sync.T = (2*nrow(p)) - sync.A
		sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
		sync = apply(sync, 1, paste, collapse=":")
		SYNC = cbind(SYNC, sync)
	}
	SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)

	# (3) GWAlpha
	SD = sqrt(var(PHEN[,1]))
	MIN = min(PHEN[,1])
	MAX = max(PHEN[,1])
	PERC = PERC
	ALPHA_GWAlpha_1 = GWAlpha(SYNC, SD, MIN, MAX, PERC)
	LL = function(data, par) {sum(-log(dnorm(data, mean=par[1], sd=par[2])))}
	OPTIM = optim(par=c(0,1), fn=LL, data=ALPHA_GWAlpha_1)
	mean_alpha = OPTIM$par[1]
	sd_alpha = OPTIM$par[2]
	LOD_GWAlpha_1 = -log(pnorm(abs(ALPHA_GWAlpha_1), mean=mean_alpha, sd=sd_alpha, lower.tail=FALSE), base=10)
	ROC_GWAlpha_1 = simple_roc.bobHorton(qtl.labels, LOD_GWAlpha_1)
	AUC_GWAlpha_1 = c(AUC_GWAlpha_1, simple_auc.jef(ROC_GWAlpha_1$TPR, ROC_GWAlpha_1$FPR))
	PER_GWAlpha_1 = plot_manhattan_ROC(LABELS=qtl.labels, LOD=LOD_GWAlpha_1, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_GWAlpha_1, N=q, METHOD="GWAlpha_1", ITER=iter, CAUSAL="SNP", LOC=qtl.loc, M=l, ROC=ROC_GWAlpha_1, AUC=AUC_GWAlpha_1)

	#clean-up:
	rm(list=ls()[grepl("GENO_pool.", ls())])

	
	#################################
	###							  ###
	###		G W A l p h a _ 2	  ### misc: GWAlpha.py test using actual GWAlpha.py script
	###							  ### 
	#################################
	write.table(SYNC, file="GWAlpha_test.sync", row.names=FALSE, col.names=FALSE, sep="\t", quote=FALSE)
	Pheno_name="GWAlpha_test"
	sig=SD
	MIN=MIN
	MAX=MAX
	perc=paste0("[", paste(PERC, collapse=","), "]")
	Q=paste0("[", paste(PHEN[POOLS[2:length(POOLS)-1]], collapse=","), "]")
	phenpy=rbind(Pheno_name, sig, MIN, MAX, perc, Q)
	PHENPY = paste0(c("Pheno_name=", "sig=", "MIN=", "MAX=", "perc=", "q="), phenpy, rep(";", times=nrow(phenpy)))
	write.table(PHENPY, file="GWAlpha_test_pheno.py", row.names=FALSE, col.names=FALSE, sep="\t", quote=FALSE)
	system("sed -i 's/GWAlpha_test/\"GWAlpha_test\"/g' GWAlpha_test_pheno.py")
	system("/volume1/SIMULATED/SRC/GWAlpha/GWAlpha.py GWAlpha_test.sync ML")
	gwalpha = read.csv("GWAlpha_GWAlpha_test_out.csv")
	ALPHA_GWAlpha_2 = gwalpha$Alpha
	OPTIM = optim(par=c(0,1), fn=LL, data=ALPHA_GWAlpha_1)
	mean_alpha = OPTIM$par[1]
	sd_alpha = OPTIM$par[2]
	LOD_GWAlpha_2 = -log(pnorm(abs(ALPHA_GWAlpha_2), mean=mean_alpha, sd=sd_alpha, lower.tail=FALSE), base=10)
	ROC_GWAlpha_2 = simple_roc.bobHorton(qtl.labels, LOD_GWAlpha_2)
	AUC_GWAlpha_2 = c(AUC_GWAlpha_2, simple_auc.jef(ROC_GWAlpha_2$TPR, ROC_GWAlpha_2$FPR))
	PER_GWAlpha_2 = plot_manhattan_ROC(LABELS=qtl.labels, LOD=LOD_GWAlpha_2, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_GWAlpha_2, N=q, METHOD="GWAlpha_2", ITER=iter, CAUSAL="SNP", LOC=qtl.loc, M=l, ROC=ROC_GWAlpha_2, AUC=AUC_GWAlpha_2)
	
	#clean-up:
	rm(list="gwalpha")
	system("rm GWAlpha_test*")

	#################################
	###							  ###
	###		T W A l p h a _ 1	  ###
	###							  ### pooled RNAseq
	################################# individual phenotypes
	print("TWAlpha:")
		# (1) PHENOTYPING
	#arrange by phenotypic values
	ID = 1:n
	ALL = data.frame(ID, Y, X, T)
	ALL = ALL[order(ALL$Y), ]
	PHEN = matrix(ALL[, 2])
	GENO = ALL[, (2+1):(2+l)]
	TRAN = ALL[, (2+l+1):(2+l+m)]
	NPOOLS=5
	POOLS = (1:5)*floor(n/NPOOLS); POOLS[3] = POOLS[3]+(n%%NPOOLS); POOLS = c(0, POOLS)
	for (i in 1:(length(POOLS)-1)) {
		index1 = POOLS[i]+1
		index2 = POOLS[i+1]
		assign(sprintf("TRAN_pool.%02d", i), TRAN[index1:index2,])	#(n individuals x m transcripts)
	}
	PERC = seq(0, 1, by=1/NPOOLS); PERC = PERC[2:(length(PERC)-1)]

	# (2) RNAseq
	# pooled RNAseq data (transform pooled RNAseq data ~(0,1)per transcript across pools)
	RNA = c()
	for (p in ls()[grepl("TRAN_pool.", ls())]) {
		p = eval(parse(text=p))
		RNA = cbind(RNA, colSums(p)) #(m transcripts x p pools)
	}
	# divide transcript abundance by the number of individuals per pool
	TRANC = c()
	for (i in 1:NPOOLS) {
		p = eval(parse(text=ls()[grepl("TRAN_pool.", ls())][i]))
		TRANC = cbind(TRANC, RNA[,i]/nrow(p))
	}
	TRANC = (TRANC-apply(TRANC, MARGIN=1, min)) + 1 # added 1 just so that we don't have 0's which results in optimization failure in TWAlpha.r +++ this improves the power!!!!20180112
	TRANC = TRANC/rowSums(TRANC)
	
	# (3) TWAlpha
	SD = sqrt(var(PHEN[,1]))
	MIN = min(PHEN[,1])
	MAX = max(PHEN[,1])
	PERC = PERC
	ALPHA_TWAlpha_1 = TWAlpha(TRANC, SD, MIN, MAX, PERC)
	LL = function(data, par) {sum(-log(dnorm(data, mean=par[1], sd=par[2])))}
	OPTIM = optim(par=c(0,1), fn=LL, data=ALPHA_TWAlpha_1)
	mean_alpha = OPTIM$par[1]
	sd_alpha = OPTIM$par[2]
	LOD_TWAlpha_1 = -log(pnorm(abs(ALPHA_TWAlpha_1), mean=mean_alpha, sd=sd_alpha, lower.tail=FALSE)) #folded the alpha's for simple pnorm upper tail only
	ROC_TWAlpha_1 = simple_roc.bobHorton(tra.labels, LOD_TWAlpha_1)
	AUC_TWAlpha_1 = c(AUC_TWAlpha_1, simple_auc.jef(ROC_TWAlpha_1$TPR, ROC_TWAlpha_1$FPR))
	PER_TWAlpha_1 = plot_manhattan_ROC(LABELS=tra.labels, LOD=LOD_TWAlpha_1, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_TWAlpha_1, N=t, METHOD="TWAlpha_1", ITER=iter, CAUSAL="Transcript", LOC=tra.loc, M=m, ROC=ROC_TWAlpha_1, AUC=AUC_TWAlpha_1)

	#clean-up:
	rm(list=ls()[grepl("TRAN_pool.", ls())])
}

#########################
########################
######################
####################
#  ULTIMATE OUTPUT
####################
######################
########################
#########################

methods = ls()[grep("AUC_", ls())]

MODEL 	= rep(rep(model, times=length(AUC_GWAS)), times=length(methods))
N 		= rep(rep(n, times=length(AUC_GWAS)), times=length(methods))
L 		= rep(rep(l, times=length(AUC_GWAS)), times=length(methods))
M 		= rep(rep(m, times=length(AUC_GWAS)), times=length(methods))
Q 		= rep(rep(q, times=length(AUC_GWAS)), times=length(methods))
T 		= rep(rep(t, times=length(AUC_GWAS)), times=length(methods))
K 		= rep(rep(k, times=length(AUC_GWAS)), times=length(methods))
ITER 	= rep(seq(1, iterations, by=1), times=length(methods))
METHODS = rep(sub("AUC_", "", methods), each=length(AUC_GWAS))

AUC 		= eval(parse(text=paste0("c(", paste(ls()[grep("AUC_", ls())], collapse=","), ")")))
PERFORMANCE	= eval(parse(text=paste0("c(", paste(ls()[grep("PER_", ls())], collapse=","), ")")))

OUT = data.frame(MODEL, N, L, M, Q, T, K, ITER, METHODS, AUC, PERFORMANCE)
write.csv(OUT, file=paste("PERFORMANCE-", fileNamesSuffix, ".csv", sep=""), row.names=F)
