#!/usr/bin/env Rscript

#Simulating phenotypes as a function of SNP and transcripts effects singly and in simple additive combinations

src_dir = "~/SOFTWARES/ANN-genome-transcriptome-proteome-phenome/"

source(paste0(src_dir, "/GWAS_TWAS.r"))
source(paste0(src_dir, "/GWAlpha.r"))
source(paste0(src_dir, "/TWAlpha.r"))
source(paste0(src_dir, "/calculate_ROC_AUC.r"))
source(paste0(src_dir, "/plot_manhattan_ROC.r"))

AUC_GWAS = c(); PER_GWAS = c()
AUC_GWAlpha_1 = c(); PER_GWAlpha_1 = c()
iter=1

GENOMES = readRDS("/volume1/Salk_Athaliana_genomes_transcriptomes/GENOMES/At_genomes.rds")
TRANSCRIPTOMES = readRDS("/volume1/Salk_Athaliana_genomes_transcriptomes/TRANSCRIPTOMES/At_transcriptomes.rds")
X.temp = t(GENOMES[,2:ncol(GENOMES)]); X.temp = X.temp[order(rownames(X.temp)),]
T.temp = t(TRANSCRIPTOMES[,2:ncol(TRANSCRIPTOMES)]); T.temp = T.temp[order(rownames(T.temp)),]
XT.mer = merge(X.temp, T.temp,by="row.names")
X = as.matrix(XT.mer[,2:(ncol(X.temp)+1)]) #exclude the Row.names column
X = X[,colSums(X)!=0 & colSums(X)!=ncol(X)] #remove monomorphic sites
MAF = 0.01
X = X[,colSums(X)>=MAF] #filter loci at >= minimum allele frequency MAF
T = as.matrix(XT.mer[,(ncol(X.temp)+2):(ncol(X.temp)+1+ncol(T.temp))])

#X too big random sampling 1000 loci here:
X = X[,sample(1:ncol(X), size=2000, replace=FALSE)]
#remove highly correlated loci
COR = cor(X)
COR[upper.tri(COR)] = 0
diag(COR) = 0
X = X[, !apply(COR, 2, function(x) any(x>0.99))]
# #simulating X without LD
# n = nrow(X)
# l = 2000
# p = rbeta(l, 2, 2)
# X = matrix(rbinom(n*l, 1, p), nrow=n)

n = nrow(X) #no. of individuals
l = ncol(X) #no. of loci
m = ncol(T) #no. of transcripts
k = 0.5 #Ve/Vp

# SNP model: y = Xb + e
q = 10 #no. of QTL

qtl.loc = sample(1:l, size=q, replace=FALSE)
qtl.labels = matrix(0, nrow=l, ncol=1)
qtl.labels[qtl.loc,1] = 1 #SNP labels: 1 for QTL and 0 for non-QTL
qtl.eff = rgamma(n=q, shape=10) #sample QTL effects from a gamma distribution
# Va = 10
# qtl.eff = rnorm(n=q, mean=5, sd=Va) #positive effects only, for simplicity
b = matrix(0, nrow=l, ncol=1)
b[qtl.loc, 1] = qtl.eff

Vmodel = var(X%*%b)
Verror = k*Vmodel
e = matrix(rnorm(n=n, mean=0, sd=sqrt(Verror)), nrow=n, ncol=1)

Y = X%*%b + e
Y = (Y-min(Y))/(max(Y)-min(Y)) #bound between 0,1

par(mfrow=c(2,2))
plot(x=1:l, y=qtl.labels, type="l", main="QTL locations", xlab="Sites", ylab="")
hist(qtl.eff, main="QTL effects", xlab="QTL effect", ylab="Frequency")
hist(Y, main="Phenotypes", xlab="Phenotypic value", ylab="Frequency")
hist(e, main="Error effects", xlab="Error effects", ylab="Frequency")

#################################
###							  ###
###         G  W  A  S        ###
###							  ### individual genoptypes
################################# individual phenotypes
print("GWAS:")
LOD_GWAS = GWAS_TWAS(Y, X)
ROC_GWAS = simple_roc.bobHorton(qtl.labels, LOD_GWAS)
AUC_GWAS = c(AUC_GWAS, simple_auc.jef(ROC_GWAS$TPR, ROC_GWAS$FPR))
bonferroni.cutoff = -log(0.05/l, base=10)
PER_GWAS = plot_manhattan_ROC(LABELS=qtl.labels, LOD=LOD_GWAS, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_GWAS, N=q, METHOD="GWAS", ITER=iter, CAUSAL="SNP", LOC=qtl.loc, M=l, ROC=ROC_GWAS, AUC=AUC_GWAS, PLOT=TRUE)


#############################
###						  ###
###		G W A l p h a	  ###
###						  ### pooled genotypes
############################# individual phenotypes
print("GWAlpha:")
# (1) PHENOTYPING
#arrange by phenotypic values
ID = 1:n
ALL = data.frame(ID, Y, X, T)
ALL = ALL[order(ALL$Y), ]
PHEN = matrix(ALL[, 2])
GENO = ALL[, (2+1):(2+l)]
TRAN = ALL[, (2+l+1):(2+l+m)]
NPOOLS=5
POOLS = (1:5)*floor(n/NPOOLS); POOLS[3] = POOLS[3]+(n%%NPOOLS); POOLS = c(0, POOLS)
for (i in 1:(length(POOLS)-1)) {
	index1 = POOLS[i]+1
	index2 = POOLS[i+1]
	assign(sprintf("GENO_pool.%02d", i), GENO[index1:index2,])
}
PERC = POOLS/n; PERC = PERC[2:(length(PERC)-1)]

# (2) GENOTYPING
# convert numeric genotypes into the sync format of allele frequencies (set all loci as biallelic A/T only for simplicity!)
SYNC = c()
for (p in ls()[grepl("GENO_pool.", ls())]) {
	p = eval(parse(text=p))
	sync.A = colSums(p)
	# sync.T = (2*nrow(p)) - sync.A #for heterozygote genotype coding: 0,1,2
	sync.T = (1*nrow(p)) - sync.A #for homozygote genotype coding: 0,1
	sync = cbind(sync.A, sync.T, rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)), rep(0, times=length(sync.A)))
	sync = apply(sync, 1, paste, collapse=":")
	SYNC = cbind(SYNC, sync)
}
SYNC = cbind(rep(c("1L"), nrow(SYNC)), 1:nrow(SYNC), rep(c("N"), nrow(SYNC)), SYNC)

# (3) GWAlpha
SD = sqrt(var(PHEN[,1]))
MIN = min(PHEN[,1])
MAX = max(PHEN[,1])
PERC = PERC
ALPHA_GWAlpha_1 = GWAlpha(SYNC, SD, MIN, MAX, PERC)
LL = function(data, par) {sum(-log(dnorm(data, mean=par[1], sd=par[2])))}
OPTIM = optim(par=c(0,1), fn=LL, data=ALPHA_GWAlpha_1)
mean_alpha = OPTIM$par[1]
sd_alpha = OPTIM$par[2]
LOD_GWAlpha_1 = -log(pnorm(abs(ALPHA_GWAlpha_1), mean=mean_alpha, sd=sd_alpha, lower.tail=FALSE), base=10)
ROC_GWAlpha_1 = simple_roc.bobHorton(qtl.labels, LOD_GWAlpha_1)
AUC_GWAlpha_1 = c(AUC_GWAlpha_1, simple_auc.jef(ROC_GWAlpha_1$TPR, ROC_GWAlpha_1$FPR))
PER_GWAlpha_1 = plot_manhattan_ROC(LABELS=qtl.labels, LOD=LOD_GWAlpha_1, CUT.OFF=bonferroni.cutoff, PERFORMANCE=PER_GWAlpha_1, N=q, METHOD="GWAlpha_1", ITER=iter, CAUSAL="SNP", LOC=qtl.loc, M=l, ROC=ROC_GWAlpha_1, AUC=AUC_GWAlpha_1, PLOT=TRUE)

#clean-up:
rm(list=ls()[grepl("GENO_pool.", ls())])

