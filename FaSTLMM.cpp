#include <RcppGSL.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <cmath>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_statistics_double.h>

////////////////////////
///					 ///
/// Matrix inversion ///
///					 ///
////////////////////////
// gsl_matrix *INVERSE(gsl_matrix *X){ //LU decompose followed by inverting this LU
// 	int n = X->size1;
// 	gsl_permutation *p = gsl_permutation_alloc(n); //permutation input of LU decomposition
// 	int s; //kind of like a workspace me thinks...
//     gsl_linalg_LU_decomp(X, p, &s); //LU decomposition
//     gsl_matrix *inverse = gsl_matrix_alloc(n, n); //setup the inverse
//     gsl_linalg_LU_invert(X, p, inverse); //inverse of th LU decomposition
//     gsl_permutation_free(p); //release memmory block from the permutation variable
//     return(inverse);
// }

/////////////////////////////////
///							  ///
/// BETA (parameter function) ///
///							  ///
///////////////////////////////// beta = solve(sum(alpha_i*(utx_i)t * utx_i) * sum(alpha_i*(utx_i)t * uty_i)
gsl_vector *BETA(double delta, gsl_vector *S, gsl_matrix *U, gsl_matrix *Xin, gsl_vector *yin) {
	int n = Xin->size1; //number of observations or individuals
	int s = Xin->size2; //number of columns in the X incidence matrix which is 2. The first one is the intercept the second one is the SNP
	gsl_matrix *E1 = gsl_matrix_alloc(s,s); //define expression 1 matrix output:::: where beta = solve(E1) %*% e2
	gsl_vector *e2 = gsl_vector_alloc(s); //define expression 2 matrix output:::: where beta = solve(E1) %*% e2
	gsl_matrix_set_all(E1, 0); //initialize with zeros
	gsl_vector_set_all(e2, 0); //initialize with zeros
	gsl_matrix *UtX = gsl_matrix_alloc(n, s); //setup t(eigenvectors) %*% X for E1 where E1 = sum_i(alpha * t(eigenvectors) %*% X)
	gsl_vector *Uty = gsl_vector_alloc(n); //setup t(eigenvectors) %*% y for e2 where e2 = sum_i(alpha * t(eigenvectors) %*% y)
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, U, Xin, 0.0, UtX); //matrix multiplication: t(eigenvectors) %*% X
	gsl_blas_dgemv(CblasTrans, 1.0, U, yin, 0.0, Uty); //matrix vector multiplication: t(eigenvectors) %*% y
	double alpha;
	// gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, eigenvec, X, 0.0, UtX); //matrix multiplication: t(eigenvectors) %*% X
	// gsl_blas_dgemv(CblasNoTrans, 1.0, eigenvec, y, 0.0, Uty); //matrix vector multiplication: t(eigenvectors) %*% y
	gsl_matrix *utx = gsl_matrix_alloc(1, s);
	gsl_vector *uty = gsl_vector_alloc(1);
	gsl_matrix *utxt_utx = gsl_matrix_alloc(s, s);
	gsl_vector *utxt_uty = gsl_vector_alloc(s);
	for (int i=0; i<n; i++){
		// printf("%f\n", s);
		//setup each i->n slices as matrices for simplicity of matrix multiplication function::: just using gsl_blas_dgemm
		// gsl_matrix *utxt = gsl_matrix_alloc(s, 1);
		for (int j=0; j<s; j++){
			// printf("%i\n", j);
			// gsl_matrix_set(utxt, j, i, gsl_matrix_get(UtX, i, j)); //extract t(t(U)X)_i
			gsl_matrix_set(utx, 0, j, gsl_matrix_get(UtX, i, j)); //extract (t(U)X)_i
		}
		gsl_vector_set(uty , 0, gsl_vector_get(Uty, i)); //extract (t(U)y)_i
		alpha = 1.0/(gsl_vector_get(S, i) + delta); //compute alpha as 1 / eigenvalue + delta; where delta  = Verror/Vgenetic
		// gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, alpha, utxt, utx, 0.0, utxt_utx); //E1_i
		// gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, utxt, utx, 0.0, utxt_utx); //E1_i
		gsl_blas_dgemm(CblasTrans, CblasNoTrans, alpha, utx, utx, 0.0, utxt_utx); //E1_i
		// gsl_matrix_scale(utxt_utx, alpha);
		gsl_matrix_add(E1, utxt_utx); //E1 = E1 + E1_i
		// gsl_blas_dgemv(CblasNoTrans, alpha, utxt, uty, 0.0, utxt_uty); //e2_i
		// gsl_blas_dgemv(CblasNoTrans, 1.0, utxt, uty, 0.0, utxt_uty); //e2_i
		gsl_blas_dgemv(CblasTrans, alpha, utx, uty, 0.0, utxt_uty); //e2_i
		// gsl_vector_scale(utxt_uty, alpha);
		gsl_vector_add(e2, utxt_uty); //e2 = e2 + e2_i
		//cleanup
		// gsl_matrix_free(utxt);
	}

	gsl_vector *b = gsl_vector_alloc(s); //setup output vector
	// gsl_blas_dgemv(CblasNoTrans, 1.0, INVERSE(E1), e2, 0.0, beta); //beta = solve(E1) %*% e2
	gsl_permutation *p = gsl_permutation_alloc(s); //permutation input of LU decomposition
	int w; //kind of like a workspace me thinks...
    gsl_linalg_LU_decomp(E1, p, &w); //LU decomposition
    gsl_linalg_LU_solve(E1, p, e2, b); //solve for beta
 	// gsl_vector_fprintf (stdout, e2, "%g");
 	// gsl_matrix_fprintf (stdout, E1, "%g");

	//cleanup
	gsl_matrix_free(utx);
	gsl_vector_free(uty);
	gsl_matrix_free(utxt_utx);
	gsl_vector_free(utxt_uty);
	gsl_matrix_free(E1);
	gsl_vector_free(e2);
	gsl_matrix_free(UtX);
	gsl_vector_free(Uty);
	gsl_permutation_free(p);

	return(b);
	// gsl_vector_free(beta);
}

////////////////////////////
///
/// Standard errors of beta elements! 
///
////////////////////////////////
// gsl_vector *BETA(double delta, gsl_vector *S, gsl_matrix *U, gsl_matrix *Xin, gsl_vector *yin) {
double SE_IMMATURE(double delta, gsl_vector *S, gsl_matrix *U, gsl_matrix *Xin) {
	int n = Xin->size1; //number of observations or individuals
	int s = Xin->size2; //number of columns in the X incidence matrix which is 2. The first one is the intercept the second one is the SNP
	gsl_matrix *E1 = gsl_matrix_alloc(s,s); //define expression 1 matrix output:::: where beta = solve(E1) %*% e2
	// gsl_vector *e2 = gsl_vector_alloc(s); //define expression 2 matrix output:::: where beta = solve(E1) %*% e2
	gsl_matrix_set_all(E1, 0); //initialize with zeros
	// gsl_vector_set_all(e2, 0); //initialize with zeros
	gsl_matrix *UtX = gsl_matrix_alloc(n, s); //setup t(eigenvectors) %*% X for E1 where E1 = sum_i(alpha * t(eigenvectors) %*% X)
	// gsl_vector *Uty = gsl_vector_alloc(n); //setup t(eigenvectors) %*% y for e2 where e2 = sum_i(alpha * t(eigenvectors) %*% y)
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, U, Xin, 0.0, UtX); //matrix multiplication: t(eigenvectors) %*% X
	// gsl_blas_dgemv(CblasTrans, 1.0, U, yin, 0.0, Uty); //matrix vector multiplication: t(eigenvectors) %*% y
	double alpha;
	// gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, eigenvec, X, 0.0, UtX); //matrix multiplication: t(eigenvectors) %*% X
	// gsl_blas_dgemv(CblasNoTrans, 1.0, eigenvec, y, 0.0, Uty); //matrix vector multiplication: t(eigenvectors) %*% y
	gsl_matrix *utx = gsl_matrix_alloc(1, s);
	// gsl_vector *uty = gsl_vector_alloc(1);
	gsl_matrix *utxt_utx = gsl_matrix_alloc(s, s);
	// gsl_vector *utxt_uty = gsl_vector_alloc(s);
	gsl_matrix *E1_solve = gsl_matrix_alloc(s,s);
	for (int i=0; i<n; i++){
		// printf("%f\n", s);
		//setup each i->n slices as matrices for simplicity of matrix multiplication function::: just using gsl_blas_dgemm
		// gsl_matrix *utxt = gsl_matrix_alloc(s, 1);
		for (int j=0; j<s; j++){
			// printf("%i\n", j);
			// gsl_matrix_set(utxt, j, i, gsl_matrix_get(UtX, i, j)); //extract t(t(U)X)_i
			gsl_matrix_set(utx, 0, j, gsl_matrix_get(UtX, i, j)); //extract (t(U)X)_i
		}
		// gsl_vector_set(uty , 0, gsl_vector_get(Uty, i)); //extract (t(U)y)_i
		alpha = 1.0/(gsl_vector_get(S, i) + delta); //compute alpha as 1 / eigenvalue + delta; where delta  = Verror/Vgenetic
		// gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, alpha, utxt, utx, 0.0, utxt_utx); //E1_i
		// gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, utxt, utx, 0.0, utxt_utx); //E1_i
		gsl_blas_dgemm(CblasTrans, CblasNoTrans, alpha, utx, utx, 0.0, utxt_utx); //E1_i
		// gsl_matrix_scale(utxt_utx, alpha);
		gsl_matrix_add(E1, utxt_utx); //E1 = E1 + E1_i
		// gsl_blas_dgemv(CblasNoTrans, alpha, utxt, uty, 0.0, utxt_uty); //e2_i
		// gsl_blas_dgemv(CblasNoTrans, 1.0, utxt, uty, 0.0, utxt_uty); //e2_i
		// gsl_blas_dgemv(CblasTrans, alpha, utx, uty, 0.0, utxt_uty); //e2_i
		// gsl_vector_scale(utxt_uty, alpha);
		// gsl_vector_add(e2, utxt_uty); //e2 = e2 + e2_i
		//cleanup
		// gsl_matrix_free(utxt);
	}
    // printf("sum(a*UtXt_UtX computed!\n");

	// gsl_vector *b = gsl_vector_alloc(s); //setup output vector
	// gsl_blas_dgemv(CblasNoTrans, 1.0, INVERSE(E1), e2, 0.0, beta); //beta = solve(E1) %*% e2
	gsl_permutation *p = gsl_permutation_alloc(s); //permutation input of LU decomposition
	int w; //kind of like a workspace me thinks...
    gsl_linalg_LU_decomp(E1, p, &w); //LU decomposition
    // gsl_linalg_LU_solve(E1, p, e2, b); //solve for beta
 	// gsl_vector_fprintf (stdout, e2, "%g");
 	// gsl_matrix_fprintf (stdout, E1, "%g");
    gsl_linalg_LU_invert(E1, p, E1_solve); //inverse of X'X

    double se_immature = sqrt(gsl_matrix_get(E1_solve, s-1, s-1));
    // printf("se computed!\n");
	//cleanup
	gsl_matrix_free(utx);
	// gsl_vector_free(uty);
	gsl_matrix_free(utxt_utx);
	// gsl_vector_free(utxt_uty);
	gsl_matrix_free(E1);
	gsl_matrix_free(E1_solve);
	// gsl_vector_free(e2);
	gsl_matrix_free(UtX);
	// gsl_vector_free(Uty);
	gsl_permutation_free(p);

	return(se_immature);
	// gsl_vector_free(beta);
}


/////////////////////////////
///						  ///
/// Vg (genetic variance) ///
///						  ///
///////////////////////////// Vg = (1/n)*sum( ((t(U)%*%y)_i - (t(U)%*%X)_i %*% BETA(delta))^2 / (eigenval_i + delta) )
double Vg(double delta, gsl_vector *S, gsl_matrix *U, gsl_matrix *Xin, gsl_vector *yin) {
	int n = Xin->size1; //number of observations or individuals
	int s = Xin->size2; //number of columns in the X incidence matrix which is 2. The first one is the intercept the second one is the SNP
	gsl_vector *Uty = gsl_vector_alloc(n); //setup the vector t(U)_%*%y
	gsl_matrix *UtX = gsl_matrix_alloc(n, s); //setup the matrix t(U)%*%X
	gsl_vector *b = gsl_vector_alloc(s); //setup beta vector
	gsl_vector *UtXb = gsl_vector_alloc(n); //setup the matrix t(U)%*%X
	gsl_blas_dgemv(CblasTrans, 1.0, U, yin, 0.0, Uty); //matrix vector multiplication: t(eigenvectors) %*% y
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, U, Xin, 0.0, UtX); //matrix multiplication: t(eigenvectors) %*% X
	// gsl_blas_dgemv(CblasNoTrans, 1.0, eigenvec, y, 0.0, Uty); //matrix vector multiplication: t(eigenvectors) %*% y
	// gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, eigenvec, X, 0.0, UtX); //matrix multiplication: t(eigenvectors) %*% X
	b = BETA(delta, S, U, Xin, yin); //compute beta
	// printf("delta=%f, int=%f, eff=%f\n", delta, gsl_vector_get(beta, 0), gsl_vector_get(beta, 1));
	gsl_blas_dgemv(CblasNoTrans, 1.0, UtX, b, 0.0, UtXb); //matrix by vector multiplication
	double SUM = 0.0; //initialize the summation i:1->n
	for (int i=0; i<n; i++){
		SUM = SUM + ( pow((gsl_vector_get(Uty, i) - gsl_vector_get(UtXb, i)), 2.0) / (gsl_vector_get(S, i) + delta) ); //summation
	}
	double OUT = (1.0/n)*SUM; //divide the sum by n
	//cleanup
	gsl_matrix_free(UtX);
	gsl_vector_free(Uty);
	gsl_vector_free(b);
	gsl_vector_free(UtXb);
	return(OUT);
}

///////////////////////////
///						///
/// Likelihood Function ///
///						///
///////////////////////////
struct dataInput { gsl_vector *S; gsl_matrix *U; gsl_matrix *Xin; gsl_vector *yin; };
// double LL_delta(inputs *args, double delta) {
double LL_delta(double delta, void *params) {
// double LL_delta(double delta, gsl_vector *eigenval, gsl_matrix *eigenvec, gsl_matrix *X, gsl_vector *y) {
	struct dataInput *data = (struct dataInput *)params;
	gsl_vector *S = (*data).S;
	gsl_matrix *U = (*data).U;
	gsl_matrix *Xin = (*data).Xin;
	gsl_vector *yin = (*data).yin;
	int n = (*Xin).size1; //number of observations or individuals
	int s = (*Xin).size2; //number of columns in the X incidence matrix which is 2. The first one is the intercept the second one is the SNP
	double e1, e2, e3, vg, e4;
	e1 = n * log10(2.0*M_PI);
	// e1 = n * log10(2.0*M_PI*Vg(delta, eigenval, eigenvec, X, y));
	e2 = 0.0;
	for (int i=0; i<n; i++){
		e2 = e2 + log10(gsl_vector_get(S, i) + delta);
		// printf("eigenval = %f\t", gsl_vector_get(eigenval, i));
	}
	e3 = n;
	vg = Vg(delta, S, U, Xin, yin);
	e4 = n * log10(vg);
	// e4 = n * log10(1.0/n) * (n*Vg(delta, eigenval, eigenvec, X, y) );
	// printf("\ne1 = %f\n e2 = %f\n e3 = %f\n e4 = %f\n", e1, e2, e3, e4);
	// printf("LL = %f\n", (-1.0/2.0) * (e1 + e2 + e3 + e4));
	// printf("delta = %f\n", delta);
	// printf("Vg = %f\n", Vg(delta, eigenval, eigenvec, X, y));
	double OUT = (-1.0/2.0) * (e1 + e2 + e3 + e4);
	// double OUT = (-1.0/2.0) * (e1 + e2 + e3);
	//cleanup
	// gsl_vector_free(eigenval);
	// gsl_matrix_free(eigenvec);
	// gsl_matrix_free(X);
	// gsl_vector_free(y);

	return(-OUT); //minimize the -likelihood == maximize the likelihood
	// return(OUT);
}

/////////////////////////////
///						  ///
/// Minimization Function ///
///						  /// 2018/09/29 ---> this is where the problem seem to be. Most probably I'm not implementing the minimization properly.
///////////////////////////// 2018/09/29 ---> beta, Vg and LL functions all seem to working as expected decreases or increases as delta and snp changes from QTL to non-QTL
gsl_vector *Brent_minimization(gsl_vector *S, gsl_matrix *U, gsl_matrix *Xin, gsl_vector *yin) {
	int n = Xin->size1; //number of observations or individuals
	int s = Xin->size2; //number of columns in the X incidence matrix which is 2. The first one is the intercept the second one is the SNP
	struct dataInput params = {S, U, Xin, yin};
	// double delta = 0.5;
	gsl_function logLik;
	logLik.function = &LL_delta;
	logLik.params = &params;

	const gsl_min_fminimizer_type *min_workspace;
	gsl_min_fminimizer *minimizer;
	min_workspace = gsl_min_fminimizer_brent;
	minimizer = gsl_min_fminimizer_alloc (min_workspace);

	// double lower = 0.01; //lower bound
	// double MINAR = 5.00; //estimated point between lower and upper bounds where the function is minimized
	// double upper = 10.0; //upper bound
	double lower = 0.01; //lower bound
	double MINAR = 0.05; //estimated point between lower and upper bounds where the function is minimized
	double upper = 1.00; //upper bound

	gsl_min_fminimizer_set (minimizer, &logLik, MINAR, lower, upper);
	int status;
	int niter = 1000;
	int iter = 0;
	do {
		iter++;
		status = gsl_min_fminimizer_iterate (minimizer);

		lower = gsl_min_fminimizer_x_lower (minimizer);
		MINAR = gsl_min_fminimizer_x_minimum (minimizer);
		upper = gsl_min_fminimizer_x_upper (minimizer);

		status = gsl_min_test_interval (lower, upper, 0.000001, 0.0);

		// if (status == GSL_SUCCESS)
		// printf ("Converged:\n");

		// printf ("%5d [%.7f, %.7f] " "%.7f\n", iter, a, b, m);
		} while (status == GSL_CONTINUE && iter < niter);
	// MINAR = 1000000; //can be estimated using the null model and used for all SNPs without minimization
	gsl_vector *b = gsl_vector_alloc(s);
	b = BETA(MINAR, S, U, Xin, yin);
	double vg = Vg(MINAR, S, U, Xin, yin);
	// double tcalc = gsl_vector_get(beta, 1) / vg;
	// double pval = 1 - gsl_cdf_tdist_P(tcalc, n); //needed to subtract from 1 since the cdf function starts from the lower tail
	// gsl_vector *OUT = gsl_vector_alloc(2); //element 1 is the SNP effect and the second element is the t-test p-values
	// gsl_vector_set(OUT, 0, gsl_vector_get(beta, 1));
	// gsl_vector_set(OUT, 1, pval);
	gsl_vector *OUT = gsl_vector_alloc(4); //intercept, SNP effect, delta & Vg
	gsl_vector_set(OUT, 0, gsl_vector_get(b, 0)); // intercept
	gsl_vector_set(OUT, 1, gsl_vector_get(b, 1)); // SNP effect
	gsl_vector_set(OUT, 2, MINAR); // delta that minimizes the log-likelihood function
	gsl_vector_set(OUT, 3, vg); // genetic variance
	//cleanup
	gsl_min_fminimizer_free(minimizer);
	gsl_vector_free(b);
	return(OUT);
	// gsl_vector_free(OUT);
}


/////////////////////
///				  ///
// MAIN FUNCTION! ///
///				  ///
/////////////////////

// [[Rcpp::depends(RcppGSL)]]
// [[Rcpp::export]]
Rcpp::List FaST_LMM(const RcppGSL::Matrix & X_INPUT, const RcppGSL::Vector & y_INPUT) {
// gsl_vector FaST_LMM(const RcppGSL::Matrix & X_INPUT, const RcppGSL::Vector & y_INPUT) {
// gsl_matrix FaST_LMM(const RcppGSL::Matrix & X_INPUT, const RcppGSL::Vector & y_INPUT) {
// double FaST_LMM(const RcppGSL::Matrix & X, const RcppGSL::Vector & y) {
	//define variables
	int n = X_INPUT.nrow(); //number of individuals
	int l = X_INPUT.ncol(); //number of loci or SNPs
	// int lk = l-1; //less the SNP in question
	int frac_sub = 10; //fraction of the loci subsetted for building the kinship matrix K
	int lk = l/frac_sub; //less the SNP in question

	int s = 2; //number of columns in the design matrix X::: column1 === intercept & second one is the SNP
	// l=10;
	Rcpp::NumericVector intcpt(l);
	Rcpp::NumericVector eff(l);
	Rcpp::NumericVector delta(l);
	Rcpp::NumericVector vg(l);
	Rcpp::NumericVector stderr(l);
	Rcpp::NumericVector tcalc(l);
	Rcpp::NumericVector pval(l);
	Rcpp::NumericVector lod(l);
	gsl_vector *y_test = gsl_vector_alloc(n); //convert RcppGSL vector  to gsl_vector
	for (int i=0; i<n; i++){
			gsl_vector_set(y_test, i, y_INPUT[i]); //set y values to convert RcppGSL to gsl_vector
	}
	gsl_vector *ARGMIN_OUT = gsl_vector_alloc(4); //intercept, SNP effect, delta & Vg
	gsl_matrix *X_test = gsl_matrix_alloc(n, s); //first column is the intercept; and second column is the SNP
	double intercept, SNP_effect, Delta, Genetic_var, Error_var, Std_error, T_calc, P_val;
	gsl_matrix *Xsub = gsl_matrix_alloc(n,lk); //initialize the kinship matrix input subsetted genotype data
	gsl_matrix *XXt = gsl_matrix_alloc(n, n);
	gsl_matrix *Kcol = gsl_matrix_alloc(n, n);
	gsl_matrix *Krow = gsl_matrix_alloc(n, n);
	gsl_matrix *K = gsl_matrix_alloc(n, n);
	gsl_vector *eigenval = gsl_vector_alloc(n); //vector of eigenvalues
	gsl_matrix *eigenvec = gsl_matrix_alloc(n, n); //matrix of eigenvectors

	///////////////////////////////////////////////////////////////////
	//BUILD KINSHIP MATRIX ONCE
		//sample a small subset of the SNP data for building the K matrix (proxy for the kinship matrix)
		int l_idx[l]; //create an index array from 0 to l
		int lk_idx[lk]; //create an index array  of loci to be used in the kinship matrix
		gsl_rng *r = gsl_rng_alloc(gsl_rng_taus); //initialize random number generator
		gsl_rng_set(r, time(NULL)); //seed with time
		for (int i=0; i<l; i++) {l_idx[i]=i;} //range 0 to l
		gsl_ran_choose(r, lk_idx, lk, l_idx, l, sizeof(int)); //randonm sampling of lk indices from l_idx into lk_idx followed by ordering of elements in the output array lk_idx
		for (int i=0; i<n; i++) {// fill up OUT with randomly chosen loci
			for (int j=0; j<lk; j++) {		
				gsl_matrix_set(Xsub, i, j, X_INPUT(i,lk_idx[j]));
				// gsl_matrix_set(XsubT, j, i, X(i,lk_idx[j]));
				// printf("%i\n", lk_idx[j]);
			}
		}

		// //subset the genotype matrix - including only every 1
		// int idx[lk];
		// for ()
		
		//build kinship matrix
		gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, Xsub, Xsub, 0.0, XXt); //XX'
		// gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, Xsub, Xsub, 0.0, K); //XX'
		double scaler, scaled;
		//by column
		for (int j=0; j<n; j++){
			scaler = gsl_matrix_get(XXt, j, j);
			for (int i=0; i<n; i++){
				scaled = (gsl_matrix_get(XXt, i, j) / scaler);
				gsl_matrix_set(Kcol, i, j, scaled); //scaled(XX')
			}
		}
		printf("scaled by column!\n");
		//by row
		for (int i=0; i<n; i++){
			scaler = gsl_matrix_get(XXt, i, i);
			for (int j=0; j<n; j++){
				scaled = (gsl_matrix_get(XXt, i, j) / scaler);
				gsl_matrix_set(Krow, i, j, scaled); //scaled(XX')
			}
		}
		printf("scaled by column!\n");
		//take the mean of Kcol and Krow to get the kinship matrix K
		for (int i=0; i<n; i++){
			for (int j=0; j<n; j++){
				gsl_matrix_set(K, i, j, ((gsl_matrix_get(Kcol, i, j) + gsl_matrix_get(Krow, i, j))/2.0));
			}
		}
		printf("Kinship matrix estimated!\n");
		// do eigendecomposition of the pseudo-Kinship matrix K
		gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(n);
		gsl_eigen_symmv(K, eigenval, eigenvec, w);
		gsl_eigen_symmv_sort(eigenval, eigenvec, GSL_EIGEN_SORT_ABS_DESC);
		printf("Spectral decomposition complete!\n");
		// free pointers
		gsl_matrix_free(Xsub);
		gsl_matrix_free(XXt);
		gsl_matrix_free(Kcol);
		gsl_matrix_free(Krow);
		gsl_rng_free(r);
		gsl_eigen_symmv_free(w);
	///////////////////////////////////////////////////////////////////
	gsl_vector *b = gsl_vector_alloc(s);
	gsl_vector *e = gsl_vector_alloc(n);
	//iterate per SNP
	for (int snp=0; snp<l; snp++){
	// for (int snp=0; snp<1; snp++){
		for (int i=0; i<n; i++){
			gsl_matrix_set(X_test, i, 0, 1); //set intercept column into 1's
			gsl_matrix_set(X_test, i, 1, X_INPUT(i, snp)); //set the SNP value in the second column
		}

		ARGMIN_OUT = Brent_minimization(eigenval, eigenvec, X_test, y_test);
		intercept = gsl_vector_get(ARGMIN_OUT, 0);
		SNP_effect = gsl_vector_get(ARGMIN_OUT, 1);
		Delta = gsl_vector_get(ARGMIN_OUT, 2);
		Genetic_var = gsl_vector_get(ARGMIN_OUT, 3);
		// Error_var = Delta * Genetic_var;
		//alternative computation of error variance via e(beta) = y - Xbeta; V(e) = 1/n *e'e //test 20181009
		// gsl_vector *b = gsl_vector_alloc(s);
		// gsl_vector *e = gsl_vector_alloc(n);
		gsl_vector_set(b, 0, intercept);
		gsl_vector_set(b, 1, SNP_effect);
		gsl_blas_dgemv(CblasNoTrans, -1.0, X_test, b, 0.0, e);
		gsl_vector_add(e, y_test); //-Xb + y
		gsl_blas_ddot(e, e, &Error_var); //e'e
		Error_var = 1.0/n * Error_var;
		// gsl_vector_free(b);
		// gsl_vector_free(e);
		// //calculate pval from the t-distribution
		Std_error = sqrt(Error_var)/sqrt(n);
		
		// Std_error = sqrt(Error_var); //standard deviation! why does this result in better lod?!?!
		Std_error = sqrt(Error_var) * SE_IMMATURE(Delta, eigenval, eigenvec, X_test);

		T_calc = abs(SNP_effect)/Std_error;
		P_val = 1 - gsl_cdf_tdist_P(T_calc, n); //needed to subtract from 1 since the cdf function starts from the lower tail
		//append to list
		intcpt(snp) = intercept;
		eff(snp) = SNP_effect;
		delta(snp) = Delta;
		vg(snp) = Genetic_var;
		stderr(snp) = Std_error;
		tcalc(snp) = T_calc;
		pval(snp) = P_val;
		lod(snp) = -log10(P_val);
		//monitoring
		double progress = (1.0*snp)/l;
		// printf("%i\n", progress);
		int pb_max = 100;
		int pb_pos = pb_max * progress;
		std::cout << "[";
		for (int pb=0; pb<pb_max; pb++){
			if(pb<pb_pos) {std::cout << '=';
			} else if (pb==pb_pos) {std::cout << '>';
			} else {std::cout << ' ';}
			// printf("%i", pb);
		}
		std::cout << "] " << int(progress*100)+1 << " %" << '\r';
		std::cout.flush();
	}
	printf("\n");
	//cleanup
	gsl_matrix_free(X_test);
	gsl_matrix_free(K);
	gsl_matrix_free(eigenvec);
	gsl_vector_free(eigenval);
	gsl_vector_free(ARGMIN_OUT);
	gsl_vector_free(y_test);
	gsl_vector_free(b);
	gsl_vector_free(e);

	//spit this out:
	// return(*K); //test output against R's!!!! 20180920
	// return(*LL_vector);
	// return(Rcpp::List::create(Rcpp::Named("beta") = B, Rcpp::Named("eigenval") = EIGENVAL, Rcpp::Named("eigenvec") = EIGENVEC));
	// return(Rcpp::List::create(Rcpp::Named("effect") = eff, Rcpp::Named("log10_pval") = lod));
	return(Rcpp::List::create(Rcpp::Named("intercept") = intcpt, Rcpp::Named("effect") = eff, Rcpp::Named("delta") = delta, Rcpp::Named("Vg") = vg, Rcpp::Named("stderr") = stderr, Rcpp::Named("tcalc") = tcalc, Rcpp::Named("pval") = pval, Rcpp::Named("log10_pval") = lod));
}
