#!/usr/bin/env Rscript
# Simulating phenotypes using empirical genomic and transcriptomic data from Arabidopsis thaliana lines

#################
###			  ###
### ARGUMENTS ###
###			  ###
#################
args = commandArgs(trailingOnly=FALSE)
#R.DIR = args[1]
#slave = args[2]
#restore = args[3]
src_dir = sub("--file=", "", dirname(args[4])) #directory where the source code of the functions are located
#--args = args[5]
DIR = args[5]

####################################
###							 	 ###
### Load functions and libraries ###
###								 ### install GSL: sudo apt install libgsl-dev
#################################### install RcppGSL: install.packages("RcppGSL")
#for testing:
DIR="/media/sf_Documents/QUANTITATIVE GENETICS - combine SNP and transcript data/SRC/ANN-genome-transcriptome-proteome-phenome/res/"
src_dir = "/media/sf_Documents/QUANTITATIVE GENETICS - combine SNP and transcript data/SRC/ANN-genome-transcriptome-proteome-phenome/"
# DIR="/home/jeffersonfparil/Documents/PHD BIOSCIENCE - Genomics - Phenomics - Population and Quantitative Genetics/[02 PhD Science Thesis Work]/[003 QUANTITATIVE GENETICS - combine SNP and transcript data]/SRC/ANN-genome-transcriptome-proteome-phenome/res/"
# src_dir = "/home/jeffersonfparil/Documents/PHD BIOSCIENCE - Genomics - Phenomics - Population and Quantitative Genetics/[02 PhD Science Thesis Work]/[003 QUANTITATIVE GENETICS - combine SNP and transcript data]/SRC/ANN-genome-transcriptome-proteome-phenome/"
# DIR="/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE GENETICS - combine SNP and transcript data/SRC/ANN-genome-transcriptome-proteome-phenome/res/"
# src_dir = "/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE GENETICS - combine SNP and transcript data/SRC/ANN-genome-transcriptome-proteome-phenome/"
# DIR="/data/Lolium/Quantitative_Genetics/At_empirical/"
# src_dir = "/data/Lolium/Softwares/ANN-genome-transcriptome-proteome-phenome/"
setwd(DIR)

library(Rcpp)
library(RcppGSL) #sudo apt install libgsl-dev
sourceCpp(paste0(src_dir, "/GWA.cpp"))
#FUNCTION:	GWA(X, y)
#INPUT: 	X SNP matrix and y phenotype vector
#OUTPUT: 	list of SNP effects ($effects) and p-values ($p.value)
sourceCpp(paste0(src_dir, "/GWA_PC1.cpp"))
#FUNCTION:	GWA(X, PC1, y)
#INPUT: 	X SNP matrix; first principal component of t(X) so that PCA if nxn; and y phenotype vector
#OUTPUT: 	list of SNP effects ($effects) and p-values ($p.value)
source(paste0(src_dir, "/plot_Manhattan_ROC.r"))
#FUNCTION1:	Manhattan(LOD, alpha, ID, QTL)
#FUNCTION2:	ROC(SCORES, LABELS, ID)
#OUTPUT:	Manhattan with threshold line and ROC plot with AUC value

################################################
###											 ###
### Load empirical Arabidopsis thaliana data ###
###											 ###
################################################
X = readRDS("At_134Salk_SNP.rds")
y = readRDS("At_134Salk_phenotype_FT10.rds"); y = y$Phenotype
T = readRDS("At_134Salk_Transcripts.rds")
# simulate genotypic and transcriptomic data
# n = 134; l = 5e5; t = 3e4
# n = 134; l = 2000; t = 1500
# allele_freq = rbeta(l, shape1=2, shape2=2)
# X = matrix(rbinom(n=n*l, size=1, prob=allele_freq), nrow=n, ncol=l)
# T = matrix(rchisq(n=n*t, df=100), nrow=n, ncol=t)
# colnames(X) = 1:l
# colnames(T) = 1:t
#########################################################
###													  ###
### Simulate an additive genetic genetic architecture ###
###													  ###
#########################################################
#remove loci with MAF<0.02
X = X[, colMeans(X)>0.02]
#remove genotypes homozygous across all loci
X = X[rowSums(X)!=ncol(X) | rowSums(X)==0, ]
T = T[rowSums(X)!=ncol(X) | rowSums(X)==0, ]
#remove offending genotype Qar_8a
X = X[rownames(X)!="Qar_8a", ]

# define pehnotype simulation parameters
n = nrow(X)			# number of individuals
l = ncol(X) 		# number of loci
p = colMeans(X) 	# allele frequencies
q = 10				# number of QTL to simulate
lim = c(0.45, 0.55)	# lower and upper limit of allele frequency to choose QTL from
H2 = 0.50			# broad-sense heritability

# simulate QTL
sub = p[p>lim[1] & p<lim[2]]
frq = sample(sub, size=q, replace=FALSE)
loc = match(attr(frq, "names"), attr(p, "names"))
eff = rchisq(n=q, df=5)
# eff = rbeta(n=q, shape1=2, shape2=2)
QTL = data.frame(loc, frq, eff)
QTL = QTL[order(QTL$loc),]
b = matrix(0, nrow=l, ncol=1)
b[QTL$loc, 1] = QTL$eff

# simulate error effects
Vg = var(X%*%b)
Ve = Vg*(1-H2)/H2
e = matrix(rnorm(n=n, mean=0, sd=sqrt(Ve)), nrow=n, ncol=1)

# simulate phenotypes
y = X%*%b + e
y = (y - min(y))/(max(y)-min(y)) #not critical - I just want my phenotypes to range from 0 to 1

par(mfrow=c(2,2))
plot(x=1:l, y=(b!=0), type="l", main="QTL locations", xlab="Sites", ylab="")
hist(QTL$eff, main="QTL effects", xlab="QTL effect", ylab="Frequency")
hist(e, main="Error effects", xlab="Error effects", ylab="Frequency")
legend("topleft", legend=c(paste0("Vg=", round(Vg)), paste0("Ve=", round(Ve))))
hist(y, main="Phenotypes", xlab="Scaled phenotypic value", ylab="Frequency")

####################
###				 ###
### Perform GWAS ###
###				 ###
#################### no population structure correction
GWAS = GWA(X=X, y=y)
# LOD.GWAS = -log(GWAS$p.value, base=10)
# par(mfrow=c(1,2))
# Manhattan(LOD=LOD.GWAS, alpha=0.05, ID="GWAS.SIM", QTL=QTL)
# ROC(SCORES=LOD.GWAS, LABELS=(b!=0), ID="GWAS.SIM") # bloody structure confounding the results!!!

########################################
###				 					 ###
### Perform GWAS with PC1 correction ###
###				 					 ###
######################################## with population structure correction using PC1
PCA = prcomp(t(X))
PC1 = PCA$rotation[,1]
# PC1 = (PC1 - min(PC1))/(max(PC1)-min(PC1))
# PC1 = scale(PC1)
GWAS_PC1 = GWA_PC1(X=X, PC1=PC1, y=y)
# LOD.GWAS_PC1 = -log(GWAS_PC1$p.value, base=10)
# par(mfrow=c(1,2))
# Manhattan(LOD=LOD.GWAS_PC1, alpha=0.05, ID="GWAS_PCA.SIM", QTL=QTL)
# ROC(SCORES=LOD.GWAS_PC1, LABELS=(b!=0), ID="GWAS_PCA.SIM") # bloody structure confounding the results!!!


###########################################################################


# #ASSESSING GWAS & GWAS WITH PC1 TO CONTROL STRUCTURE: 20180904
# par(mfrow=c(2,2))
# Manhattan(LOD=GWAS$log10_pval, alpha=0.05, ID="GWAS.SIM", QTL=QTL)
# ROC(SCORES=GWAS$log10_pval, LABELS=(b!=0), ID="GWAS.SIM") # bloody structure confounding the results!!!
# Manhattan(LOD=GWAS_PC1$log10_pval, alpha=0.05, ID="GWAS_PCA.SIM", QTL=QTL)
# ROC(SCORES=GWAS_PC1$log10_pval, LABELS=(b!=0), ID="GWAS_PCA.SIM") # bloody structure confounding the results!!!


###########################################################################


########################
###				 	 ###
### Perform FaST-LMM ###
###				 	 ### Lippert et al 20111
######################## with population structure correction

# TESTS - 20181014
sourceCpp(paste0(src_dir, "/GWA.cpp"))
sourceCpp(paste0(src_dir, "/FaSTLMM.cpp"))

# K = X[,1:10000]
K = X
start_time = Sys.time()
x=FaST_LMM(K,y)
end_time = Sys.time()
end_time - start_time
# MIN=0.01
# MAX=100
# plot(x=seq(MIN, MAX, by=0.01), y=x, xlab="delta", ylab="LL(delta)")

GWAS = GWA(X=K, y=y)
PCA = prcomp(t(K))
PC1 = PCA$rotation[,1]
GWAS_PC1 = GWA_PC1(X=K, PC1=PC1, y=y)

jpeg("GWAS_eff_and_error.jpeg", width=2000, height=1000)
par(mfrow=c(2,2))
plot(x$log10_pval)
plot(GWAS$log10_pval)
plot(x$effect, GWAS$effect)
plot(x$stderr, GWAS$stderr)
dev.off()

#ASSESSING GWAS & GWAS WITH PC1 TO CONTROL STRUCTURE: 20180904
jpeg("Assoc_compare.jpeg", width=2000, height=1500)
par(mfrow=c(3,2))
Manhattan(LOD=GWAS$log10_pval, alpha=0.05, ID="GWAS.SIM", QTL=QTL)
ROC(SCORES=GWAS$log10_pval, LABELS=(b!=0), ID="GWAS.SIM") # bloody structure confounding the results!!!
Manhattan(LOD=GWAS_PC1$log10_pval, alpha=0.05, ID="GWAS_PCA.SIM", QTL=QTL)
ROC(SCORES=GWAS_PC1$log10_pval, LABELS=(b!=0), ID="GWAS_PCA.SIM") # bloody structure confounding the results!!!
Manhattan(LOD=x$log10_pval, alpha=0.05, ID="FaST-LMM.SIM", QTL=QTL)
ROC(SCORES=x$log10_pval, LABELS=(b!=0), ID="FaST-LMM.SIM")
dev.off()

# for(i in 1:8){
# 	idx = sample(1:(ncol(X)-999), 1)
# 	K = X[,idx:(idx+999)]
# 	x=FaST_LMM(K,y)
# 	plot(x=seq(MIN, MAX, by=0.01), y=x, xlab="delta", ylab="LL(delta)", type="l")
# }
tcalc = abs(x$effect)/(x$stderr*sqrt(n))	 #????
pval = pt(q=tcalc, df=(n), lower.tail=FALSE)
# lod = -log(pval+1e-100, base=10)
lod = -log(pval, base=10)
plot(lod)
###########################################################################



#######################
###					###
### Perform GWAlpha ###
###					###
#######################
np = 5						# number of pools
nip = floor(n/np)			# approximate number of individuals per pool
bins = rep(nip, each=np)	# approximate number of individuals per pool
bins[3] = bins[3] + n%%np 	# add remainder to the central pool
bins = cumsum(bins)			# index of individuals belonging to pool 2 to pool5
bins = c(0, bins)			# add index of individuals from all pools

Xy = cbind(X, y)			# merge genotype and phenotype data for sorting according to phenotypic value
Xy = Xy[order(y), ]			# sort from low to high phenotypic values
Xp = Xy[,1:l]				# genotype data
yp = Xy[,(l+1)]				# phenotype data
for (i in 1:np){
	Xpool = Xp[(bins[i]+1):(bins[i+1]), ]
	counts = colSums(Xpool)
	counts_alt = nrow(Xpool) - counts
	fillers = rep(":0:0:0", times=length(counts))
	sync = paste0(counts, ":", counts_alt, fillers)
	if(!exists("SYNC")){
		SYNC = eval(parse(text=paste0("data.frame(POOL_", i, "=sync)")))
	} else {
		eval(parse(text=paste0("SYNC$POOL_", i, "=sync")))
	}
}

CHROM_LOC = matrix(unlist(strsplit(colnames(Xp), "_")), nrow=ncol(Xp), ncol=2, byrow=TRUE)
CHROM_LOC = cbind(CHROM_LOC, rep("N", times=nrow(CHROM_LOC)))
SYNC = cbind(CHROM_LOC, SYNC)
SYNC$POOL_1 = as.character(SYNC$POOL_1) #making sure pool1 are of character types not factors!!!
write.table(SYNC, file="GWAlpha_sim.sync", row.names=FALSE, col.names=FALSE, sep="\t", quote=FALSE)
#rm(list="SYNC")

######## MAKE GWALPHA FASTER WITH RcppGSL!##########
#inputs:
#allele freq matrix --> SYNC_MATRIX
#phenotype data:
#	> min
#	> max
#	> standard deviation
#	> percentiles i.e. 20th, 40th, 60th and 80th percentiles
SYNC_MATRIX = SYNC[,4:ncol(SYNC)]
MIN_MAX_SD = c(min(yp), max(yp), sd(yp))
PERC = qnorm(c(0.2, 0.4, 0.6, 0.8), mean=mean(yp), sd=sd(yp)) #grouped equal number of individuals per bin i.e. 20% per bin

# ########################################################
# ###													 ###
# ### Simulate and additive transcript phenotype model ###
# ###													 ###
# ########################################################
# n = nrow(T)			# number of individuals
# m = ncol(T)			# number of transcripts
# t = 10				# number of causal transcripts to simnulate
# H2 = 0.5			# brad-sense heritability

# # simulate QTT
