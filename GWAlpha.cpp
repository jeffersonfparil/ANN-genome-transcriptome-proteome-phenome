#include <RcppGSL.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_cdf.h>
#include <cmath>

// Trying for increase the speed of GWAlpha

/////////////////////
///				  ///
// MAIN FUNCTION! ///
///				  ///
/////////////////////

// [[Rcpp::depends(RcppGSL)]]
// [[Rcpp::export]]
Rcpp::List GWAlpha(const RcppGSL::Matrix & SYNC_MATRIX, const RcppGSL::Vector & y_INPUT) {
	//define output variables
	Rcpp::NumericVector intcpt(l);
	Rcpp::NumericVector eff(l);
	
	//iterate per SNP
	for (int snp=0; snp<l; snp++){
	}
	return(Rcpp::List::create(Rcpp::Named("intercept") = intcpt, Rcpp::Named("effect") = eff, Rcpp::Named("delta") = delta, Rcpp::Named("Vg") = vg, Rcpp::Named("stderr") = stderr, Rcpp::Named("tcalc") = tcalc, Rcpp::Named("pval") = pval, Rcpp::Named("log10_pval") = lod));
}
